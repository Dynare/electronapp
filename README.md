<a name="logo"/>
<div align="center">
<a href="https://git.dynare.org/Dynare/electronapp" target="_blank">
<img src="https://git.dynare.org/uploads/-/system/project/avatar/126/dynareApp.jpg?width=64" width="64" height="64" alt="dynareApp Logo"></img>
</a>
</div>

# dynareApp

dynareApp (formerly electronApp) is a standalone application providing a Graphical User Interface (GUI) for Dynare that embeds a Dynare code editor, an output console and graphical reporting of results. It offers a standalone unified Dynare user experience with graphically enhanced functionalities. dynareApp jointly uses several technologies. The interface is rendered using web technologies whereas the computations are performed using [Dynare](http://www.dynare.org/) and a numerical computing environment such as [MatLab](https://www.mathworks.com/products/matlab.html?s_tid=hp_products_matlab). In more details, it is written in [Node.js](https://nodejs.org/en/) using the [Electron](https://www.electronjs.org/) framework.

 
# License
Most of the source files are covered by the GNU General Public Licence version
3 or later. Please read the attached LICENSE file.

# Installing dynareApp

## Standard install process

[Node.js](https://nodejs.org/en/) is required to run the dynareApp with [npm](https://www.npmjs.com/) as the default package manager for the project. The [Electron](https://www.electronjs.org/) software framework as well as the [Electron Forge](https://www.electronforge.io/) project manager are also required. A fresh install of [Dynare](https://www.dynare.org/) and a Dynare compatible version of [MATLAB](https://www.mathworks.com/products/matlab.html) are also required.

Note that the provided `package.json` file will install all the necessary dependencies, including Electron and Electron Forge once NodeJS and npm are available in the system. The user will still need to install Dynare and Matlab separately.

To run the development version of the dynareApp, follow these steps:
- Download and install NodeJS and npm,
- Download dynareApp source, 
- cd to the dynareApp directory and run `npm install` to install required dependencies,
- From the Dynare directory, copy the preprocessor binary from `/matlab/preprocessor64` to the dynareApp `src/assets/preprocessor` directory
- Edit the `src/assets/dynareapp.JSON` file and enter the path to your Matlab binary path and the Dynare install directory path
- In dynareApp directory run `npm start` to run the app

## Issue with sqlite3

During install there might be an issue with the sqlite3 binaries and the above standard install process might fail. In that case it might be useful to compile the sqlite3 binaries from source. The following procedure can be followed:


- cd to the dynareApp directory and `run npm install` to install required dependencies as usual,
- In the newly created `/node_module` directory, erase all electron related folders such as `electron`, `electron-debug`, `electron-installer-common`, etc.
- Build sqlite3 from source by typing `npm install sqlite3 --build-from-source`
- Reinstall electron by typing `npm install electron`
- From the Dynare directory, copy the preprocessor binary from `/matlab/preprocessor64` to the dynareApp `src/assets/preprocessor` directory
- Edit the `src/assets/dynareapp.JSON` file and enter the path to your Matlab binary path and the Dynare install directory path
- In dynareApp directory run `npm start` to run the app

# Building a distributable

dynareApp uses Electron Forge as a project manager. To build a distributable for the current system, simply `cd`to the main app directory and type `electron-forge make`. A new distributable will be available under `/out`.

If the above way of making the build results in buiding errors, use the following command to build:
`npm run make -- --targets @electron-forge/maker-zip`
This will build the executable and also make a zip of it. A new distributable and its zip file will be available under `/out`.


# Documentation

A small user guide (common with the Dynare webApp) for the perfect-foresight and stochastic simulation modules can be found under `/doc` or by following [webApp_doc_latest.pdf](https://git.dynare.org/Dynare/electronapp/-/blob/master/doc/webApp_doc_latest.pdf)