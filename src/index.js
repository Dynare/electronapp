const {
  ipcMain,
  dialog,
  BrowserWindow,
  app,
  Menu
} = require('electron')
const path = require('path');
const url = require('url');
const checkfileexistatpath = require('./appModules').checkfileexistatpath;
const loadjsonfileatpath = require('./appModules').loadjsonfileatpath;


// Handle creating/removing shortcuts on Windows when installing/uninstalling.
if (require('electron-squirrel-startup')) { // eslint-disable-line global-require
  app.quit();
}

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow = null;
let setupWindow = null;

// stack of available background threads
var available = []

// queue of tasks to be done
var tasks = []

var matlabreadyflag = 0;
var initialsetupflag = 0;

// Application menu

let template = [{
    label: 'File',
    submenu: [{
        label: 'New project',
        accelerator: 'Command+N',
        click: function () {
          // write function to create a new project
          mainWindow.webContents.send('createnewproject', 'please!')
        }
      },
      {
        label: 'Open project',
        //accelerator: 'Command+Q',
        click: function () {
          // write function to open a project 
        }
      },
    ]
  },
  {
    label: 'Edit',
    submenu: [{
        label: 'Undo',
        accelerator: 'CmdOrCtrl+Z',
        selector: 'undo:'
      },
      {
        label: 'Redo',
        accelerator: 'Shift+CmdOrCtrl+Z',
        selector: 'redo:'
      },
      {
        type: 'separator'
      },
      {
        label: 'Cut',
        accelerator: 'CmdOrCtrl+X',
        selector: 'cut:'
      },
      {
        label: 'Copy',
        accelerator: 'CmdOrCtrl+C',
        selector: 'copy:'
      },
      {
        label: 'Paste',
        accelerator: 'CmdOrCtrl+V',
        selector: 'paste:'
      },
      {
        label: 'Select All',
        accelerator: 'CmdOrCtrl+A',
        selector: 'selectAll:'
      }
    ]
  }
];

// creating the OS X specific menu
if (process.platform === 'darwin') {
  const name = app.getName()
  template.unshift({
    label: name,
    submenu: [{
      label: 'Quit',
      accelerator: 'Command+Q',
      click: function () {
        app.quit()
      }
    }]
  })
}




// main application window creation function
function createWindow() {

  console.log('Hello from Electron.');
  mainWindow = new BrowserWindow({
    show: true,
    backgroundColor: '#FFF',
    minWidth: 256,
    webPreferences: {
      devTools: false,
      nodeIntegration: true
    }
  });
  mainWindow.maximize();
  mainWindow.webContents.loadURL(`file://${__dirname}/index.html`); // #A
  // mainWindow.webContents.openDevTools();

  // Wait for 'ready-to-show' to display our window
  mainWindow.once('ready-to-show', () => {
    mainWindow.show()
  })

  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  })
}

// setup window creation function
function createSetupWindow() {
  return new Promise(function (resolve, reject) {
    setupWindow = new BrowserWindow({
      "show": true,
      webPreferences: {
        nodeIntegration: true,
        webSecurity: true
      }
    });
    setupWindow.maximize();
    setupWindow.loadURL('file://' + __dirname + '/setup.html')
    // setupWindow.webContents.openDevTools();
    setupWindow.on('closed', () => {
      console.log('background window closed')
    });

    ipcMain.on('openmatlabpathdialog', function (event) {
      dialog.showOpenDialog({
        properties: ['openFile']
      }, function (files) {
        if (files) event.sender.send('selectedmatlabpath', files)
      })
    });

    ipcMain.on('opendynarepathdialog', function (event) {
      dialog.showOpenDialog({
        properties: ['openDirectory']
      }, function (files) {
        if (files) event.sender.send('selecteddynarepath', files)
      })
    });

    ipcMain.on('pathsavedok', function (event) {
      return resolve();
    });
  });
}


// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', async function () {

  // verifying if the dynareapp.JSON that contains the paths is available
  try {
    var initfileflag = await checkfileexistatpath('dynareapp.JSON', './assets/');
  } catch (error) {
    console.log('error' + error);
  }

  // if not available we display the setup window and await for the setpup to finish
  if (initfileflag == false) {
    try {
      var testsetup = await createSetupWindow();
    } catch (error) {
      console.log('error' + error);
    }
    // once setup is finished we close setup window
    setupWindow.close();
  }

  // Once setup is finished and the paths have been saved, we continue normal program
  initialsetupflag = 1;

  //menu creation 
  const appMenu = Menu.buildFromTemplate(template);
  Menu.setApplicationMenu(appMenu);

  // running main windows
  createWindow();
  //createBgWindow();


  //loading matlab et dynare paths
  try {
    var pathholder = await loadjsonfileatpath('dynareapp.JSON', './assets/');
  } catch (error) {
    console.log("Can't load dynareapp.JSON" + error);
    return false;
  }
  var matlabpath = pathholder.matlabpath;
  var dynarepath = pathholder.dynarepath;

  //running matlab in a subprocess--------------------
  const {
    fork
  } = require('child_process');
  const program = path.join(__dirname, 'background.js');
  const parameters = [matlabpath, dynarepath];
  const options = {
    stdio: ['pipe', 'pipe', 'pipe', 'ipc']
  };
  const child = fork(program, parameters, options);

  var initprogressbar = 0;
  var initprogressbarBAYES = 0;
  var initprogressbarSMOOTH = 0;
  
  var progressbarcount = 0;
  var progressbarcountBAYES = 0;
  var progressbarcountSMOOTH = 0;

  // this message comes from either stochastic or perfect foresight and is a call to perform simulations
  ipcMain.on('rundynarematlab', function (event, data) {

    console.log('received ipc message from runprocesses');
    console.log(data)

    if (data=='estimation')
    {
      initprogressbar = 1;
      progressbarcount = 0;

      initprogressbarBAYES = 1;
      progressbarcountBAYES = 0;

      initprogressbarSMOOTH = 1;
      progressbarcountSMOOTH = 0;
    }


    if (matlabreadyflag == 1) {
      console.log("Matlab is ready !");
      mymessage = {
        'messageid': 'rundynarematlab',
        'messagetype': data
      };
      matlabreadyflag = 0;
      child.send(mymessage);
    } else {
      console.log("Matlab is not ready !");
    }
    event.returnValue = 'I heard you!';
  });

  child.on('message', message => {

    var messageprocess = 0;

    if (message.messageid == 'checkok') {

      console.log("backgrounok");

    }


    if (message.messageid == 'matlabready') {
      matlabreadyflag = 1;
      console.log("matlabready");
      console.log(message.message);
      messageprocess = 1;

      if (message.message == 1) {
        console.log('-=stochastic simulation completed=-');
        mainWindow.webContents.send('stochasticsimfinish', message.message);
      }

      if (message.message == 2) {
        console.log('-=perfect foresight computation completed=-');
        mainWindow.webContents.send('perfectsimfinish', message.message);
      }

      if (message.message == 3) {
        console.log('-=estimation computation completed=-');
        mainWindow.webContents.send('estimationfinish', message.message);
      }

    }

    if (message.messageid == 'stdoutmessage') {
      // console.log("stdoutmessage");
      // console.log(`${message.message}`);

      if (message.messagetype=='progressbar'){
        if (initprogressbar==1)
        {
          initprogressbar=0;
          mainWindow.webContents.send('matlabmessagetoconsole_progressbar_init', message.message);
          // console.log('matlabmessagetoconsole_progressbar_init sent');
        }
        else {
          progressbarcount=progressbarcount+1;
          if (progressbarcount%50==0){
            mainWindow.webContents.send('matlabmessagetoconsole_progressbar', message.message);
            // console.log('matlabmessagetoconsole_progressbar');
          }
          
  
        } 
        
      } else if (message.messagetype == 'progressbarBAYES') {
        if (initprogressbarBAYES == 1) {
          initprogressbarBAYES = 0;
          mainWindow.webContents.send('matlabmessagetoconsole_progressbarbayes_init', message.message);
        } else {
          progressbarcountBAYES = progressbarcountBAYES + 1;
          if (progressbarcountBAYES % 5 == 0) {
            mainWindow.webContents.send('matlabmessagetoconsole_progressbarbayes', message.message);
          }
        }
      } else if (message.messagetype == 'progressbarSMOOTH') {
        if (initprogressbarSMOOTH == 1) {
          initprogressbarSMOOTH = 0;
          mainWindow.webContents.send('matlabmessagetoconsole_progressbarsmooth_init', message.message);
        } else {
          progressbarcountSMOOTH = progressbarcountSMOOTH + 1;
          if (progressbarcountSMOOTH % 5 == 0) {
            mainWindow.webContents.send('matlabmessagetoconsole_progressbarsmooth', message.message);
          }
        }
      }
      else{
        console.log(message.message);
        // sockIO.emit('matlabmessagetoconsole', message.message);
        mainWindow.webContents.send('matlabmessagetoconsole', message.message);
      }





      
      messageprocess = 1;
    }

    if (messageprocess == 0) {
      console.log('Message from child:', message.message, 'with id:', message.messageid);
    }
  });


});



// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
});

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (initialsetupflag > 0) {
    if (mainWindow === null) {
      createWindow()
    }
  }
});