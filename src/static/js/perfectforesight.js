const ipcperf = require('electron').ipcRenderer;
const setpreprocessmore = require('./runprocesses').setpreprocessmore;
const runperfectnode = require('./runprocesses').runperfectnode;



var matlabdata;
var numshocks = [];
for (i = 0; i < 1000; i++) {
  numshocks[i] = 0;
}

function sortbyfirst(a, b) {
  if (a[0] === b[0]) {
    return 0;
  } else {
    return (a[0] < b[0]) ? -1 : 1;
  }
}

//------------------------------------------------------------@@@button set perfect
$('#setperfect').click(async function () {

  var modelname = $('#codename').val();
  if (modelname == null || modelname == "") {
    $.alert({
      title: 'Project name is empty.',
      content: 'Please choose a name for this project...',
    });
    return false;
  }


  var modelcode = {
    'modelcode': window.editor_dynare.getValue(),
    'modelhash': $('#codehash').val(),
    'modelname': modelname
  };

  // calling the 'node side'
  try {
    var resp = await setpreprocessmore(modelcode);
  } catch (error) {
    if (error.status == 200) {
      return popthis('Error', error.message);
    } else if (error.status == 100) {
      showconsolealert(error.message, 1);
      return false;
    } else if (error.status == 999) {
      popthis('Error', resp.message);
    }
  }

  showconsolealert(resp.message, 1);
  scrolltextarea();

  var currentdate = new Date();
  $('#classlist-form-messages').text('Last saved at: ' + currentdate);

  // sucess handling


  //the model has exo shocks: we make a table
  // if a steady state has not been provided if ask extra info on that
  dynaremodel = resp.modeljson;

  // steady-state compute table
  document.getElementById("perforssinittable").innerHTML = "";
  if (resp.status == 201) {
    var extrassinfo = "";
    var extrasshelp = "";
    if (resp.extrassinfo !== null) {
      extrassinfo = resp.extrassinfo;
      extrasshelp = " (the last known steady-state info have been loaded below)"
    }
    var extrasstype = "";
    if (resp.extrasstype == 1) {
      extrasstype = 'checked';
    }

    sscomputeeendo = `
            <h4>Steady state</h4>
            <p style="color:#3399CC">You did not provide any way to compute the steady-state. Please specify a way to compute the steady-state` + extrasshelp + `.</p>
            <div class="form-group no-margin"> <textarea class="form-control" id="perforSSblock" name="perforSSblock" placeholder="Steady-state equations;" rows="7">` + extrassinfo + `</textarea> </div>
            <div class="form-group no-margin"><label><input type="checkbox" name="issteadystatelock" id="issteadystateblock"` + extrasstype + `/> Check if the above will compute the exact steady-state (will be interpreted as a <span style="font-style: italic;">steady_state_model</span> block). Leave unchecked if the above should be used as a starting point to compute the steady-state (will be interpreted as an <span style="font-style: italic;">initval</span> block).</label></div>
            <input id="modelsteadystatestatus" name="modelsteadystatestatus" value="99" type="hidden">
            `;
  } else if (resp.status == 2) {
    sscomputeeendo = `
            <input id="modelsteadystatestatus" name="modelsteadystatestatus" value="55" type="hidden">
            `;
  } else {
    sscomputeeendo = `
            <input id="modelsteadystatestatus" name="modelsteadystatestatus" value="0" type="hidden">
            `;
  }
  document.getElementById("perforssinittable").innerHTML = sscomputeeendo;
  document.getElementById("model_endonum").value = (dynaremodel.endogenous).length;
  document.getElementById("model_exonum").value = (dynaremodel.exogenous).length;
  $('#perforModal').modal('show');

  return false;
});



//--------------------------------------------@@@button add sequence of shocks code
$('#addshock').click(function () {
  var rowindex;
  var xcond = 0;
  var iter = 0;
  while (xcond == 0) {
    if (numshocks[iter] == 0) {
      xcond = 1;
      rowindex = iter;
      numshocks[iter] = 1;
    }
    iter = iter + 1;
  }


  expectedshockstable = '';
  expectedshockstable += '<tr><td><select id="shockname' + rowindex + '" class="form-control input-sm" name="shocks">';
  for (x in dynaremodel.exogenous) {
    expectedshockstable += '<option value="' + x + '">' + dynaremodel.exogenous[x]["longName"] + '</option>';
  }
  expectedshockstable += "</select></td>";
  expectedshockstable += '<td><input type="text" class="form-control no-border input-sm" name="shockstart' + rowindex + '" id="shockstart' + rowindex + '" placeholder="Start"></td>';
  expectedshockstable += '<td><input type="text" class="form-control no-border input-sm" name="shockend' + rowindex + '" id="shockend' + rowindex + '" placeholder="End"></td>';
  expectedshockstable += '<td><input type="text" class="form-control no-border input-sm" name="shockvalues' + rowindex + '" id="shockvalues' + rowindex + '" placeholder="Value(s)"></td>';
  expectedshockstable += '<td><input type="text" class="form-control no-border input-sm" name="shockanticipdate' + rowindex + '" id="shockanticipdate' + rowindex + '" placeholder="0"></td>';
  expectedshockstable += '<td><input type="checkbox" class="form-control no-border input-sm" name="shockanticip' + rowindex + '" id="shockanticip' + rowindex + '"></td>';
  expectedshockstable += '<td><input type="checkbox" class="form-control no-border input-sm" onclick="activateunactivateperiod(' + rowindex + ')" name="shockpermanent' + rowindex + '" id="shockpermanent' + rowindex + '"></td>';
  expectedshockstable += '<td><input type="button" value="X" onclick="SomeDeleteRowFunction(this,' + rowindex + ')" class="form-control no-border input-sm" name="removeshock' + rowindex + '" id="removeshock' + rowindex + '"></td></tr>';

  $('#perforseqexptable tbody').append(expectedshockstable);

  return false;
});

//----------------------------------------------------@@@disable/unable periods when permanent shock is clicked
function activateunactivateperiod(thisnum) {

  if ($('#shockend' + thisnum.toString()).is(':disabled')) {
    //textbox is disabled
    $('#shockend' + thisnum.toString()).removeAttr('disabled');
    $('#shockstart' + thisnum.toString()).val('');
  } else {
    $('#shockend' + thisnum.toString()).attr({
      'disabled': 'disabled'
    });
    $('#shockend' + thisnum.toString()).val('');
    $('#shockstart' + thisnum.toString()).val('1');
  }
}

//----------------------------------------------------@@@removes a shock line in Shocks table
function SomeDeleteRowFunction(o, rowindex) {
  var p = o.parentNode.parentNode;
  p.parentNode.removeChild(p);
  numshocks[rowindex] = 0;
}


//----------------------------------------------------@@@button run perfect: verifies input then runs perfect foresight simulation
$('#runperfect').click(async function () {
  //getting all the necessary info from the forms
  var endonum = $('#model_endonum').val();
  var exonum = $('#model_exonum').val();
  var ssstatus = $('#modelsteadystatestatus').val();
  var globalvaluescount = 0;
  // checking the shocks tab
  var shockindex;
  var shockvaluearray = [];
  var shocksdescription = [];
  var permanentshocksdescription = [];
  var ssdescription = [];
  var permanentshockexist = 0;
  var nonanticipatedshockexist = 0;
  var transitoryshockexist = 0;
  var delayexist = 0;
  var permanentflag = [];
  var transitoryflag = [];
  var shocksflag = [];
  var nonanticipmatrix = [];
  var trackrow = 0;
  var sstype = 0;

  var ispermanent;
  var isnonanticipated;
  var isdelayed;

  for (i = 0; i < exonum; i++) {
    permanentflag[i] = 0;
    shocksflag[i] = 0;
    transitoryflag[i] = 0;
  }

  // setup tab checks

  // steady-state info check
  if (ssstatus == 99) {
    sstext = $('#perforSSblock').val();
    if (sstext.length == 0) {
      $('.nav-tabs a[href="#perforsetuptable"]').tab('show');
      popthis('Steady state error.', 'Please provide a way to compute the steady-state.');
      return (0);
    }
    if ($('#issteadystateblock').is(":checked") == true) {
      var sstype = 1;
    } else {
      var sstype = 2;
    }
    ssdescription.push({
      'sstext': sstext,
      'sstype': parseInt(sstype, 10)
    });
  }



  // Shocks tab checks
  $('.nav-tabs a[href="#perforseqexp"]').tab('show');

  var rowCount = $('#perforseqexptable tr').length - 1; // careful this counts the th row as a row, thus the -1

  if (rowCount > 0) {
    // there are shocks in the table
    globalvaluescount = 1;
    var xcond = 0;
    var iter = 0;
    var countshocks = 0;

    while (xcond == 0) {
      // we iterate over global numshocks that kept track of shock indexes in any order they were entered
      if (numshocks[iter] == 1) {
        // if numshocks identifies a valid row in the table, we start further checks
        countshocks = countshocks + 1;

        //we find the true row number of the current checked line in the table
        var perftablerow = $('#shockname' + iter.toString()).closest('tr');
        var perftablerowindex = perftablerow.index() + 1;


        shockvaluearray = [];

        // we fetch periods and values and anticipation dates
        shockstart = $('#shockstart' + iter.toString()).val();
        shockstart = shockstart.trim();
        shockend = $('#shockend' + iter.toString()).val();
        shockend = shockend.trim();
        shockvalue = $('#shockvalues' + iter.toString()).val();
        shockvalue = shockvalue.trim();
        shockanticipdate = $('#shockanticipdate' + iter.toString()).val();
        shockanticipdate = shockanticipdate.trim();

        ispermanent = 0;
        isnonanticipated = 0;
        isdelayed = 0;
        previousdelay = 0
        previousnonanticip = 0;

        // we fetch the index of the shock given by the Dynare preprocessor
        shockindex = $('#shockname' + iter.toString()).val();

        // we verify if shock has been flagged as permanent or non anticipated
        if ($("#shockpermanent" + iter.toString()).is(':checked')) {
          permanentflag[shockindex] = permanentflag[shockindex] + 1;
          permanentshockexist = 1;
          ispermanent = 1;
          if (nonanticipatedshockexist == 1) {
            previousnonanticip = 1;
          }
          if (delayexist == 1) {
            previousdelay = 1;
          }
        }

        if ($("#shockanticip" + iter.toString()).is(':checked')) {
          nonanticipatedshockexist = 1;
          isnonanticipated = 1;
        }

        // we validate the anticipation date
        if (shockanticipdate == null || shockanticipdate == "") {
          shockanticipdate = 0;
        } else {
          if (!(Number.isInteger(Number(shockanticipdate)))) {
            popthis('Shocks error.', 'The shock expectation date on row ' + perftablerowindex + ' is not an integer number.');
            return (0);
          }
          if (Number(shockanticipdate) < 1) {
            popthis('Shocks error.', 'The shock expectation date on row ' + perftablerowindex + ' can not be lower than 1.');
            return (0);
          }
          delayexist = 1;
          isdelayed = 1;
        }

        if (permanentflag[shockindex] > 1) {
          popthis('Shocks error.', 'You requested that shock ' + dynaremodel.exogenous[shockindex]["longName"] + ' is permanent in two or more instances. This is not supported, please specify only one permanent status per shock.');
        }


        if (((ispermanent == 0) && (isnonanticipated == 0)) && (isdelayed == 0)) {
          // the shock is not permanent or non anticipated or know with delay, user is supposed to enter periods
          transitoryflag[shockindex] = 1;
          // we validate the starting period
          if (shockstart == null || shockstart == "") {
            popthis('Shocks error.', 'You did not specify any starting period for shock on row ' + perftablerowindex + '.');
            return (0);
          } else {
            if (!(Number.isInteger(Number(shockstart)))) {
              popthis('Shocks error.', 'The shock starting period on row ' + perftablerowindex + ' is not an integer number.');
              return (0);
            }
            if (Number(shockstart) < 1) {
              popthis('Shocks error.', 'The shock starting period on row ' + perftablerowindex + ' can not be lower than 1.');
              return (0);
            }
            shockstart = parseInt(shockstart, 10);
          }

          // we validate the shock value
          if (shockvalue == null || shockvalue == "") {
            popthis('Shocks error.', 'You did not specify any value for shock on row ' + perftablerowindex + '.');
            return (0);
          } else {
            if (!(IsNumeric(shockvalue))) {
              // the user can enter several values separated by semicolon, we validate that
              if (shockvalue.indexOf(';') > -1) {
                columnsplit = shockvalue.split(';');
                columnsplitlength = columnsplit.length;
                if ((columnsplit[columnsplitlength - 1] == null) || (columnsplit[columnsplitlength - 1] == "")) {
                  // we let the user end such an entry with a semicolon, but then reduce the length
                  columnsplitlength--;
                }
                if (columnsplitlength > 1) {
                  // if we are sure there should be at least 2 periods, we check the end period
                  if (shockend == null || shockend == "") {
                    popthis('Shocks error.', 'You did not specify any ending period for shock on row ' + perftablerowindex + '.');
                    return (0);
                  } else {
                    if (!(Number.isInteger(Number(shockend)))) {
                      popthis('Shocks error.', 'The shock ending period on row ' + perftablerowindex + ' is not an integer number.');
                      return (0);
                    }
                    // if (Number(shockend) > Number(simperiods)) {
                    //   popthis('Shocks error.', 'The shock ending period on row ' + perftablerowindex + ' can not be larger than the number of simulation periods.');
                    //   return (0);
                    // }
                    if (Number(shockend) < Number(shockstart)) {
                      popthis('Shocks error.', 'The shock ending period on row ' + perftablerowindex + ' cannot be before the shock starting period.');
                      return (0);
                    }
                    shockend = parseInt(shockend, 10);
                  }

                  // we verify if the number of periods and values match
                  if (!(columnsplitlength == (shockend - shockstart + 1))) {
                    popthis('Shocks error.', 'The number of periods and the number of shock values does not match on row ' + perftablerowindex + '.');
                    return (0);
                  }

                  // we define a values array
                  for (i = 0; i < columnsplitlength; i++) {
                    if (!(IsNumeric(columnsplit[i]))) {
                      popthis('Shocks error.', 'At least one shock vector value on row ' + perftablerowindex + ' is not a number.');
                      return (0);
                    }
                    shockvaluearray.push(Number(columnsplit[i]));
                  }
                } else {
                  // this is a single shock with just a start period
                  if (!(IsNumeric(columnsplit[0]))) {
                    popthis('Shocks error.', 'The shock value on row ' + perftablerowindex + ' is not a number.');
                    return (0);
                  }
                  shockvaluearray.push(Number(columnsplit[0]));
                  shockend = parseInt(shockstart, 10);
                }
              } else {
                popthis('Shocks error.', 'The shock value on row ' + perftablerowindex + ' is not a number.');
                return (0);
              }
            } else {
              // the user did provide a single numeric value for the shock value
              // the user can provide a single value and a start and end period, in that case the value is the same for this whole range, or just a start period
              if (shockend == null || shockend == "") {
                // this is a single shock with just a start period
                if (!(IsNumeric(shockvalue))) {
                  popthis('Shocks error.', 'The shock value on row ' + perftablerowindex + ' is not a number.');
                  return (0);
                }
                shockend = parseInt(shockstart, 10);
                shockvaluearray.push(Number(shockvalue));
              } else {
                // this is a single value but with a range, we repeat the value over the range
                if (!(Number.isInteger(Number(shockend)))) {
                  popthis('Shocks error.', 'The shock ending period on row ' + perftablerowindex + ' is not an integer number.');
                  return (0);
                }
                // if (Number(shockend) > Number(simperiods)) {
                //   popthis('Shocks error.', 'The shock ending period on row ' + perftablerowindex + ' can not be larger than the number of simulation periods.');
                //   return (0);
                // }
                shockend = parseInt(shockend, 10);

                // we define a values array
                for (i = Number(shockstart); i < (Number(shockend) + 1); i++) {
                  shockvaluearray.push(Number(shockvalue));
                }
              }
            }
          }
          transitoryshockexist = 1;
          // collecting the description of the shocks in an object
          shocksdescription.push({
            'shockindex': parseInt(shockindex, 10),
            'shockname': dynaremodel.exogenous[shockindex]["longName"],
            'shockstartperiod': shockstart,
            'shockendperiod': shockend,
            'shockvalue': shockvaluearray,
            'shocknonanticipated': isnonanticipated,
            'shockpermanent': ispermanent,
            'shockdelayed': isdelayed
          });
        }

        // we want to rule out permanent shocks anticipated at date 1 or delayed permanent shocks with anticipation date 1, because basically it's the same thing as a permanent shock at date 1
        if ((ispermanent == 1) && ((isnonanticipated == 1) || (isdelayed == 1))) {
          if (shockstart == null || shockstart == "") {
            shockstart = 1;
          } else {
            if (!(Number.isInteger(Number(shockstart)))) {
              popthis('Shocks error.', 'The shock starting period on row ' + perftablerowindex + ' is not an integer number.');
              return (0);
            }
            if (Number(shockstart) < 1) {
              popthis('Shocks error.', 'The shock starting period on row ' + perftablerowindex + ' can not be lower than 1.');
              return (0);
            }
            shockstart = parseInt(shockstart, 10);
          }

          // we validate the anticipation date
          if (shockanticipdate == null || shockanticipdate == "") {
            shockanticipdate = 0;
          } else {
            if (Number(shockanticipdate) > Number(shockstart)) {
              popthis('Shocks error.', 'The shock expectation date on row ' + perftablerowindex + ' can not be after the start date.');
              return (0);
            }
            // if ((nonanticipatedshockexist==1)&&(delayexist==1)){popthis('Shocks error.','Please either choose a non anticipated shock or a delayed anticipation shock with anticipation date for shock on row '+countshocks+', but not both.');return(0);}
          }

          if ((isnonanticipated == 1) && (shockstart == 1)) {
            isnonanticipated = 0;
            if (previousnonanticip == 0) {
              nonanticipatedshockexist = 0;
            }
          }
          if ((isdelayed == 1) && (shockanticipdate == 1)) {
            isdelayed = 0;
            if (previousdelay == 0) {
              delayexist = 0;
            }
          }
        }
        if (((ispermanent == 1) && (isnonanticipated == 0)) && (isdelayed == 0)) {
          // the shock is permanent and anticipated, we allow only one value and ignore the ending period

          // we validate the starting period
          if (shockstart == null || shockstart == "") {
            shockstart = 1;
          } else {
            if (!(Number.isInteger(Number(shockstart)))) {
              popthis('Shocks error.', 'The shock starting period on row ' + perftablerowindex + ' is not an integer number.');
              return (0);
            }
            if (Number(shockstart) < 1) {
              popthis('Shocks error.', 'The shock starting period on row ' + perftablerowindex + ' can not be lower than 1.');
              return (0);
            }
            shockstart = parseInt(shockstart, 10);
          }

          // we validate the shock value
          if (shockvalue == null || shockvalue == "") {
            popthis('Shocks error.', 'You did not specify any value for shock on row ' + perftablerowindex + '.');
            return (0);
          } else {
            if (!(IsNumeric(shockvalue))) {
              popthis('Shocks error.', 'The shock value on row ' + perftablerowindex + ' is not numeric. A permanent shock can only have a single numeric value, no comma or semi-colon is authorized.');
              return (0);
            } else {
              // we collect the value
              shockvaluearray.push(Number(shockvalue));
            }
          }

          permanentshocksdescription.push({
            'shockindex': parseInt(shockindex, 10),
            'shockname': dynaremodel.exogenous[shockindex]["longName"],
            'shockstartperiod': shockstart,
            'shockendperiod': shockend,
            'shockvalue': shockvaluearray,
            'shocknonanticipated': isnonanticipated,
            'shockpermanent': ispermanent,
            'shockdelayed': isdelayed
          });
        }

        if ((isnonanticipated == 1) || (isdelayed == 1)) {
          // the shock is either non anticipated or delayed

          // we validate the starting period
          if (shockstart == null || shockstart == "") {
            popthis('Shocks error.', 'You did not specify any starting period for shock on row ' + perftablerowindex + '.');
            return (0);
          } else {
            if (!(Number.isInteger(Number(shockstart)))) {
              popthis('Shocks error.', 'The shock starting period on row ' + perftablerowindex + ' is not an integer number.');
              return (0);
            }
            if (Number(shockstart) < 1) {
              popthis('Shocks error.', 'The shock starting period on row ' + perftablerowindex + ' can not be lower than 1.');
              return (0);
            }
            shockstart = parseInt(shockstart, 10);
          }

          // we validate the anticipation date
          if (shockanticipdate == null || shockanticipdate == "") {
            shockanticipdate = 0;
          } else {
            if (Number(shockanticipdate) > Number(shockstart)) {
              popthis('Shocks error.', 'The shock expectation date on row ' + perftablerowindex + ' can not be after the start date.');
              return (0);
            }
          }

          // we validate the shock value
          if (shockvalue == null || shockvalue == "") {
            popthis('Shocks error.', 'You did not specify any value for shock on row ' + perftablerowindex + '.');
            return (0);
          } else {
            if (!(IsNumeric(shockvalue))) {
              // the user can enter several values separated by semicolon, we validate that
              if (shockvalue.indexOf(';') > -1) {
                columnsplit = shockvalue.split(';');
                columnsplitlength = columnsplit.length;
                if ((columnsplit[columnsplitlength - 1] == null) || (columnsplit[columnsplitlength - 1] == "")) {
                  // we let the user end such an entry with a semicolon, but then reduce the length
                  columnsplitlength--;
                }
                if (columnsplitlength > 1) {
                  // if we are sure there should be at least 2 periods, we check the end period
                  if (shockend == null || shockend == "") {
                    popthis('Shocks error.', 'You did not specify any ending period for shock on row ' + perftablerowindex + '.');
                    return (0);
                  } else {
                    if (!(Number.isInteger(Number(shockend)))) {
                      popthis('Shocks error.', 'The shock ending period on row ' + perftablerowindex + ' is not an integer number.');
                      return (0);
                    }
                    // if (Number(shockend) > Number(simperiods)) {
                    //   popthis('Shocks error.', 'The shock ending period on row ' + perftablerowindex + ' can not be larger than the number of simulation periods.');
                    //   return (0);
                    // }
                    if (Number(shockend) < Number(shockstart)) {
                      popthis('Shocks error.', 'The shock ending period on row ' + perftablerowindex + ' can before the shock starting period.');
                      return (0);
                    }
                    shockend = parseInt(shockend, 10);
                  }

                  // we verify if the number of periods and values match
                  if (!(columnsplitlength == (shockend - shockstart + 1))) {
                    popthis('Shocks error.', 'The number of periods and the number of shock values does not match on row ' + perftablerowindex + '.');
                    return (0);
                  }

                  // we define a values array
                  for (i = 0; i < columnsplitlength; i++) {
                    if (!(IsNumeric(columnsplit[i]))) {
                      popthis('Shocks error.', 'At least one shock vector value on row ' + perftablerowindex + ' is not a number.');
                      return (0);
                    }
                    shockvaluearray.push(Number(columnsplit[i]));
                  }
                } else {
                  // this is a single shock with just a start period
                  if (!(IsNumeric(columnsplit[0]))) {
                    popthis('Shocks error.', 'The shock value on row ' + perftablerowindex + ' is not a number.');
                    return (0);
                  }
                  shockvaluearray.push(Number(columnsplit[0]));
                  shockend = parseInt(shockstart, 10);
                }
              } else {
                popthis('Shocks error.', 'The shock value on row ' + perftablerowindex + ' is not a number.');
                return (0);
              }
            } else {
              // the user did provide a single numeric value for the shock value
              // the user can provide a single value and a start and end period, in that case the value is the same for this whole range, or just a start period
              if (shockend == null || shockend == "") {
                // this is a single shock with just a start period
                if (!(IsNumeric(shockvalue))) {
                  popthis('Shocks error.', 'The shock value on row ' + perftablerowindex + ' is not a number.');
                  return (0);
                }
                shockend = parseInt(shockstart, 10);
                shockvaluearray.push(Number(shockvalue));
              } else {
                // this is a single value but with a range, we repeat the value over the range
                if (!(Number.isInteger(Number(shockend)))) {
                  popthis('Shocks error.', 'The shock ending period on row ' + perftablerowindex + ' is not an integer number.');
                  return (0);
                }
                // if (Number(shockend) > Number(simperiods)) {
                //   popthis('Shocks error.', 'The shock ending period on row ' + perftablerowindex + ' can not be larger than the number of simulation periods.');
                //   return (0);
                // }
                shockend = parseInt(shockend, 10);

                // we define a values array
                for (i = Number(shockstart); i < (Number(shockend) + 1); i++) {
                  shockvaluearray.push(Number(shockvalue));
                }
              }
            }
          }

          // defining special anticipation tracking
          if (isdelayed == 1) {
            nonanticipmatrix[trackrow] = new Array(9);
            nonanticipmatrix[trackrow][0] = parseInt(shockanticipdate, 10);
            nonanticipmatrix[trackrow][1] = parseInt(shockindex, 10);
            if (ispermanent == 1) {
              nonanticipmatrix[trackrow][2] = 1;
            } else {
              nonanticipmatrix[trackrow][2] = 0;
            }
            nonanticipmatrix[trackrow][3] = 1;
            nonanticipmatrix[trackrow][4] = shockstart;
            nonanticipmatrix[trackrow][5] = shockend;
            nonanticipmatrix[trackrow][6] = shockvaluearray;
            nonanticipmatrix[trackrow][8] = "";
            trackrow++;
          } else {
            cl("shockstart:" + shockstart);
            cl("shockend:" + shockend);
            icurrentloop = 0;
            for (ixloop = shockstart; ixloop <= shockend; ixloop++) {
              cl("ixloop:" + ixloop)
              nonanticipmatrix[trackrow] = new Array(9);
              nonanticipmatrix[trackrow][0] = ixloop;
              nonanticipmatrix[trackrow][1] = parseInt(shockindex, 10);
              if (ispermanent == 1) {
                nonanticipmatrix[trackrow][2] = 1;
              } else {
                nonanticipmatrix[trackrow][2] = 0;
              }
              nonanticipmatrix[trackrow][3] = 0;
              nonanticipmatrix[trackrow][6] = shockvaluearray[icurrentloop];
              nonanticipmatrix[trackrow][8] = "";
              icurrentloop++;
              trackrow++;
            }
          }

        }


        //we count the number of this specific shock
        shocksflag[shockindex] = shocksflag[shockindex] + 1;
      }
      if (countshocks == rowCount) {
        xcond = 1;
      }
      iter = iter + 1;
    }


  }


  //checking options tab
  var perfoptionsdescription = [];
  var perfoptcounter = 0;
  var perfoptval;
  var perfoptdyn;


  for (let element of perfoptionslist) {

    if ($("#perfopt_id_" + perfoptcounter.toString()).is(':checked')) {

      if (element.perfoptinputtype == 0) { //checking no input options
        perfoptionsdescription.push({
          'perfoptindex': parseInt(perfoptcounter, 10),
          'perfoptdynarename': element.perfoptdynarename,
          'perfoptinputtype': parseInt(element.perfoptinputtype, 10),
          'perfoptvalue': ''
        });
      }

      if (element.perfoptinputtype == 1) { //checking integer input options

        perfoptval = $('#perfopt_input_' + perfoptcounter.toString()).val();
        perfoptval = perfoptval.trim();
        perfoptdyn = (element.perfoptdynarename).bold();

        if ((perfoptval == null || perfoptval == "")) {
          $('.nav-tabs a[href="#perfoptions"]').tab('show');
          popthis('Options error.', 'You have activated the ' + perfoptdyn + ' option without specifying a value. Please deactivate this option if you are not using it or want to use its default value.');
          return (0);
        } else {
          if (!(Number.isInteger(Number(perfoptval)))) {
            $('.nav-tabs a[href="#perfoptions"]').tab('show');
            popthis('Options error.', perfoptdyn + ' value should be an integer number.');
            return (0);
          }
          // if (Number(perfoptval) < 0) {
          //   $('.nav-tabs a[href="#perfoptions"]').tab('show');
          //   popthis('Options error.', perfoptdyn + ' value cannot be negative. Please deactivate this option to use the default value.');
          //   return (0);
          // }
        }

        perfoptionsdescription.push({
          'perfoptindex': parseInt(perfoptcounter, 10),
          'perfoptdynarename': element.perfoptdynarename,
          'perfoptinputtype': parseInt(element.perfoptinputtype, 10),
          'perfoptvalue': parseInt(perfoptval, 10)
        });
      }


      if ((element.perfoptinputtype == 2) || (element.perfoptinputtype == 4)) { //checking double and double with 'e' input options

        perfoptval = $('#perfopt_input_' + perfoptcounter.toString()).val();
        perfoptval = perfoptval.trim();
        perfoptdyn = (element.perfoptdynarename).bold();

        if ((perfoptval == null || perfoptval == "")) {
          $('.nav-tabs a[href="#perfoptions"]').tab('show');
          popthis('Options error.', 'You have activated the ' + perfoptdyn + ' option without specifying a value. Please deactivate this option if you are not using it or want to use its default value.');
          return (0);
        } else {
          if (!(IsNumeric(perfoptval))) {
            $('.nav-tabs a[href="#perfoptions"]').tab('show');
            popthis('Options error.', perfoptdyn + ' value should be a number.');
            return (0);
          }
        }

        perfoptionsdescription.push({
          'perfoptindex': parseInt(perfoptcounter, 10),
          'perfoptdynarename': element.perfoptdynarename,
          'perfoptinputtype': parseInt(element.perfoptinputtype, 10),
          'perfoptvalue': parseFloat(perfoptval, 10)
        });

      }



    }
    perfoptcounter++;
  }



  if (globalvaluescount == 0) {
    popthis('Simulation input error.', 'You have submitted no input to perform the simulation. Please input value(s) and retry.');
    return (0);
  } else {
    { //we post to the server

      nonanticipmatrix.sort(sortbyfirst);
      nonanticipmatrix[trackrow] = new Array(9);
      nonanticipmatrix[trackrow][0] = -1;

      if ((delayexist == 1) && (nonanticipatedshockexist == 1)) {
        popthis('Warning.', 'You have specified that a shock is non-expected while providing an expectation date. Only the effect of the expectation date will be taken into account.');
      }

      var perforval = {
        'modelhash': $('#codehash').val(),
        'ssstatus': parseInt(ssstatus, 10),
        'endonum': parseInt(endonum, 10),
        'exonum': parseInt(exonum, 10),
        'ssdescription': ssdescription,
        'shocksdescription': shocksdescription,
        'permanentshocksdescription': permanentshocksdescription,
        'optionsdescription': perfoptionsdescription,
        'transitoryshockexist': transitoryshockexist,
        'permanentshockexist': permanentshockexist,
        'nonanticipatedshockexist': nonanticipatedshockexist,
        'delayexist': delayexist,
        'nonanticipmatrix': nonanticipmatrix,
        'accesstype': 'runperfect',
        'jsontest': ["12"]
      };

      // var perforvaldata = JSON.stringify(perforval);

      $("#outputtable").empty();
      $("#plotbtn").empty();
      $("#plotbox").empty();
      $('#perforModal').modal('hide');
      var div = document.createElement('div');
      div.className = 'loader';
      div.innerHTML = '';
      document.getElementById('inner').appendChild(div);




      // calling the 'node side'
      try {
        var resp = await runperfectnode(JSON.stringify(perforval));
      } catch (error) {
        if (error.status == 2) {
          $('.nav-tabs a[href="#console"]').tab('show');
          $("#inner").empty();
          document.getElementById("inner").innerHTML = '<p style="color:red;">Matlab errors found. See console.</p>';
          return false;

        } else if (error.status == 3) {
          currentout = currentout + resp.data.message;
          $("textarea#preprocessorout").val(currentout + '\n \n The preprocessor could nor process your input. Please check the extra steady-state information you provided.');
          $('.nav-tabs a[href="#console"]').tab('show');
          $("#inner").empty();
          document.getElementById("inner").innerHTML = '<p style="color:red;">Preprocessor errors found. See console.</p>';
          return false;
        }
      }


      return false;
    }
  }

});



ipcperf.on('perfectsimfinish', (event, arg) => {

  async function finishperfect() {

    $('.nav-tabs a[href="#output"]').tab('show');

    try {
      matlabdata = await loadjsonfile('perforout.JSON');
    } catch (error) {
      return popthis('Error', 'Unable to open perforout.JSON to process:' + error);
    }


    // if (resp.data.status == 1) { //everything went ok
    var endovarname;

    // creating output content
    var sstbl1 = `
          <section>
          <h2>Steady state results</h2>
          <table class="table table-striped table-hover">
          <thead>
          <tr>
          <th>#</th>
          <th>Endogenous variable</th>
          <th>Initial Steady state</th>
          <th>Final Steady state</th>
          </tr>
          </thead>
          <tbody>
          `;

    var plotdropdown = `
          <section>
          <h2>Plots</h2>
          <div class="btn-group" role="group" aria-label="...">
          <div class="btn-group" role="group">
            <button id="plotbtn" class="btn btn-primary btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Select graph <span class="caret"></span><span class="sr-only">Toggle Dropdown</span>
            </button>
            <ul class="dropdown-menu" aria-labelledby="plotbtn">
            <li class="dropdown-header">Endogenous variables:</li>
            `;
    y = 0;
    for (x in matlabdata.endo_names) {
      endovarname = matlabdata.endo_names[x].trim();
      plotdropdown += '<li onclick="plotmethis(&quot;' + endovarname + '&quot;)"><a href="#stophere">' + endovarname + '</a></li>';
      y = y + 1;
      sstbl1 += '<tr><th scope="row">' + y + '</th>';
      sstbl1 += "<td>" + endovarname + "</td>";
      sstbl1 += "<td>" + matlabdata.steady_state1[x] + "</td>";
      sstbl1 += "<td>" + matlabdata.steady_state2[x] + "</td>";
    }
    plotdropdown += '</ul></div>';
    plotdropdown += `
      <div class="btn-group" role="group">
      <button id="exportbtn" class="btn btn-primary btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Export data <span class="caret"></span><span class="sr-only">Toggle Dropdown</span>
      </button>
      <ul class="dropdown-menu" aria-labelledby="exportbtn">
      <li class="dropdown-header">Select file type:</li>
      <li onclick="exportfile(&quot;json&quot;)"><a href="#stophere">Export to JSON</a></li>
      <li onclick="exportfile(&quot;csv&quot;)"><a href="#stophere">Export to CSV</a></li>
      </ul>
      </div>`;

    plotdropdown += `
      </div>
          </section>
          `;
    sstbl1 += "</tbody></table></section>";
    $("#inner").empty();
    document.getElementById("outputtable").innerHTML = sstbl1;
    document.getElementById("plotbtn").innerHTML = plotdropdown;


    plotmethis(matlabdata.endo_names[0].trim());



  }



  finishperfect();



});

// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ output

// graph plotter
function plotmethis(plotvar) {

  var trace1 = {
    x: matlabdata.endo_simul.plotx,
    y: matlabdata.endo_simul[plotvar]
  };
  var data = [trace1];

  var layout = {
    title: plotvar,
    xaxis: {
      title: 'Periods'
    },
    yaxis: {
      title: plotvar
    }
  };

  Plotly.newPlot('plotbox', data, layout);
}

// files exporter
function exportfile(exporttype) {

  var filename = $('#codename').val();

  if (exporttype == 'json') {
    var exportjson = {
      'simulation': [],
      'steadystate1': matlabdata.steady_state1,
      'steadystate2': matlabdata.steady_state2
    };
    for (var key in matlabdata.endo_simul) {
      if (key != "plotx") {
        exportjson.simulation.push({
          [key]: matlabdata.endo_simul[key]
        });
      }
    }
    var exportjsontext = JSON.stringify(exportjson);
    download(filename + ".JSON", exportjsontext);
  }

  if (exporttype == 'csv') {
    var exportcsv = matlabdata.endo_names + "\r\n";
    var csvarray = [];
    for (var mykey in matlabdata.endo_names) {
      csvarray.push(matlabdata.endo_simul[matlabdata.endo_names[mykey]]);
    }
    var transpose = csvarray[0].map((col, i) => csvarray.map(row => row[i]));
    for (itr = 0; itr < transpose.length; itr++) {
      exportcsv += transpose[itr] + "\r\n";
    }
    download(filename + ".csv", exportcsv);
  }
}


function onoffperfoption(thisnum) {
  if ($('#perfopt_input_' + thisnum.toString()).is(':disabled')) {
    //textbox is disabled
    $('#perfopt_input_' + thisnum.toString()).removeAttr('disabled');
    $('#perfopt_input_' + thisnum.toString()).val('');
  } else {
    $('#perfopt_input_' + thisnum.toString()).attr({
      'disabled': 'disabled'
    });
    $('#perfopt_input_' + thisnum.toString()).val('');
  }
}

// function onoffperfoptionselect(thisnum) {
//   if ($('#perfopt_input_' + thisnum.toString()).is(':disabled')) {
//     //textbox is disabled
//     $("#perfopt_input_" + thisnum.toString()).removeAttr('disabled');
//   } else {
//     $("#perfopt_input_" + thisnum.toString()).attr('disabled', 'disabled');
//   }
// }



function createperfoptions() {

  var perfopttableoptions = '';
  perfoptionslist.forEach(function (element) {

    if (element.perfoptinputtype == 0) { //options that do not require the entry of a value
      perfopttableoptions += `<tr>
                            <th scope="row"><input type="checkbox" id="perfopt_id_` + element.perfoptid + `" onchange="onoffperfoption(` + element.perfoptid + `)" data-toggle="toggle" data-onstyle="success" data-size="mini"></th>
                            <td>` + element.perfoptdynarename + `</td>
                            <td></td>
                            <td><div class="tooltipcss"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span><span class="tooltiptextcss">` + element.perfoptinfo + `</span></div></td>
                          </tr>`;
    }

    if ((element.perfoptinputtype != 0) && (element.perfoptinputtype != 5)) { //options that do require the entry of a value



      perfopttableoptions += `<tr>
                          <th scope="row"><input type="checkbox" id="perfopt_id_` + element.perfoptid + `" onchange="onoffperfoption(` + element.perfoptid + `)" data-toggle="toggle" data-onstyle="success" data-size="mini"></th>
                          <td>` + element.perfoptdynarename + `</td>
                          <td><input class="form-control input-sm" name="perfopt_input_` + element.perfoptid + `" id="perfopt_input_` + element.perfoptid + `" placeholder="` + element.perfoptplaceholder + `" type="text" value="" disabled="disabled"></td>
                          <td><div class="tooltipcss"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span><span class="tooltiptextcss">` + element.perfoptinfo + `</span></div></td>
                        </tr>`;

    }


  });

  $('#perfoptiontable tbody').append(perfopttableoptions);
}


var perfoptionslist = [{
    perfoptid: 0,
    perfoptdynarename: 'periods',
    perfoptplaceholder: '200',
    perfoptinputtype: 1,
    perfoptinfo: 'Number of periods of the simulation.<br> Defaut: 200.'
  },
  {
    perfoptid: 1,
    perfoptdynarename: 'maxit',
    perfoptplaceholder: '50',
    perfoptinputtype: 1,
    perfoptinfo: 'Determines the maximum number of iterations used in the non-linear solver.<br> Defaut: 50.'
  },
  {
    perfoptid: 2,
    perfoptdynarename: 'tolf',
    perfoptplaceholder: '1.0e-5',
    perfoptinputtype: 2,
    perfoptinfo: 'Convergence criterion for termination based on the function value. Iteration will cease when it proves impossible to improve the function value by more than tolf. <br> Defaut: 1.0e-5.'
  },
  {
    perfoptid: 3,
    perfoptdynarename: 'tolx',
    perfoptplaceholder: '1.0e-5',
    perfoptinputtype: 2,
    perfoptinfo: 'Convergence criterion for termination based on the change in the function argument. Iteration will cease when the solver attempts to take a step that is smaller than tolx.<br> Defaut: 1.0e-5.'
  },
  {
    perfoptid: 4,
    perfoptdynarename: 'noprint',
    perfoptplaceholder: '',
    perfoptinputtype: 0,
    perfoptinfo: 'Don’t print anything. Useful for loops.'
  },
  {
    perfoptid: 5,
    perfoptdynarename: 'stack_solve_algo',
    perfoptplaceholder: '0',
    perfoptinputtype: 1,
    perfoptinfo: 'Algorithm used for computing the solution. Possible values are: <br>0: Newton method to solve simultaneously all the equations for every period, using sparse matrices (Default). <br>1: Use a Newton algorithm with a sparse LU solver at each iteration (requires bytecode and/or block option, see Model declaration). <br>2: Use a Newton algorithm with a Generalized Minimal Residual (GMRES) solver at each iteration (requires bytecode and/or block option, see Model declaration) <br>3: Use a Newton algorithm with a Stabilized Bi-Conjugate Gradient (BICGSTAB) solver at each iteration (requires bytecode and/or block option, see Model declaration). <br>4: Use a Newton algorithm with a optimal path length at each iteration (requires bytecode and/or block option, see Model declaration). <br>5: Use a Newton algorithm with a sparse Gaussian elimination (SPE) solver at each iteration (requires bytecode option, see Model declaration). <br>6: Use the historical algorithm proposed in Juillard (1996): it is slower than stack_solve_algo=0, but may be less memory consuming on big models (not available with bytecode and/or block options). <br>7: Allows the user to solve the perfect foresight model with the solvers available through option solve_algo (See solve_algo for a list of possible values, note that values 5, 6, 7 and 8, which require bytecode and/or block options, are not allowed).<br> Defaut: 0: Newton method to solve simultaneously all the equations for every period.'
  },
  {
    perfoptid: 6,
    perfoptdynarename: 'robust_lin_solve',
    perfoptplaceholder: '',
    perfoptinputtype: 0,
    perfoptinfo: 'Triggers the use of a robust linear solver for the default stack_solve_algo=0.'
  },
  {
    perfoptid: 7,
    perfoptdynarename: 'solve_algo',
    perfoptplaceholder: '',
    perfoptinputtype: 1,
    perfoptinfo: 'Determines the non-linear solver to use.Possible values for OPTION are: <br> 0: Use fsolve (under MATLAB, only available if you have the Optimization Toolbox; always available under Octave). <br>1: Use Dynare’s own nonlinear equation solver (a Newton-like algorithm with line-search). <br>2: Splits the model into recursive blocks and solves each block in turn using the same solver as value 1. <br>3: Use Chris Sims’ solver. <br>4: Splits the model into recursive blocks and solves each block in turn using a trust-region solver with autoscaling. <br>5: Newton algorithm with a sparse Gaussian elimination (SPE) (requires bytecode option, see Model declaration). <br>6: Newton algorithm with a sparse LU solver at each iteration (requires bytecode and/or block option, see Model declaration). <br>7: Newton algorithm with a Generalized Minimal Residual (GMRES) solver at each iteration (requires bytecode and/or block option, see Model declaration). <br>8: Newton algorithm with a Stabilized Bi-Conjugate Gradient (BICGSTAB) solver at each iteration (requires bytecode and/or block option, see Model declaration). <br>9: Trust-region algorithm on the entire model. <br>10: Levenberg-Marquardt mixed complementarity problem (LMMCP) solver (Kanzow and Petra (2004)). <br>11: PATH mixed complementarity problem solver of Ferris and Munson (1999). The complementarity conditions are specified with an mcp equation tag, see lmmcp. Dynare only provides the interface for using the solver. Due to licence restrictions, you have to download the solver’s most current version yourself from http://pages.cs.wisc.edu/~ferris/path.html and place it in MATLAB’s search path.'
  },
  {
    perfoptid: 8,
    perfoptdynarename: 'no_homotopy',
    perfoptplaceholder: '',
    perfoptinputtype: 0,
    perfoptinfo: 'By default, the perfect foresight solver uses a homotopy technique if it cannot solve the problem. Concretely, it divides the problem into smaller steps by diminishing the size of shocks and increasing them progressively until the problem converges. This option tells Dynare to disable that behavior. Note that the homotopy is not implemented for purely forward or backward models.'
  },
  {
    perfoptid: 9,
    perfoptdynarename: 'markowitz',
    perfoptplaceholder: '0.5',
    perfoptinputtype: 2,
    perfoptinfo: 'Value of the Markowitz criterion, used to select the pivot. Only used when stack_solve_algo = 5. <br> Defaut: 0.5.'
  },
  {
    perfoptid: 10,
    perfoptdynarename: 'minimal_solving_periods',
    perfoptplaceholder: '1',
    perfoptinputtype: 1,
    perfoptinfo: 'Specify the minimal number of periods where the model has to be solved, before using a constant set of operations for the remaining periods. Only used when stack_solve_algo = 5.<br> Defaut: 1.'
  },
  {
    perfoptid: 11,
    perfoptdynarename: 'lmmcp',
    perfoptplaceholder: '',
    perfoptinputtype: 0,
    perfoptinfo: 'Solves the perfect foresight model with a Levenberg-Marquardt mixed complementarity problem (LMMCP) solver (Kanzow and Petra (2004)), which allows to consider inequality constraints on the endogenous variables (such as a ZLB on the nominal interest rate or a model with irreversible investment). This option is equivalent to stack_solve_algo=7 and solve_algo=10. Using the LMMCP solver requires a particular model setup as the goal is to get rid of any min/max operators and complementary slackness conditions that might introduce a singularity into the Jacobian. This is done by attaching an equation tag (see Dynare help) with the mcp keyword to affected equations. '
  },
  {
    perfoptid: 12,
    perfoptdynarename: 'endogenous_terminal_period',
    perfoptplaceholder: '',
    perfoptinputtype: 0,
    perfoptinfo: 'The number of periods is not constant across Newton iterations when solving the perfect foresight model. The size of the nonlinear system of equations is reduced by removing the portion of the paths (and associated equations) for which the solution has already been identified (up to the tolerance parameter). This strategy can be interpreted as a mix of the shooting and relaxation approaches. Note that round off errors are more important with this mixed strategy (user should check the reported value of the maximum absolute error). Only available with option stack_solve_algo==0.'
  },
  {
    perfoptid: 13,
    perfoptdynarename: 'linear_approximation',
    perfoptplaceholder: '',
    perfoptinputtype: 0,
    perfoptinfo: 'Solves the linearized version of the perfect foresight model. The model must be stationary. Only available with option stack_solve_algo==0.'
  }
];

createperfoptions();
