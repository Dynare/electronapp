const setpreprocess = require('./runprocesses').setpreprocess;
const newmodelindb = require('./appModules').newmodelindb;

// const showconsolealert = require('./appModules').showconsolealert;


// create a file download link
function download(filename, text) {
  var element = document.createElement('a');
  element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
  element.setAttribute('download', filename);
  element.style.display = 'none';
  document.body.appendChild(element);
  element.click();
  document.body.removeChild(element);
}

function scrolltextarea(){
  var textarea = document.getElementById('preprocessorout');
  textarea.scrollTop = textarea.scrollHeight; 
}





var int;
var autosaveinterval = 30000;
int = window.setInterval(autosavecall, autosaveinterval);

async function autosavecall() {

  var modelcode = {
    'modelcode': window.editor_dynare.getValue(),
    'modelhash': $('#codehash').val(),
    'modelname': $('#codename').val()
  };

      // calling the 'node side'
      try {
        var resp = await savetodb(modelcode);
      } catch (error) {
        if (error.status == 200) {
          console.log("200")
          return popthis('Error', error.message);
        } else if (error.status == 100) {
          console.log("100")
          showconsolealert(error.message, 1);
          console.log('console error');
          return false;
        } else if (error.status == 999) {
          popthis('Error', resp.message);
        }
      }
  
      
      // console.log(resp)
  
        var currentdate = new Date();
        $('#classlist-form-messages').text('Last saved at: ' + currentdate);

        return false;
  }



$('#savecode').click(async function () {
//button save code

  var modelname = $('#codename').val();
  if (modelname==null || modelname==""){
    $.alert({
      title: 'Project name is empty.',
      content: 'Please choose a name for this project...',
  });
  return false;
  }

  var modelcode = {
    'modelcode':window.editor_dynare.getValue(),
    'modelhash':$('#codehash').val(),
    'modelname':modelname
    //'accesstype': 'save'
  };

    // calling the 'node side'
    try {
      var resp = await savetodb(modelcode);
    } catch (error) {
      if (error.status == 200) {
        console.log("200")
        return popthis('Error', error.message);
      } else if (error.status == 100) {
        console.log("100")
        showconsolealert(error.message, 1);
        console.log('console error');
        return false;
      } else if (error.status == 999) {
        popthis('Error', resp.message);
      }
    }

    console.log(resp)


      var currentdate = new Date();
      $('#classlist-form-messages').text('Last saved at: ' + currentdate);

  return false;
});

//button clear console
$('#consoleclearbtn').click(function () {
  document.getElementById("preprocessorout").value='';
});


//new project on click function
async function createnewproject(){

  editorformurl=$(editorform).attr('action');

  $.confirm({
      title: 'New project',
      content: '' +
      '<form action="" class="formName">' +
      '<div class="form-group">' +
      '<label>Please enter a name for the new project:</label>' +
      '<input type="text" placeholder="Project name" class="newprojectname form-control" required />' +
      '</div>' +
      '</form>',
      buttons: {
          formSubmit: {
            text: 'Create',
            btnClass: 'btn-blue',
            action: async function () {
                var newname = this.$content.find('.newprojectname').val();
                if (!newname) {
                  $.alert('Please provide a valid name');
                  return false;
                }
                // var modelcode = {
                //   'codetext': 'nothing',
                //   'codehash': 'nothing',
                //   'codename': newname,
                //   'accesstype': 'newproject'
                // };
                // calling the 'node side'
                try {
                  var resp = await newmodelindb(newname);
                } catch (error) {
                  if (error.status == 200) {
                    console.log("200")
                    return popthis('Error', error.message);
                  } else if (error.status == 100) {
                    console.log("100")
                    showconsolealert(error.message, 1);
                    console.log('console error');
                    return false;
                  } else if (error.status == 999) {
                    popthis('Error', resp.message);
                  }
                }

                var loc = window.location;
                //this will reload with get forcing python to request the latest model from db
                window.location = loc.protocol + '//' + loc.host + loc.pathname + loc.search;

              }
          },
          cancel: function () {
              //close
          },
      },
      onContentReady: function () {
          // bind to events
          var jc = this;
          this.$content.find('form').on('submit', function (e) {
              // if the user submits the form by pressing enter in the field.
              e.preventDefault();
              jc.$$formSubmit.trigger('click'); // reference the button and click it
          });
      }
  });

}




//button run code

$('#runcode').click(async function () {

  console.log('Runcode');

  // const reply = ipc.sendSync('synchronous-message', 'Mr. Watson, come here.');
  // console.log(reply);

  var modelname = $('#codename').val();
  if (modelname==null || modelname==""){
    $.alert({
      title: 'Project name is empty.',
      content: 'Please choose a name for this project...',
  });
  return false;
  }
  
  
  var modelcode = {
    'modelcode':window.editor_dynare.getValue(),
    'modelhash':$('#codehash').val(),
    'modelname':$('#codename').val()
    // 'accesstype': 'savepreprocess'
  };

//calling the node side
try {
  var resp = await setpreprocess(modelcode);
} catch (error) {
  if (error.status == 200) {
    console.log("200")
    return popthis('Error', error.message);
  } else if (error.status == 100) {
    console.log("100")
    showconsolealert(error.message, 1);
    console.log('console error');
    return false;
  }
}

showconsolealert(resp.message, 1);
//displays the new save time
var currentdate = new Date();
$('#classlist-form-messages').text('Last saved at: ' + currentdate);
  return false;
});
