const ipcest = require('electron').ipcRenderer;
const runestimationnode = require('./runprocesses').runestimationnode;
const runsaveestimationfilenode = require('./runprocesses').runsaveestimationfilenode;




var estmatlabdata;

$('#setestimation').click(async function () {

  // let basedir = path.resolve(__dirname, './assets/modfiles/');

  let modelname = $('#codename').val();
  if (modelname == null || modelname == "") {
    $.alert({
      title: 'Project name is empty.',
      content: 'Please choose a name for this project...',
    });
    return false;
  }

  let modelcode = {
    'modelcode': window.editor_dynare.getValue(),
    'modelhash': $('#codehash').val(),
    'modelname': $('#codename').val(),
    'accesstype': 'setestimation'
  };

  // calling the 'node side'
  try {
    var resp = await setpreprocessmore(modelcode);
  } catch (error) {
    if (error.status == 200) {
      return popthis('Error', error.message);
    } else if (error.status == 100) {
      showconsolealert(error.message, 1);
      return false;
    } else if (error.status == 999) {
      popthis('Error', resp.message);
    }
  }

  showconsolealert(resp.message, 1);
  scrolltextarea();

  var currentdate = new Date();
  $('#classlist-form-messages').text('Last saved at: ' + currentdate);


  dynaremodel = resp.modeljson;

  // if a steady state has not been provided if ask extra info on that
  // steady-state compute table
  document.getElementById("estinittable").innerHTML = "";
  if (resp.status == 201) {
    var extrassinfo = "";
    var extrasshelp = "";
    if (resp.extrassinfo !== null) {
      extrassinfo = resp.extrassinfo;
      extrasshelp = " (the last known steady-state info have been loaded below)"
    }
    var extrasstype = "";
    if (resp.extrasstype == 1) {
      extrasstype = 'checked';
    }

    sscomputeeendo = `
        <h4>Steady state</h4>
        <p style="color:#3399CC">You did not provide any way to compute the steady-state. Please specify a way to compute the steady-state` + extrasshelp + `.</p>
        <div class="form-group no-margin"> <textarea class="form-control" id="estSSblock" name="estSSblock" placeholder="Steady-state equations;" rows="7">` + extrassinfo + `</textarea> </div>
        <div class="form-group no-margin"><label><input type="checkbox" name="estissteadystateblock" id="estissteadystateblock"` + extrasstype + `/> Check if the above will compute the exact steady-state (will be interpreted as a <span style="font-style: italic;">steady_state_model</span> block). Leave unchecked if the above should be used as a starting point to compute the steady-state (will be interpreted as an <span style="font-style: italic;">initval</span> block).</label></div>
        <input id="estmodelsteadystatestatus" name="estmodelsteadystatestatus" value="99" type="hidden">
        `;
  } else if (resp.status == 2) {
    sscomputeeendo = `
        <input id="estmodelsteadystatestatus" name="estmodelsteadystatestatus" value="55" type="hidden">
        `;
  } else {
    sscomputeeendo = `
        <input id="estmodelsteadystatestatus" name="estmodelsteadystatestatus" value="0" type="hidden">
        `;
  }
  document.getElementById("estinittable").innerHTML = sscomputeeendo;
  document.getElementById("estmodel_endonum").value = (dynaremodel.endogenous).length;
  document.getElementById("estmodel_exonum").value = (dynaremodel.exogenous).length;
  document.getElementById("estmodel_paramnum").value = (dynaremodel.exogenous).length + (dynaremodel.parameters).length;
  $('#estimModal').modal('show');


  return false;

});


var estobsnum = [];
var estparnum = [];
for (i = 0; i < 1000; i++) {
  estobsnum[i] = 0;
  estparnum[i] = 0;
}


$('#estaddobs').click(function () {
  var estobsrowindex;
  var estobsxcond = 0;
  var estobsiter = 0;

  while (estobsxcond == 0) {
    if (estobsnum[estobsiter] == 0) {
      estobsxcond = 1;
      estobsrowindex = estobsiter;
      estobsnum[estobsiter] = 1;
    }
    estobsiter = estobsiter + 1;
  }
  estobsexpectedshockstable = '';
  estobsexpectedshockstable += '<tr><td><select id="estobsvarname' + estobsrowindex + '" class="form-control input-sm" name="estobsvarname">';
  for (x in dynaremodel.endogenous) {
    estobsexpectedshockstable += '<option value="' + x + '">' + dynaremodel.endogenous[x]["longName"] + '</option>';
  }
  estobsexpectedshockstable += "</select></td>";


  estobsexpectedshockstable += '<td><input type="button" value="X" onclick="estobsSomeDeleteRowFunction(this,' + estobsrowindex + ')" class="form-control no-border input-sm btn-default btn-xs" name="estobsremoveshock' + estobsrowindex + '" id="estobsremoveshock' + estobsrowindex + '"></td></tr>';
  $('#estobstable tbody').append(estobsexpectedshockstable);
  return false;
});

function estobsSomeDeleteRowFunction(o, estobsrowindex) {
  var p = o.parentNode.parentNode;
  p.parentNode.removeChild(p);
  estobsnum[estobsrowindex] = 0;
}




$('#estaddparam').click(function () {
  var estparrowindex;
  var estparxcond = 0;
  var estpariter = 0;
  var exovarcounter = 999;
  while (estparxcond == 0) {
    if (estparnum[estpariter] == 0) {
      estparxcond = 1;
      estparrowindex = estpariter;
      estparnum[estpariter] = 1;
    }
    estpariter = estpariter + 1;
  }
  estparexpectedshockstable = '';
  estparexpectedshockstable += '<tr><td><select id="estparname' + estparrowindex + '" class="form-control input-sm" name="estparname" onchange="enableestvarcorr(' + estparrowindex + ')">';
  estparexpectedshockstable += '<option disabled>Parameters</option>';
  for (x in dynaremodel.parameters) {
    estparexpectedshockstable += '<option value="' + x + '">' + dynaremodel.parameters[x]["longName"] + '</option>';
  }

  estparexpectedshockstable += '<option disabled>Exo variables</option>';
  for (x in dynaremodel.exogenous) {
    exovarcounter++;
    estparexpectedshockstable += '<option value="' + exovarcounter + '">' + dynaremodel.exogenous[x]["longName"] + '</option>';
  }
  estparexpectedshockstable += "</select></td>";

  estparexpectedshockstable += '<td><input type="text" class="form-control no-border input-sm" name="estparvalue' + estparrowindex + '" id="estparvalue' + estparrowindex + '" placeholder="Value"></td>';
  estparexpectedshockstable += '<td><input type="text" class="form-control no-border input-sm" name="estparLbound' + estparrowindex + '" id="estparLbound' + estparrowindex + '" placeholder="L. Bound"></td>';
  estparexpectedshockstable += '<td><input type="text" class="form-control no-border input-sm" name="estparUbound' + estparrowindex + '" id="estparUbound' + estparrowindex + '" placeholder="U. Bound"></td>';

  estparexpectedshockstable += '<td><select id="estparprior' + estparrowindex + '" class="form-control input-sm" name="estparprior' + estparrowindex + '" onchange="enablebayesian(' + estparrowindex + ')"><option value="1" selected="selected">N/A</option><option value="2">beta_pdf</option><option value="3">gamma_pdf</option><option value="4">normal_pdf</option><option value="5">uniform_pdf</option><option value="6">inv_gamma_pdf</option><option value="7">inv_gamma1_pdf</option><option value="8">inv_gamma2_pdf</option><option value="9">weibull_pdf</option></td>';

  estparexpectedshockstable += '<td><input type="text" class="form-control no-border input-sm" name="estparpriormean' + estparrowindex + '" id="estparpriormean' + estparrowindex + '" placeholder="Prior mean" disabled></td>';
  estparexpectedshockstable += '<td><input type="text" class="form-control no-border input-sm" name="estparpriorstderr' + estparrowindex + '" id="estparpriorstderr' + estparrowindex + '" placeholder="Prior Std. Err" disabled></td>';

  estparexpectedshockstable += '<td><input type="text" class="form-control no-border input-sm" name="estparprior3par' + estparrowindex + '" id="estparprior3par' + estparrowindex + '" placeholder="Prior 3rd param" disabled></td>';
  estparexpectedshockstable += '<td><input type="text" class="form-control no-border input-sm" name="estparprior4par' + estparrowindex + '" id="estparprior4par' + estparrowindex + '" placeholder="Prior 4th param" disabled></td>';
  estparexpectedshockstable += '<td><input type="text" class="form-control no-border input-sm" name="estparpriorscale' + estparrowindex + '" id="estparpriorscale' + estparrowindex + '" placeholder="Scale" disabled></td>';

  exovarcounter = 999;
  estparexpectedshockstable += '<td><select id="estparcorrvar' + estparrowindex + '" class="form-control input-sm" name="estparcorrvar' + estparrowindex + '" disabled>';
  estparexpectedshockstable += '<option value="0">N/A</option>';
  for (x in dynaremodel.exogenous) {
    exovarcounter++;
    estparexpectedshockstable += '<option value="' + exovarcounter + '">' + dynaremodel.exogenous[x]["longName"] + '</option>';
  }
  estparexpectedshockstable += "</select></td>";
  estparexpectedshockstable += '<td><input type="button" value="X" onclick="estparSomeDeleteRowFunction(this,' + estparrowindex + ')" class="form-control no-border input-sm" name="estparremoveshock' + estparrowindex + '" id="estparremoveshock' + estparrowindex + '"></td></tr>';
  $('#estpartable tbody').append(estparexpectedshockstable);
  return false;
});

function estparSomeDeleteRowFunction(o, estparrowindex) {
  var p = o.parentNode.parentNode;
  p.parentNode.removeChild(p);
  estparnum[estparrowindex] = 0;
}

function enablebayesian(shockindex) {
  var selectedvalue = $("#estparprior" + shockindex).val();
  if (selectedvalue < 2) {
    $("#estparpriormean" + shockindex).attr('disabled', 'disabled');
    $("#estparpriorstderr" + shockindex).attr('disabled', 'disabled');
    $("#estparprior3par" + shockindex).attr('disabled', 'disabled');
    $("#estparprior4par" + shockindex).attr('disabled', 'disabled');
    $("#estparpriorscale" + shockindex).attr('disabled', 'disabled');
  } else {
    $("#estparpriormean" + shockindex).removeAttr('disabled');
    $("#estparpriorstderr" + shockindex).removeAttr('disabled');
    $("#estparprior3par" + shockindex).removeAttr('disabled');
    $("#estparprior4par" + shockindex).removeAttr('disabled');
    $("#estparpriorscale" + shockindex).removeAttr('disabled');
  }
}

function enableestvarcorr(shockindex) {
  var selectedvalue = $("#estparname" + shockindex).val();
  if (selectedvalue < 1000) {
    $("#estparcorrvar" + shockindex).val('0');
    $("#estparcorrvar" + shockindex).attr('disabled', 'disabled');
  } else {
    $("#estparcorrvar" + shockindex).removeAttr('disabled');
  }
}

//----------------------------------------------------@@@disable/unable estimation options
function onoffestimoption(thisnum) {

  if ($('#estimopt_input_' + thisnum.toString()).is(':disabled')) {
    //textbox is disabled
    $('#estimopt_input_' + thisnum.toString()).removeAttr('disabled');
    $('#estimopt_input_' + thisnum.toString()).val('');
  } else {
    $('#estimopt_input_' + thisnum.toString()).attr({
      'disabled': 'disabled'
    });
    $('#estimopt_input_' + thisnum.toString()).val('');
  }
}

function onoffestimoptionselect(thisnum) {

  if ($('#estimopt_input_' + thisnum.toString()).is(':disabled')) {
    //textbox is disabled
    $("#estimopt_input_" + thisnum.toString()).removeAttr('disabled');
    // $("#estimopt_input_" + thisnum.toString()).attr('disabled',false);
    // document.getElementById("estimopt_input_24").disabled = false;

  } else {
    $("#estimopt_input_" + thisnum.toString()).attr('disabled', 'disabled');
  }
}


$('#runestimation').click(async function () {


  var estendonum = $('#estmodel_endonum').val();
  var estparamnum = $('#estmodel_paramnum').val();

  // var datafilepath = $('#estfile');
  var datafilepath = document.getElementById("estfile");
  var estssstatus = $('#estmodelsteadystatestatus').val();
  var estssdescription = [];




  //checking input file and extension
  if (datafilepath.files.length == 0) {
    $('.nav-tabs a[href="#estimsetuptable"]').tab('show');
    popthis('Setup error.', 'You did not provide a data file. This action is mandatory.');
    return (0);
  } else {
    var filename = datafilepath.files[0].name;
    extension = filename.split('.').pop();
    var allowedextensions = ['m', 'mat', 'xls', 'xlsx', 'csv'];
    var extcheck = 0;
    allowedextensions.forEach(function (myextension) {
      if (extension == myextension) {
        extcheck = 1;
      }
    });
    if (extcheck == 0) {
      $('.nav-tabs a[href="#estimsetuptable"]').tab('show');
      popthis('Setup error.', 'The file you provided does not seem to be of the correct type. Please provide a file with one of the following extensions:.m, .mat, .xls, .xlsx, .csv.');
      return (0);
    }
  }


  // steady-state info check
  if (estssstatus == 99) {
    estsstext = $('#estSSblock').val();
    if (estsstext.length == 0) {
      $('.nav-tabs a[href="#estimsetuptable"]').tab('show');
      popthis('Setup error.', 'Please provide a way to compute the steady-state.');
      return (0);
    }
    if ($('#estissteadystateblock').is(":checked") == true) {
      var estsstype = 1;
    } else {
      var estsstype = 2;
    }
    estssdescription.push({
      'sstext': estsstext,
      'sstype': parseInt(estsstype, 10)
    });
  }



  // Observations table check

  //observables
  var obsglobalvaluescount = 0;
  var obsattributeflag = [];
  for (i = 0; i < estendonum; i++) {
    obsattributeflag[i] = 0;
  }
  var obsindex;
  var obsdescription = [];

  var obsrowCount = $('#estobstable tr').length - 1; // careful this counts the th row as a row, thus the -1

  if (obsrowCount > 0) {
    // there are shocks in the table
    obsglobalvaluescount = 1;

    var obsxcond = 0;
    var obsiter = 0;
    var obscountshocks = 0;

    while (obsxcond == 0) {
      // we iterate over global estobsnum that kept track of observable indexes in any order they were entered
      if (estobsnum[obsiter] == 1) {
        // if estobsnum identifies a valid row in the table, we start further checks
        obscountshocks = obscountshocks + 1;

        // We fetch shock infos
        obsindex = $('#estobsvarname' + obsiter.toString()).val();


        //we check if the same observable has already been declared
        if (obsattributeflag[obsindex] > 0) {
          $('.nav-tabs a[href="#estimsetuptable"]').tab('show');
          popthis('Setup error.', 'The observable variable on row ' + obscountshocks + ', has already been declared. Please enter each observable only once.');
          return (0);
        } else {
          obsattributeflag[obsindex] = 1;
        }



        obsdescription.push({
          'obsindex': parseInt(obsindex, 10)
        });

        //we count the number of this specific shock
        // shocksflag[shockindex] = shocksflag[shockindex] + 1;
      }
      if (obscountshocks == obsrowCount) {
        obsxcond = 1;
      }
      obsiter = obsiter + 1;
    }
  }


  // estimated params table check

  var parglobalvaluescount = 0;
  var parattributeflag = [];
  for (i = 0; i < estparamnum; i++) {
    parattributeflag[i] = 0;
  }
  var parindex;
  var pardescription = [];

  var parpairs = [];

  var parrowCount = $('#estpartable tr').length - 1; // careful this counts the th row as a row, thus the -1

  if (parrowCount > 0) {
    // there are parameters in the table
    parglobalvaluescount = 1;

    var parxcond = 0;
    var pariter = 0;
    var parcountshocks = 0;


    var bayesiancount = 0;
    var mlcount = 0;

    while (parxcond == 0) {
      // we iterate over global estparnum that kept track of shock indexes in any order they were entered
      if (estparnum[pariter] == 1) {
        // if estparnum identifies a valid row in the table, we start further checks
        parcountshocks = parcountshocks + 1;

        //we find the true row number of the current checked line in the table
        var partablerow = $('#estparname' + pariter.toString()).closest('tr');
        var partablerowindex = partablerow.index() + 1;


        // We fetch parameter infos
        parshockindex = $('#estparname' + pariter.toString()).val();


        parinivalue = $('#estparvalue' + pariter.toString()).val();
        parLbound = $('#estparLbound' + pariter.toString()).val();
        parUbound = $('#estparUbound' + pariter.toString()).val();
        parprior = $('#estparprior' + pariter.toString()).val();
        parpriormean = $('#estparpriormean' + pariter.toString()).val();
        parpriorstderr = $('#estparpriorstderr' + pariter.toString()).val();
        parprior3par = $('#estparprior3par' + pariter.toString()).val();
        parprior4par = $('#estparprior4par' + pariter.toString()).val();
        parpriorscale = $('#estparpriorscale' + pariter.toString()).val();
        // parcorrattributevalue = $("#estparcorrvar" + pariter.toString()).val();

        parinivalue = parinivalue.trim();
        parLbound = parLbound.trim();
        parUbound = parUbound.trim();
        parpriormean = parpriormean.trim();
        parpriorstderr = parpriorstderr.trim();
        parprior3par = parprior3par.trim();
        parprior4par = parprior4par.trim();
        parpriorscale = parpriorscale.trim();

        // parassoshockindex = -10;
        parassoshockindex = $('#estparcorrvar' + pariter.toString()).val();



        if (parshockindex > 999) {
          //we first validate the case of an exogenous variable been selected (and correlation issues)


          if (parassoshockindex > 0) { //if correlation has been selected

            if (parshockindex == parassoshockindex) {
              $('.nav-tabs a[href="#estimparams"]').tab('show');
              popthis('Variables error.', 'On row ' + partablerowindex + ', the correlation should be specified for separate variables.');
              return (0);
            }

            // we verify if correlation has already been specified for any two same shocks
            if (Number(parassoshockindex) > Number(parshockindex)) {
              if (parpairs.indexOf(parshockindex + '-' + parassoshockindex) == -1) {
                parpairs.push(parshockindex + '-' + parassoshockindex);
              } else {
                popthis('Variables error.', 'On row ' + partablerowindex + ', this covariance/correlation has already been specified between these variables.');
                return (0);
              }
            } else {
              if (parpairs.indexOf(parassoshockindex + '-' + parshockindex) == -1) {
                parpairs.push(parassoshockindex + '-' + parshockindex);
              } else {
                popthis('Variables error.', 'On row ' + partablerowindex + ', this covariance/correlation has already been specified between these variables.');
                return (0);
              }
            }

          }

        }

        //Standard check for all parameters/variables



        //Check if parameter declared twice
        if (parassoshockindex < 1000) {
          if (parattributeflag[parshockindex] > 0) {
            popthis('Pameters error.', 'On row ' + partablerowindex + ', You have already specified attributes for this parameter/variable. Please add only one row per parameter/variable.');
            return (0);
          } else {
            parattributeflag[parshockindex] = 1;
          }
        }

        //}

        var parvalueflag = 0;

        // we validate the shock value
        if (!(parinivalue == null || parinivalue == "")) {
          if (!(IsNumeric(parinivalue))) {
            if (!(parinivalue.toLowerCase() == "inf")) {
              popthis('Parameter error.', 'The parameter value on row ' + partablerowindex + ' is not a number or inf.');
              return (0);
            } else {
              parvalueflag = 1;
            }

          } else {
            parvalueflag = 1;
          }
        }

        // we validate the shock prior mean
        if (parprior > 1) {
          bayesiancount += 1;
          if (!(parpriormean == null || parpriormean == "")) {
            if (!(IsNumeric(parpriormean))) {
              if (!(parpriormean.toLowerCase() == "inf")) {
                popthis('Parameter error.', 'The parameter prior mean on row ' + partablerowindex + ' is not a number or inf.');
                return (0);
              } else {
                parvalueflag = 1;
              }
            } else {
              parvalueflag = 1;
            }
          }

          if (!(parpriorstderr == null || parpriorstderr == "")) {
            if (!(IsNumeric(parpriorstderr))) {
              if (!(parpriorstderr.toLowerCase() == "inf")) {
                popthis('Parameter error.', 'The prior stdErr value on row ' + partablerowindex + ' is not a number or inf.');
                return (0);
              }
            }
          } else {
            popthis('Parameter error.', 'You did not specify a prior stdErr for parameter on row ' + partablerowindex + '. This information is mandatory if you specified a prior shape.');
            return (0);
          }

          if (!(parprior3par == null || parprior3par == "")) {
            if (!(IsNumeric(parprior3par))) {
              if (!(parprior3par.toLowerCase() == "inf")) {
                popthis('Parameter error.', 'The prior 3rd param value on row ' + partablerowindex + ' is not a number or inf.');
                return (0);
              }
            }
          }

          if (!(parprior4par == null || parprior4par == "")) {
            if (!(IsNumeric(parprior4par))) {
              if (!(parprior4par.toLowerCase() == "inf")) {
                popthis('Parameter error.', 'The prior 4rd param value on row ' + partablerowindex + ' is not a number or inf.');
                return (0);
              }
            }
          }

          if (!(parpriorscale == null || parpriorscale == "")) {
            if (!(IsNumeric(parpriorscale))) {
              if (!(parpriorscale.toLowerCase() == "inf")) {
                popthis('Parameter error.', 'The prior scale value on row ' + partablerowindex + ' is not a number or inf.');
                return (0);
              }
            }
          }
        } else {
          mlcount += 1;
        }


        if (parvalueflag == 0) {
          popthis('Parameter error.', 'You did not specify a value or a prior mean for parameter on row ' + partablerowindex + '. One of these information is mandatory.');
          return (0);
        }


        if (!(parLbound == null || parLbound == "")) {
          if (!(IsNumeric(parLbound))) {
            if (!(parLbound.toLowerCase() == "inf")) {
              popthis('Parameter error.', 'The lower bound value on row ' + partablerowindex + ' is not a number or inf.');
              return (0);
            }
          }
        }

        if (!(parUbound == null || parUbound == "")) {
          if (!(IsNumeric(parUbound))) {
            if (!(parUbound.toLowerCase() == "inf")) {
              popthis('Parameter error.', 'The upper bound value on row ' + partablerowindex + ' is not a number or inf.');
              return (0);
            }
          }
        }



        if ((bayesiancount > 0) && (mlcount > 0)) {
          popthis('Parameter error.', 'Estimation must be either fully ML or fully Bayesian. Please declare only variables/parameters with priors or only variables/parameters without priors.');
          return (0);


        }




        pardescription.push({
          'parshockindex': parseInt(parshockindex, 10),
          'parinivalue': (parinivalue.toLowerCase() == 'inf') ? (parinivalue.toLowerCase()) : (parseFloat(parinivalue, 10)),
          'parLbound': (parLbound.toLowerCase() == 'inf') ? (parLbound.toLowerCase()) : (parseFloat(parLbound, 10)),
          'parUbound': (parUbound.toLowerCase() == 'inf') ? (parUbound.toLowerCase()) : (parseFloat(parUbound, 10)),
          'parprior': parseInt(parprior, 10),
          'parpriormean': (parpriormean.toLowerCase() == 'inf') ? (parpriormean.toLowerCase()) : (parseFloat(parpriormean, 10)),
          'parpriorstderr': (parpriorstderr.toLowerCase() == 'inf') ? (parpriorstderr.toLowerCase()) : (parseFloat(parpriorstderr, 10)),
          'parprior3par': (parprior3par.toLowerCase() == 'inf') ? (parprior3par.toLowerCase()) : (parseFloat(parprior3par, 10)),
          'parprior4par': (parprior4par.toLowerCase() == 'inf') ? (parprior4par.toLowerCase()) : (parseFloat(parprior4par, 10)),
          'parpriorscale': (parpriorscale.toLowerCase() == 'inf') ? (parpriorscale.toLowerCase()) : (parseFloat(parpriorscale, 10)),
          'parassoshockindex': parseInt(parassoshockindex, 10)
        });

        //we count the number of this specific shock
        // shocksflag[shockindex] = shocksflag[parshockindex] + 1;
      }
      if (parcountshocks == parrowCount) {
        parxcond = 1;
      }
      pariter = pariter + 1;
    }


  }


  if (obsglobalvaluescount == 0) {
    $('.nav-tabs a[href="#estimsetuptable"]').tab('show');
    popthis('Estimation input error.', 'You have submitted no observable variable(s) for the estimation. Please add observable variable(s) and retry.');
    return (0);
  }

  if (parglobalvaluescount == 0) {
    $('.nav-tabs a[href="#estimparams"]').tab('show');
    popthis('Estimation input error.', 'You have submitted no parameters/variables to be estimated. Please add observable variable(s) and retry.');
    return (0);
  }


  // options check
  var estoptionsdescription = [];
  var estimoptcounter = 0;
  var estimoptval;
  var estimoptdyn;
  var estim0checker = 0;

  for (let element of estimoptionslist) {

    if ($("#estimopt_id_" + estimoptcounter.toString()).is(':checked')) {

      if (element.estoptinputtype == 0) { //checking no input options
        estoptionsdescription.push({
          'estoptindex': parseInt(estimoptcounter, 10),
          'estoptdynarename': element.estoptdynarename,
          'estoptinputtype': parseInt(element.estoptinputtype, 10),
          'estoptvalue': ''
        });
      }

      if (element.estoptinputtype == 1) { //checking integer input options

        estimoptval = $('#estimopt_input_' + estimoptcounter.toString()).val();
        estimoptval = estimoptval.trim();
        estimoptdyn = (element.estoptdynarename).bold();

        if ((estimoptval == null || estimoptval == "")) {
          $('.nav-tabs a[href="#estimoptions"]').tab('show');
          popthis('Options error.', 'You have activated the ' + estimoptdyn + ' option without specifying a value. Please deactivate this option if you are not using it or want to use its default value.');
          return (0);
        } else {
          if (!(Number.isInteger(Number(estimoptval)))) {
            $('.nav-tabs a[href="#estimoptions"]').tab('show');
            popthis('Options error.', estimoptdyn + ' value should be an integer number.');
            return (0);
          }
          // if (Number(estimoptval) < 0) {
          //   $('.nav-tabs a[href="#estimoptions"]').tab('show');
          //   popthis('Options error.', estimoptdyn + ' value cannot be negative. Please deactivate this option to use the default value.');
          //   return (0);
          // }
        }

        estoptionsdescription.push({
          'estoptindex': parseInt(estimoptcounter, 10),
          'estoptdynarename': element.estoptdynarename,
          'estoptinputtype': parseInt(element.estoptinputtype, 10),
          'estoptvalue': parseInt(estimoptval, 10)
        });
      }


      if ((element.estoptinputtype == 2) || (element.estoptinputtype == 4)) { //checking double and double with 'e' input options

        estimoptval = $('#estimopt_input_' + estimoptcounter.toString()).val();
        estimoptval = estimoptval.trim();
        estimoptdyn = (element.estoptdynarename).bold();

        if ((estimoptval == null || estimoptval == "")) {
          $('.nav-tabs a[href="#estimoptions"]').tab('show');
          popthis('Options error.', 'You have activated the ' + estimoptdyn + ' option without specifying a value. Please deactivate this option if you are not using it or want to use its default value.');
          return (0);
        } else {
          if (!(IsNumeric(estimoptval))) {
            $('.nav-tabs a[href="#estimoptions"]').tab('show');
            popthis('Options error.', estimoptdyn + ' value should be a number.');
            return (0);
          }
        }

        estoptionsdescription.push({
          'estoptindex': parseInt(estimoptcounter, 10),
          'estoptdynarename': element.estoptdynarename,
          'estoptinputtype': parseInt(element.estoptinputtype, 10),
          'estoptvalue': parseFloat(estimoptval, 10)
        });
      }


      if ((element.estoptinputtype == 10) || (element.estoptinputtype == 11)) { //checking integers, range of integers or array of integers

        estimoptval = $('#estimopt_input_' + estimoptcounter.toString()).val();
        estimoptval = estimoptval.trim();
        estimoptdyn = (element.estoptdynarename).bold();

        if ((estimoptval == null || estimoptval == "")) { //entry is empty
          $('.nav-tabs a[href="#estimoptions"]').tab('show');
          popthis('Options error.', 'You have activated the ' + estimoptdyn + ' option without specifying a value. Please deactivate this option if you are not using it or want to use its default value.');
          return (0);
        } else {

          if ((IsNumeric(estimoptval))) { //we verify if entry is a number

            if (!(Number.isInteger(Number(estimoptval)))) { //if it's a number it has to be a single integer
              $('.nav-tabs a[href="#estimoptions"]').tab('show');
              popthis('Options error.', estimoptdyn + ' value should be an integer number.');
              return (0);
            }

            estoptionsdescription.push({
              'estoptindex': parseInt(estimoptcounter, 10),
              'estoptdynarename': element.estoptdynarename,
              'estoptinputtype': 1,
              'estoptvalue': parseInt(estimoptval, 10)
            });

          } else { //if not a number we verify if it is a range or an array of numbers

            if ((estimoptval.indexOf(':') > -1) || (estimoptval.indexOf(' ') > -1)) {

              if (estimoptval.indexOf(':') > -1) { //it is a range
                var estimoptsplit = estimoptval.split(':');
                var estimoptsplitlength = estimoptsplit.length;

                if (estimoptsplitlength == 2) { //the range should divide into two numbers

                  if (!(Number.isInteger(Number(estimoptsplit[0])))) { //if it's a number it has to be a single integer
                    $('.nav-tabs a[href="#estimoptions"]').tab('show');
                    popthis('Options error.', estimoptdyn + ' range values should be integer numbers.');
                    return (0);
                  }

                  if (!(Number.isInteger(Number(estimoptsplit[1])))) { //if it's a number it has to be a single integer
                    $('.nav-tabs a[href="#estimoptions"]').tab('show');
                    popthis('Options error.', estimoptdyn + ' range values should be integer numbers.');
                    return (0);
                  }
                  estoptionsdescription.push({
                    'estoptindex': parseInt(estimoptcounter, 10),
                    'estoptdynarename': element.estoptdynarename,
                    'estoptinputtype': 10,
                    'estoptvalue': '[' + estimoptsplit[0] + ':' + estimoptsplit[1] + ']'
                  });
                } else {
                  $('.nav-tabs a[href="#estimoptions"]').tab('show');
                  popthis('Options error.', estimoptdyn + ' value is not properly entered.');
                  return (0);
                }
              }

              if (estimoptval.indexOf(' ') > -1) { //it is an array

                var estimoptsplit = estimoptval.split(' ');
                var estimoptsplitlength = estimoptsplit.length;

                for (i = 0; i < estimoptsplitlength; i++) {
                  if (!(Number.isInteger(Number(estimoptsplit[i])))) { //if it's a number it has to be a single integer
                    $('.nav-tabs a[href="#estimoptions"]').tab('show');
                    popthis('Options error.', estimoptdyn + ' array values should be integer numbers.');
                    return (0);
                  }
                }
                estoptionsdescription.push({
                  'estoptindex': parseInt(estimoptcounter, 10),
                  'estoptdynarename': element.estoptdynarename,
                  'estoptinputtype': 11,
                  'estoptvalue': '[' + estimoptval + ']'
                });
              }
            } else {
              $('.nav-tabs a[href="#estimoptions"]').tab('show');
              popthis('Options error.', estimoptdyn + ' value is not in the accepted value types. Please check the entry and the documentation.');
              return (0);
            }
          }
        }
      }



      if (element.estoptinputtype == 21) { //checking array of double

        estimoptval = $('#estimopt_input_' + estimoptcounter.toString()).val();
        estimoptval = estimoptval.trim();
        estimoptdyn = (element.estoptdynarename).bold();

        if ((estimoptval == null || estimoptval == "")) { //entry is empty
          $('.nav-tabs a[href="#estimoptions"]').tab('show');
          popthis('Options error.', 'You have activated the ' + estimoptdyn + ' option without specifying a value. Please deactivate this option if you are not using it or want to use its default value.');
          return (0);
        } else {

          // if ((IsNumeric(estimoptval))) { //we verify if entry is a number

          //   estoptionsdescription.push({
          //     'estoptindex': parseInt(estimoptcounter, 10),
          //     'estoptdynarename': element.estoptdynarename,
          //     'estoptinputtype': 2,
          //     'estoptvalue': parseInt(estimoptval, 10)
          //   });


          // } else { //if not a number we verify if it is a range or an array of numbers

          if (estimoptval.indexOf(' ') > -1) {

            var estimoptsplit = estimoptval.split(' ');
            var estimoptsplitlength = estimoptsplit.length;

            for (i = 0; i < estimoptsplitlength; i++) {
              if (!(IsNumeric(estimoptsplit[i]))) { //if it's a number it has to be a single integer
                $('.nav-tabs a[href="#estimoptions"]').tab('show');
                popthis('Options error.', estimoptdyn + ' array values should be double numbers.');
                return (0);
              }
            }
            estoptionsdescription.push({
              'estoptindex': parseInt(estimoptcounter, 10),
              'estoptdynarename': element.estoptdynarename,
              'estoptinputtype': 21,
              'estoptvalue': '[' + estimoptval + ']'
            });
          } else {
            $('.nav-tabs a[href="#estimoptions"]').tab('show');
            popthis('Options error.', estimoptdyn + ' value is not in the accepted value types (array of doubles). Please check the entry and the documentation.');
            return (0);
          }
          // }
        }
      }


      if (element.estoptinputtype == 5) { //checking list type specific options

        var estimoptindex = $('#estimopt_input_' + estimoptcounter.toString()).val();
        estimoptval = $("#estimopt_input_" + estimoptcounter.toString() + " option:selected").text();
        estimoptdyn = (element.estoptdynarename).bold();

        if (estimoptindex == null) {
          $('.nav-tabs a[href="#estimoptions"]').tab('show');
          popthis('Options error.', 'You have activated the ' + estimoptdyn + ' option without selection a value. Please deactivate this option if you are not using it or want to use its default value.');
          return (0);
        } else {

          estoptionsdescription.push({
            'estoptindex': parseInt(estimoptcounter, 10),
            'estoptdynarename': element.estoptdynarename,
            'estoptinputtype': 5,
            'estoptvalue': estimoptval
          });
        }


      }


      if (element.estoptinputtype == 60) { //saving non checked types

        estimoptval = $('#estimopt_input_' + estimoptcounter.toString()).val();
        estimoptval = estimoptval.trim();
        estimoptdyn = (element.estoptdynarename).bold();


        if ((estimoptval == null || estimoptval == "")) { //entry is empty
          $('.nav-tabs a[href="#estimoptions"]').tab('show');
          popthis('Options error.', 'You have activated the ' + estimoptdyn + ' option without specifying a value. Please deactivate this option if you are not using it or want to use its default value.');
          return (0);
        } else {


          if (estimoptcounter == 0) { //checking specifically the "endogenous variables behind the estimation command"
            estim0checker = 1;

            if (estimoptval.indexOf(' ') > -1) { //an array of values has been entered, we check if they are endogenous vars

              var estimoptsplit = estimoptval.split(' ');
              var estimoptsplitlength = estimoptsplit.length;

              var estendolist = '';
              for (i = 0; i < estimoptsplitlength; i++) {
                var endocheckflag = 0;
                for (x in dynaremodel.endogenous) {
                  if (dynaremodel.endogenous[x]["longName"] == estimoptsplit[i]) {
                    endocheckflag = 1;
                  }
                }
                if (endocheckflag == 0) {
                  $('.nav-tabs a[href="#estimoptions"]').tab('show');
                  popthis('Options error.', estimoptdyn + ' values are not among the endogenous variables of the model.');
                  return (0);
                } else {
                  estendolist += estimoptsplit[i] + ' ';
                }
              }

              estoptionsdescription.push({
                'estoptindex': parseInt(estimoptcounter, 10),
                'estoptdynarename': element.estoptdynarename,
                'estoptinputtype': 60,
                'estoptvalue': estimoptval
              });
            } else {

              if (estimoptval == 'varobs') { //We list all endogenous observables behind the estimation command
                var estendolist = '';
                for (let obsiter of obsdescription) {
                  estendolist += dynaremodel.endogenous[obsiter.obsindex]["longName"] + ' ';
                }
                estoptionsdescription.push({
                  'estoptindex': parseInt(estimoptcounter, 10),
                  'estoptdynarename': element.estoptdynarename,
                  'estoptinputtype': 60,
                  'estoptvalue': estendolist
                });
              } else {

                //we check for a single endogenous entry
                var endocheckflag = 0;
                for (x in dynaremodel.endogenous) {
                  if (dynaremodel.endogenous[x]["longName"] == estimoptval) {
                    endocheckflag = 1;
                  }
                }

                if (endocheckflag == 0) {
                  $('.nav-tabs a[href="#estimoptions"]').tab('show');
                  popthis('Options error.', estimoptdyn + ' value(s) are not among the endogenous variables of the model.');
                  return (0);
                } else {
                  estoptionsdescription.push({
                    'estoptindex': parseInt(estimoptcounter, 10),
                    'estoptdynarename': element.estoptdynarename,
                    'estoptinputtype': 60,
                    'estoptvalue': estimoptval
                  });
                }

              }
            }

          } else {
            estoptionsdescription.push({
              'estoptindex': parseInt(estimoptcounter, 10),
              'estoptdynarename': element.estoptdynarename,
              'estoptinputtype': 60,
              'estoptvalue': '(' + estimoptval + ')'
            });
          }
        }
      }



    }
    estimoptcounter++;
  }


  if (estim0checker == 0) { //we add the list of all endo vars to "endogenous variables" option if the user did not activate it

    var estendolist = '';
    for (x in dynaremodel.endogenous) {
      estendolist += dynaremodel.endogenous[x]["longName"] + ' ';
    }
    estoptionsdescription.push({
      'estoptindex': 0,
      'estoptdynarename': 'endogenous_variables',
      'estoptinputtype': 60,
      'estoptvalue': estendolist
    });
  }


  //we post to the server

  //getting the file to be sent

  var fd = new FormData();
  var re = /(?:\.([^.]+))?$/;
  fd.append("file", document.getElementById('estfile').files[0]);
  fd.append("filename", $('#codehash').val());
  fd.append("filetype", re.exec(document.getElementById('estfile').files[0].name)[1]);

  var estimationval = {
    'modelhash': $('#codehash').val(),
    'ssstatus': parseInt(estssstatus, 10),
    'ssdescription': estssdescription,
    'obsdescription': obsdescription,
    'pardescription': pardescription,
    'optionsdescription': estoptionsdescription,
    'datafiletype': re.exec(document.getElementById('estfile').files[0].name)[1],
    'accesstype': 'runestimation'
  };

  // var estimationvaldata = JSON.stringify(estimationval);
  $("#outputtable").empty();
  $("#outputtable2").empty();
  $("#plotbtn").empty();
  $("#plotbox").empty();
  $('#estimModal').modal('hide');
  var div = document.createElement('div');
  div.className = 'loader';
  div.innerHTML = '';
  document.getElementById('inner').appendChild(div);
  $('.nav-tabs a[href="#console"]').tab('show');

  // calling the 'node side'
  try {
    var resp = await runsaveestimationfilenode(fd);
  } catch (error) {
    if (error.status == 2) {
      $('.nav-tabs a[href="#console"]').tab('show');
      $("#inner").empty();
      document.getElementById("inner").innerHTML = '<p style="color:red;">Matlab errors found. See console.</p>';
      return false;

    } else if (error.status == 3) {
      currentout = currentout + resp.data.message;
      $("textarea#preprocessorout").val(currentout + '\n \n The preprocessor could nor process your input. Please check the extra steady-state information you provided.');
      $('.nav-tabs a[href="#console"]').tab('show');
      $("#inner").empty();
      document.getElementById("inner").innerHTML = '<p style="color:red;">Preprocessor errors found. See console.</p>';
      return false;
    }
  }

  try {
    var resp = await runestimationnode(JSON.stringify(estimationval));
  } catch (error) {
    console.log(error)
    if (error.status == 2) {
      $('.nav-tabs a[href="#console"]').tab('show');
      $("#inner").empty();
      document.getElementById("inner").innerHTML = '<p style="color:red;">Matlab errors found. See console.</p>';
      return false;

    } else if (error.status == 3) {
      currentout = currentout + resp.data.message;
      $("textarea#preprocessorout").val(currentout + '\n \n The preprocessor could nor process your input. Please check the extra steady-state information you provided.');
      $('.nav-tabs a[href="#console"]').tab('show');
      $("#inner").empty();
      document.getElementById("inner").innerHTML = '<p style="color:red;">Preprocessor errors found. See console.</p>';
      return false;
    }
  }



  return false;



});



ipcest.on('estimationfinish', (event, arg) => {

  async function finishestimation() {


    scrolltextarea();
    $('#MHmodal').modal('hide');
    $('#BImodal').modal('hide');
    $('#SMmodal').modal('hide');

    $('.nav-tabs a[href="#output"]').tab('show');

    try {
      estmatlabdata = await loadjsonfile('estimout.JSON');
    } catch (error) {
      return popthis('Error', 'Unable to open estimout.JSON to process:' + error);
    }


    if (estmatlabdata.estimtypeflag == 0) {
      //Reporting ML estimation

      var estbl1 = `
        <section>
        <h2>Results from Maximum Likelihood Estimation</h2>
        <table class="table table-striped table-hover">
        <thead>
        <tr>
        <th>#</th>
        <th>Variable/parameter</th>
        <th>Estimate</th>
        <th>Std. Dev</th>
        <th>t-stat</th>
        </tr>
        </thead>
        <tbody>
        `;



      if ("parameters" in estmatlabdata.mle_mode) {
        estbl1 += '<tr><th id="par" colspan="5" scope="colgroup">Parameters</th></tr>';
        y = 0;
        for (x in estmatlabdata.mle_mode.parameters) {
          y = y + 1;
          estbl1 += '<tr><th scope="row">' + y + '</th>';

          estbl1 += "<td>" + x + "</td>";
          estbl1 += "<td>" + estmatlabdata.mle_mode.parameters[x] + "</td>";
          estbl1 += "<td>" + estmatlabdata.mle_std_at_mode.parameters[x] + "</td>";
          estbl1 += "<td></td></tr>";
        }
      }

      if ("shocks_std" in estmatlabdata.mle_mode) {
        estbl1 += '<tr><th id="par" colspan="5" scope="colgroup">Std. Dev. of shocks</th></tr>';
        y = 0;
        for (x in estmatlabdata.mle_mode.shocks_std) {
          y = y + 1;
          estbl1 += '<tr><th scope="row">' + y + '</th>';

          estbl1 += "<td>" + x + "</td>";
          estbl1 += "<td>" + estmatlabdata.mle_mode.shocks_std[x] + "</td>";
          estbl1 += "<td>" + estmatlabdata.mle_std_at_mode.shocks_std[x] + "</td>";
          estbl1 += "<td></td></tr>";
        }
      }

      if ("shocks_corr" in estmatlabdata.mle_mode) {
        estbl1 += '<tr><th id="par" colspan="5" scope="colgroup">Correlation of shocks</th></tr>';
        y = 0;
        for (x in estmatlabdata.mle_mode.shocks_corr) {
          y = y + 1;
          estbl1 += '<tr><th scope="row">' + y + '</th>';

          estbl1 += "<td>" + x + "</td>";
          estbl1 += "<td>" + estmatlabdata.mle_mode.shocks_corr[x] + "</td>";
          estbl1 += "<td>" + estmatlabdata.mle_std_at_mode.shocks_corr[x] + "</td>";
          estbl1 += "<td></td></tr>";
        }
      }


      estbl1 += "</tbody></table></section>";


      var plotdropdown = `
    <section>
    <h2>Plots</h2>
    <div class="btn-group" role="group" aria-label="...">
    <div class="btn-group" role="group">
      <button id="plotbtn" class="btn btn-primary btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Select graph <span class="caret"></span><span class="sr-only">Toggle Dropdown</span>
      </button>
      <ul class="dropdown-menu" aria-labelledby="plotbtn">`;

      y = 0;
      if ("SmoothedShocks" in estmatlabdata) {
        plotdropdown += '<li class="dropdown-header">Smoothed shocks:</li>';
        for (x in estmatlabdata.SmoothedShocks) {
          plotdropdown += '<li onclick="estplotsmoothedshocks(&quot;' + x + '&quot;)"><a href="#stophere">' + x + '</a></li>';
          if (y == 0) {
            var firstname = x;
            y = 1;
          }
        }
      }



      if ("SmoothedObs" in estmatlabdata) {
        plotdropdown += '<li class="dropdown-header">Historical and smoothed variables:</li>';
        for (x in estmatlabdata.SmoothedObs) {
          plotdropdown += '<li onclick="estplotsmoothedvars(&quot;' + x + '&quot;)"><a href="#stophere">' + x + '</a></li>';
        }
      }

      //non-MH forecast mean
      if ("forecastvarmean" in estmatlabdata) {
        plotdropdown += '<li class="dropdown-header">Forecasts:</li>';
        for (x in estmatlabdata.forecastvarmean) {
          plotdropdown += '<li onclick="estplotforecastvar(&quot;' + x + '&quot;)"><a href="#stophere">' + x + '</a></li>';
        }
      }

      plotdropdown += '</ul></div>';
      plotdropdown += `
    <div class="btn-group" role="group">
    <button id="exportbtn" class="btn btn-primary btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Export data <span class="caret"></span><span class="sr-only">Toggle Dropdown</span>
    </button>
    <ul class="dropdown-menu" aria-labelledby="exportbtn">
    <li class="dropdown-header">Select file type:</li>
    <li onclick="exportfile(&quot;json&quot;)"><a href="#stophere">Export to JSON</a></li>
    <li onclick="exportfile(&quot;csv&quot;)"><a href="#stophere">Export to CSV</a></li>
    </ul>
    </div>`;

      plotdropdown += `
      </div>
      </section>
      `;
      document.getElementById("plotbtn").innerHTML = plotdropdown;


      estplotsmoothedshocks(firstname);

    }

    var priorname = ['dummy', 'beta_pdf', 'gamma_pdf', 'normal_pdf', 'inv_gamma_pdf', 'uniform_pdf', 'inv_gamma2_pdf', 'dummy', 'weibull_pdf'];

    if (estmatlabdata.estimtypeflag == 1) {
      //Reporting Bayesian estimation


      var estbl1 = `
      <section>
      <h2>Results from Posterior Estimation</h2>
      <table class="table table-striped table-hover">
      <thead>
      <tr>
      <th>#</th>
      <th>Variable/parameter</th>
      <th>Prior mean</th>
      <th>Mode</th>
      <th>Std. Dev</th>
      <th>Prior</th>
      <th>Pstdev</th>
      </tr>
      </thead>
      <tbody>
      `;


      if ("param_vals" in estmatlabdata) {
        estbl1 += '<tr><th id="par" colspan="7" scope="colgroup">Parameters</th></tr>';
        y = 0;
        for (x in estmatlabdata.posterior_mode.parameters) {
          estbl1 += '<tr><th scope="row">' + (y + 1) + '</th>';
          estbl1 += "<td>" + x + "</td>";

          if (Array.isArray(estmatlabdata.param_vals[0])) {
            estbl1 += "<td>" + estmatlabdata.param_vals[y][5] + "</td>";
          } else {
            estbl1 += "<td>" + estmatlabdata.param_vals[5] + "</td>";
          }

          estbl1 += "<td>" + estmatlabdata.posterior_mode.parameters[x] + "</td>";
          estbl1 += "<td>" + estmatlabdata.posterior_std.parameters[x] + "</td>";
          if (Array.isArray(estmatlabdata.param_vals[0])) {
            estbl1 += "<td>" + priorname[estmatlabdata.param_vals[y][4]] + "</td>";
            estbl1 += "<td>" + estmatlabdata.param_vals[y][6] + "</td>";
          } else {
            estbl1 += "<td>" + priorname[estmatlabdata.param_vals[4]] + "</td>";
            estbl1 += "<td>" + estmatlabdata.param_vals[6] + "</td>";
          }
          estbl1 += "<td></td></tr>";
          y = y + 1;
        }
      }

      if ("var_exo" in estmatlabdata) {
        estbl1 += '<tr><th id="par" colspan="7" scope="colgroup">Standard deviation of shocks</th></tr>';
        y = 0;
        for (x in estmatlabdata.posterior_mode.shocks_std) {
          estbl1 += '<tr><th scope="row">' + (y + 1) + '</th>';
          estbl1 += "<td>" + x + "</td>";

          if (Array.isArray(estmatlabdata.var_exo[0])) {
            estbl1 += "<td>" + estmatlabdata.var_exo[y][5] + "</td>";
          } else {
            estbl1 += "<td>" + estmatlabdata.var_exo[5] + "</td>";
          }
          estbl1 += "<td>" + estmatlabdata.posterior_mode.shocks_std[x] + "</td>";
          estbl1 += "<td>" + estmatlabdata.posterior_std.shocks_std[x] + "</td>";
          if (Array.isArray(estmatlabdata.var_exo[0])) {
            estbl1 += "<td>" + priorname[estmatlabdata.var_exo[y][4]] + "</td>";
            estbl1 += "<td>" + estmatlabdata.var_exo[y][6] + "</td>";
          } else {
            estbl1 += "<td>" + priorname[estmatlabdata.var_exo[4]] + "</td>";
            estbl1 += "<td>" + estmatlabdata.var_exo[6] + "</td>";
          }
          estbl1 += "<td></td></tr>";
          y = y + 1;
        }
      }

      if ("corrx" in estmatlabdata) {
        estbl1 += '<tr><th id="par" colspan="7" scope="colgroup">Correlation of shocks</th></tr>';
        y = 0;
        for (x in estmatlabdata.posterior_mode.shocks_corr) {
          estbl1 += '<tr><th scope="row">' + (y + 1) + '</th>';
          estbl1 += "<td>" + x + "</td>";

          if (Array.isArray(estmatlabdata.corrx[0])) {
            estbl1 += "<td>" + estmatlabdata.corrx[y][6] + "</td>";
          } else {
            estbl1 += "<td>" + estmatlabdata.corrx[6] + "</td>";
          }
          estbl1 += "<td>" + estmatlabdata.posterior_mode.shocks_corr[x] + "</td>";
          estbl1 += "<td>" + estmatlabdata.posterior_std.shocks_corr[x] + "</td>";
          if (Array.isArray(estmatlabdata.corrx[0])) {
            estbl1 += "<td>" + priorname[estmatlabdata.corrx[y][5]] + "</td>";
            estbl1 += "<td>" + estmatlabdata.corrx[y][7] + "</td>";
          } else {
            estbl1 += "<td>" + priorname[estmatlabdata.corrx[5]] + "</td>";
            estbl1 += "<td>" + estmatlabdata.corrx[7] + "</td>";
          }
          estbl1 += "<td></td></tr>";
          y = y + 1;
        }
      }

      estbl1 += "</tbody></table></section>";



      var estbl2 = `
      <section>
      <h2>Estimation Results</h2>
      <table class="table table-striped table-hover">
      <thead>
      <tr>
      <th>#</th>
      <th>Variable/parameter</th>
      <th>Prior mean</th>
      <th>Post. mean</th>
      <th>90% HPD int. (inf)</th>
      <th>90% HPD int. (sup)</th>
      <th>Prior</th>
      <th>Pstdev</th>
      </tr>
      </thead>
      <tbody>
      `;

      if ("param_vals" in estmatlabdata) {
        estbl2 += '<tr><th id="par" colspan="8" scope="colgroup">Parameters</th></tr>';
        y = 0;
        for (x in estmatlabdata.posterior_mean.parameters) {
          estbl2 += '<tr><th scope="row">' + (y + 1) + '</th>';
          estbl2 += "<td>" + x + "</td>";

          if (Array.isArray(estmatlabdata.param_vals[0])) {
            estbl2 += "<td>" + estmatlabdata.param_vals[y][5] + "</td>";
          } else {
            estbl2 += "<td>" + estmatlabdata.param_vals[5] + "</td>";
          }
          estbl2 += "<td>" + estmatlabdata.posterior_mean.parameters[x] + "</td>";
          estbl2 += "<td>" + estmatlabdata.posterior_hpdinf.parameters[x] + "</td>";
          estbl2 += "<td>" + estmatlabdata.posterior_hpdsup.parameters[x] + "</td>";

          if (Array.isArray(estmatlabdata.param_vals[0])) {
            estbl2 += "<td>" + priorname[estmatlabdata.param_vals[y][4]] + "</td>";
            estbl2 += "<td>" + estmatlabdata.param_vals[y][6] + "</td>";
          } else {
            estbl2 += "<td>" + priorname[estmatlabdata.param_vals[4]] + "</td>";
            estbl2 += "<td>" + estmatlabdata.param_vals[6] + "</td>";
          }
          estbl2 += "<td></td></tr>";
          y = y + 1;
        }
      }

      if ("var_exo" in estmatlabdata) {
        estbl2 += '<tr><th id="par" colspan="8" scope="colgroup">Standard deviation of shocks</th></tr>';
        y = 0;
        for (x in estmatlabdata.posterior_mean.shocks_std) {
          estbl2 += '<tr><th scope="row">' + (y + 1) + '</th>';
          estbl2 += "<td>" + x + "</td>";
          // estbl2+="<td>"+estmatlabdata.exo_names[y]+"</td>";
          if (Array.isArray(estmatlabdata.var_exo[0])) {
            estbl2 += "<td>" + estmatlabdata.var_exo[y][5] + "</td>";
          } else {
            estbl2 += "<td>" + estmatlabdata.var_exo[5] + "</td>";
          }

          estbl2 += "<td>" + estmatlabdata.posterior_mean.shocks_std[x] + "</td>";
          estbl2 += "<td>" + estmatlabdata.posterior_hpdinf.shocks_std[x] + "</td>";
          estbl2 += "<td>" + estmatlabdata.posterior_hpdsup.shocks_std[x] + "</td>";

          if (Array.isArray(estmatlabdata.var_exo[0])) {
            estbl2 += "<td>" + priorname[estmatlabdata.var_exo[y][4]] + "</td>";
            estbl2 += "<td>" + estmatlabdata.var_exo[y][6] + "</td>";
          } else {
            estbl2 += "<td>" + priorname[estmatlabdata.var_exo[4]] + "</td>";
            estbl2 += "<td>" + estmatlabdata.var_exo[6] + "</td>";
          }
          estbl2 += "<td></td></tr>";
          y = y + 1;
        }
      }

      if ("corrx" in estmatlabdata) {
        estbl2 += '<tr><th id="par" colspan="8" scope="colgroup">Correlation of shocks</th></tr>';
        y = 0;
        for (x in estmatlabdata.posterior_mean.shocks_corr) {
          estbl2 += '<tr><th scope="row">' + (y + 1) + '</th>';
          estbl2 += "<td>" + x + "</td>";
          // estbl2+="<td>"+estmatlabdata.exo_names[y]+"</td>";
          if (Array.isArray(estmatlabdata.corrx[0])) {
            estbl2 += "<td>" + estmatlabdata.corrx[y][6] + "</td>";
          } else {
            estbl2 += "<td>" + estmatlabdata.corrx[6] + "</td>";
          }

          estbl2 += "<td>" + estmatlabdata.posterior_mean.shocks_corr[x] + "</td>";
          estbl2 += "<td>" + estmatlabdata.posterior_hpdinf.shocks_corr[x] + "</td>";
          estbl2 += "<td>" + estmatlabdata.posterior_hpdsup.shocks_corr[x] + "</td>";

          if (Array.isArray(estmatlabdata.corrx[0])) {
            estbl2 += "<td>" + priorname[estmatlabdata.corrx[y][5]] + "</td>";
            estbl2 += "<td>" + estmatlabdata.corrx[y][7] + "</td>";
          } else {
            estbl2 += "<td>" + priorname[estmatlabdata.corrx[5]] + "</td>";
            estbl2 += "<td>" + estmatlabdata.corrx[7] + "</td>";
          }
          estbl2 += "<td></td></tr>";
          y = y + 1;
        }
      }

      estbl2 += "</tbody></table></section>";


      var plotdropdown = `
    <section>
    <h2>Plots</h2>
    <div class="btn-group" role="group" aria-label="...">
    <div class="btn-group" role="group">
      <button id="plotbtn" class="btn btn-primary btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Select graph <span class="caret"></span><span class="sr-only">Toggle Dropdown</span>
      </button>
      <ul class="dropdown-menu" aria-labelledby="plotbtn">`;

      //plotting priors
      y = 0;

      if ("prior_density" in estmatlabdata) {

        plotdropdown += '<li class="dropdown-header">Priors:</li>';

        if ("parameters" in estmatlabdata.prior_density) {
          for (x in estmatlabdata.prior_density.parameters) {
            plotdropdown += '<li onclick="estplotpriors(&quot;' + x + '&quot;,0)"><a href="#stophere">' + x + '</a></li>';
            if (y == 0) {
              var firstname = x;
              var firsttype = 0;
              y = 1;
            }
          }
        }

        if ("shocks_std" in estmatlabdata.prior_density) {
          for (x in estmatlabdata.prior_density.shocks_std) {
            plotdropdown += '<li onclick="estplotpriors(&quot;' + x + '&quot;,1)"><a href="#stophere">' + x + '</a></li>';
            if (y == 0) {
              var firstname = x;
              var firsttype = 1;
              y = 1;
            }
          }
        }

        if ("shocks_corr" in estmatlabdata.prior_density) {
          for (x in estmatlabdata.prior_density.shocks_corr) {
            plotdropdown += '<li onclick="estplotpriors(&quot;' + x + '&quot;,2)"><a href="#stophere">' + x + '</a></li>';
          }
        }
      }

      //plotting priors+posteriors


      if ("posterior_density" in estmatlabdata) {

        plotdropdown += '<li class="dropdown-header">Priors and posteriors:</li>';

        if ("parameters" in estmatlabdata.posterior_density) {
          for (x in estmatlabdata.posterior_density.parameters) {
            plotdropdown += '<li onclick="estplotposteriors(&quot;' + x + '&quot;,0)"><a href="#stophere">' + x + '</a></li>';
          }
        }

        if ("shocks_std" in estmatlabdata.posterior_density) {
          for (x in estmatlabdata.posterior_density.shocks_std) {
            plotdropdown += '<li onclick="estplotposteriors(&quot;' + x + '&quot;,1)"><a href="#stophere">' + x + '</a></li>';
          }
        }

        if ("shocks_corr" in estmatlabdata.posterior_density) {
          for (x in estmatlabdata.posterior_density.shocks_corr) {
            plotdropdown += '<li onclick="estplotposteriors(&quot;' + x + '&quot;,2)"><a href="#stophere">' + x + '</a></li>';
          }
        }

      }



      if ("SmoothedShocks" in estmatlabdata) {
        plotdropdown += '<li class="dropdown-header">Smoothed shocks:</li>';
        for (x in estmatlabdata.SmoothedShocks) {
          plotdropdown += '<li onclick="estplotsmoothedshocks(&quot;' + x + '&quot;)"><a href="#stophere">' + x + '</a></li>';
        }
      }



      if ("SmoothedObs" in estmatlabdata) {
        plotdropdown += '<li class="dropdown-header">Historical and smoothed variables:</li>';
        for (x in estmatlabdata.SmoothedObs) {
          plotdropdown += '<li onclick="estplotsmoothedvars(&quot;' + x + '&quot;)"><a href="#stophere">' + x + '</a></li>';
        }
      }

      //bayesian_irf option plots
      if ("bayesianirfmean" in estmatlabdata) {
        plotdropdown += '<li class="dropdown-header">Posterior distribution of IRFs:</li>';
        for (x in estmatlabdata.bayesianirfmean) {
          plotdropdown += '<li onclick="estplotbayesianirfs(&quot;' + x + '&quot;)"><a href="#stophere">' + x + '</a></li>';
        }

      }

      //smoother option plots
      if ("smoothedvarmean" in estmatlabdata) {
        plotdropdown += '<li class="dropdown-header">Smoothed variables:</li>';
        for (x in estmatlabdata.smoothedvarmean) {
          plotdropdown += '<li onclick="estplotsmoothvar(&quot;' + x + '&quot;)"><a href="#stophere">' + x + '</a></li>';
        }
      }

      if ("smoothedshocksmean" in estmatlabdata) {
        plotdropdown += '<li class="dropdown-header">Smoothed shocks:</li>';
        for (x in estmatlabdata.smoothedshocksmean) {
          plotdropdown += '<li onclick="estplotsmoothshocks(&quot;' + x + '&quot;)"><a href="#stophere">' + x + '</a></li>';
        }
      }

      if ("constantmean" in estmatlabdata) {
        plotdropdown += '<li class="dropdown-header">Smoothed constant:</li>';
        for (x in estmatlabdata.constantmean) {
          plotdropdown += '<li onclick="estplotsmoothconst(&quot;' + x + '&quot;)"><a href="#stophere">' + x + '</a></li>';
        }
      }

      if ("updatedvarmean" in estmatlabdata) {
        plotdropdown += '<li class="dropdown-header">Updated variables:</li>';
        for (x in estmatlabdata.updatedvarmean) {
          plotdropdown += '<li onclick="estplotupdatedvars(&quot;' + x + '&quot;)"><a href="#stophere">' + x + '</a></li>';
        }
      }


      //forecast option plots
      //MH forecast mean
      if ("meanforecastvarmean" in estmatlabdata) {
        plotdropdown += '<li class="dropdown-header">Forecasted Variables (Mean):</li>';
        for (x in estmatlabdata.meanforecastvarmean) {
          plotdropdown += '<li onclick="estplotmeanforecastvar(&quot;' + x + '&quot;)"><a href="#stophere">' + x + '</a></li>';
        }
      }

      //MH forecast point
      if ("pointforecastvarmean" in estmatlabdata) {
        plotdropdown += '<li class="dropdown-header">Forecasted Variables (Point):</li>';
        for (x in estmatlabdata.pointforecastvarmean) {
          plotdropdown += '<li onclick="estplotpointforecastvar(&quot;' + x + '&quot;)"><a href="#stophere">' + x + '</a></li>';
        }
      }







      plotdropdown += '</ul></div>';
      plotdropdown += `
    <div class="btn-group" role="group">
    <button id="exportbtn" class="btn btn-primary btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Export data <span class="caret"></span><span class="sr-only">Toggle Dropdown</span>
    </button>
    <ul class="dropdown-menu" aria-labelledby="exportbtn">
    <li class="dropdown-header">Select file type:</li>
    <li onclick="exportfile(&quot;json&quot;)"><a href="#stophere">Export to JSON</a></li>
    <li onclick="exportfile(&quot;csv&quot;)"><a href="#stophere">Export to CSV</a></li>
    </ul>
    </div>`;

      plotdropdown += `
      </div>
      </section>
      `;
      document.getElementById("plotbtn").innerHTML = plotdropdown;


      estplotpriors(firstname, firsttype);

    }






    $("#inner").empty();
    document.getElementById("outputtable").innerHTML = estbl1;

    if (estmatlabdata.estimtypeflag == 1) {
      document.getElementById("outputtable2").innerHTML = estbl2;
    }

  }

  finishestimation();

});

function estplotpriors(plotvar, priortype) {

  if (priortype == 0) {
    xvar = [];
    yvar = [];
    if ("parameters" in estmatlabdata.prior_density) {
      for (i = 0; i < estmatlabdata.prior_density.parameters[plotvar].length; i++) {
        xvar.push(estmatlabdata.prior_density.parameters[plotvar][i][0]);
        yvar.push(estmatlabdata.prior_density.parameters[plotvar][i][1]);
      }
      var trace1 = {
        x: xvar,
        y: yvar,
        line: {
          color: 'rgb(182, 179, 179)',
          width: 3
        }
      };
      var data = [trace1];
      var layout = {
        title: plotvar,
        xaxis: {
          title: ''
        },
        yaxis: {
          title: "Density"
        }
      };
    }
  }

  if (priortype == 1) {
    xvar = [];
    yvar = [];
    if ("shocks_std" in estmatlabdata.prior_density) {
      for (i = 0; i < estmatlabdata.prior_density.shocks_std[plotvar].length; i++) {
        xvar.push(estmatlabdata.prior_density.shocks_std[plotvar][i][0]);
        yvar.push(estmatlabdata.prior_density.shocks_std[plotvar][i][1]);
      }
      var trace1 = {
        x: xvar,
        y: yvar,
        line: {
          color: 'rgb(182, 179, 179)',
          width: 3
        }
      };
      var data = [trace1];
      var layout = {
        title: 'SE_' + plotvar,
        xaxis: {
          title: ''
        },
        yaxis: {
          title: "Density"
        }
      };
    }
  }

  if (priortype == 2) {
    xvar = [];
    yvar = [];
    if ("shocks_corr" in estmatlabdata.prior_density) {
      for (i = 0; i < estmatlabdata.prior_density.shocks_corr[plotvar].length; i++) {
        xvar.push(estmatlabdata.prior_density.shocks_corr[plotvar][i][0]);
        yvar.push(estmatlabdata.prior_density.shocks_corr[plotvar][i][1]);
      }
      var trace1 = {
        x: xvar,
        y: yvar,
        line: {
          color: 'rgb(182, 179, 179)',
          width: 3
        }
      };
      var data = [trace1];
      var layout = {
        title: 'CC_' + plotvar,
        xaxis: {
          title: ''
        },
        yaxis: {
          title: "Density"
        }
      };
    }
  }

  Plotly.newPlot('plotbox', data, layout);

}

function estplotposteriors(plotvar, priortype) {

  if (priortype == 0) {
    xvar = [];
    yvar = [];
    x2var = [];
    y2var = [];
    if ("parameters" in estmatlabdata.prior_density) {
      for (i = 0; i < estmatlabdata.prior_density.parameters[plotvar].length; i++) {
        xvar.push(estmatlabdata.prior_density.parameters[plotvar][i][0]);
        yvar.push(estmatlabdata.prior_density.parameters[plotvar][i][1]);
      }
      for (i = 0; i < estmatlabdata.posterior_density.parameters[plotvar].length; i++) {
        x2var.push(estmatlabdata.posterior_density.parameters[plotvar][i][0]);
        y2var.push(estmatlabdata.posterior_density.parameters[plotvar][i][1]);
      }
      var trace1 = {
        x: xvar,
        y: yvar,
        name: 'Prior',
        line: {
          color: 'rgb(182, 179, 179)',
          width: 3
        }
      };
      var trace2 = {
        x: x2var,
        y: y2var,
        name: 'Posterior',
        line: {
          color: 'rgb(0, 0, 0)',
          width: 3
        }
      };


      var data = [trace1, trace2];
      var layout = {
        title: plotvar,
        xaxis: {
          title: ''
        },
        yaxis: {
          title: "Density"
        },
        shapes: [{
          type: 'line',
          x0: estmatlabdata.posterior_mode.parameters[plotvar],
          y0: 0,
          x1: estmatlabdata.posterior_mode.parameters[plotvar],
          y1: Math.max(...trace2.y),
          line: {
            color: 'rgb(25, 253, 2)',
            width: 2,
            dash: 'dot'
          }
        }]
      };
    }
  }

  if (priortype == 1) {
    xvar = [];
    yvar = [];
    x2var = [];
    y2var = [];
    if ("shocks_std" in estmatlabdata.prior_density) {
      for (i = 0; i < estmatlabdata.prior_density.shocks_std[plotvar].length; i++) {
        xvar.push(estmatlabdata.prior_density.shocks_std[plotvar][i][0]);
        yvar.push(estmatlabdata.prior_density.shocks_std[plotvar][i][1]);
      }
      for (i = 0; i < estmatlabdata.posterior_density.shocks_std[plotvar].length; i++) {
        x2var.push(estmatlabdata.posterior_density.shocks_std[plotvar][i][0]);
        y2var.push(estmatlabdata.posterior_density.shocks_std[plotvar][i][1]);
      }
      var trace1 = {
        x: xvar,
        y: yvar,
        name: 'Prior',
        line: {
          color: 'rgb(182, 179, 179)',
          width: 3
        }
      };
      var trace2 = {
        x: x2var,
        y: y2var,
        name: 'Posterior',
        line: {
          color: 'rgb(0, 0, 0)',
          width: 3
        }
      };

      var data = [trace1, trace2];
      var layout = {
        title: 'SE_' + plotvar,
        xaxis: {
          title: ''
        },
        yaxis: {
          title: "Density"
        },
        shapes: [{
          type: 'line',
          x0: estmatlabdata.posterior_mode.shocks_std[plotvar],
          y0: 0,
          x1: estmatlabdata.posterior_mode.shocks_std[plotvar],
          y1: Math.max(...trace2.y),
          line: {
            color: 'rgb(25, 253, 2)',
            width: 2,
            dash: 'dot'
          }
        }]
      };
    }
  }

  if (priortype == 2) {
    xvar = [];
    yvar = [];
    x2var = [];
    y2var = [];
    if ("shocks_corr" in estmatlabdata.prior_density) {
      for (i = 0; i < estmatlabdata.prior_density.shocks_corr[plotvar].length; i++) {
        xvar.push(estmatlabdata.prior_density.shocks_corr[plotvar][i][0]);
        yvar.push(estmatlabdata.prior_density.shocks_corr[plotvar][i][1]);
      }
      for (i = 0; i < estmatlabdata.posterior_density.shocks_corr[plotvar].length; i++) {
        x2var.push(estmatlabdata.posterior_density.shocks_corr[plotvar][i][0]);
        y2var.push(estmatlabdata.posterior_density.shocks_corr[plotvar][i][1]);
      }
      var trace1 = {
        x: xvar,
        y: yvar,
        name: 'Prior',
        line: {
          color: 'rgb(182, 179, 179)',
          width: 3
        }
      };
      var trace2 = {
        x: x2var,
        y: y2var,
        name: 'Posterior',
        line: {
          color: 'rgb(0, 0, 0)',
          width: 3
        }
      };

      var data = [trace1, trace2];
      var layout = {
        title: 'CC_' + plotvar,
        xaxis: {
          title: ''
        },
        yaxis: {
          title: "Density"
        },
        shapes: [{
          type: 'line',
          x0: estmatlabdata.posterior_mode.shocks_corr[plotvar],
          y0: 0,
          x1: estmatlabdata.posterior_mode.shocks_corr[plotvar],
          y1: Math.max(...trace2.y),
          line: {
            color: 'rgb(25, 253, 2)',
            width: 2,
            dash: 'dot'
          }
        }]
      };
    }
  }

  Plotly.newPlot('plotbox', data, layout);

}

function estplotsmoothedshocks(plotvar) {

  yvar = [];
  for (i = 0; i < estmatlabdata.SmoothedShocks[plotvar].length; i++) {
    yvar.push(estmatlabdata.SmoothedShocks[plotvar][i][0]);
  }

  var trace1 = {
    y: yvar,
    line: {
      color: 'rgb(0, 0, 0)',
      width: 1
    }
  };

  var data = [trace1];
  var layout = {
    title: plotvar,
    xaxis: {
      title: ''
    },
    yaxis: {
      title: ''
    },
    shapes: [{
      type: 'line',
      x0: 0,
      y0: 0,
      x1: (estmatlabdata.SmoothedShocks[plotvar].length - 1),
      y1: 0,
      line: {
        color: 'rgb(253, 63, 2)',
        width: 2,
        dash: 'solid'
      }
    }]
  };
  Plotly.newPlot('plotbox', data, layout);

}

function estplotsmoothedvars(plotvar) {

  yvar = [];
  y2var = [];

  for (i = 0; i < estmatlabdata.obs_data[plotvar].length; i++) {
    yvar.push(estmatlabdata.obs_data[plotvar][i][0]);
  }
  for (i = 0; i < estmatlabdata.SmoothedObs[plotvar].length; i++) {
    y2var.push(estmatlabdata.SmoothedObs[plotvar][i][0]);
  }
  var trace1 = {
    y: yvar,
    name: 'Historical',
    line: {
      color: 'rgb(0, 0, 0)',
      width: 1
    }
  };
  var trace2 = {
    y: y2var,
    name: 'Smoothed',
    line: {
      color: 'rgb(253, 63, 2)',
      dash: 'longdash',
      width: 1
    }
  };

  var data = [trace1, trace2];
  var layout = {
    title: plotvar,
    xaxis: {
      title: ''
    },
    yaxis: {
      title: ''
    }
  };

  Plotly.newPlot('plotbox', data, layout);

}

function estplotbayesianirfs(plotvar) {

  xvar = [];
  x2var = []
  yvar = [];
  y2var = [];

  //creating confidence interval
  for (i = 0; i < estmatlabdata.bayesianirfhpdsup[plotvar].length; i++) {
    yvar.push(estmatlabdata.bayesianirfhpdsup[plotvar][i][0]);
    xvar.push(i);
  }

  for (i = (estmatlabdata.bayesianirfhpdinf[plotvar].length - 1); i > -1; i--) {
    yvar.push(estmatlabdata.bayesianirfhpdinf[plotvar][i][0]);
    xvar.push(i);
  }

  //creating posterior mean
  for (i = 0; i < estmatlabdata.bayesianirfmean[plotvar].length; i++) {
    y2var.push(estmatlabdata.bayesianirfmean[plotvar][i][0]);
    x2var.push(i);
  }

  var trace1 = {
    x: xvar,
    y: yvar,
    fill: "tozerox",
    fillcolor: "rgba( 171, 178, 185 ,0.3)",
    line: {
      color: "transparent"
    },
    name: 'HPD',
    showlegend: false,
    type: "scatter"
  };
  var trace2 = {
    x: x2var,
    y: y2var,
    name: 'Mean',
    line: {
      color: 'rgb(23, 32, 42)',
      dash: 'solid',
      width: 2
    }
  };

  var data = [trace1, trace2];
  var layout = {
    title: plotvar,
    xaxis: {
      title: ''
    },
    yaxis: {
      title: ''
    }
  };

  Plotly.newPlot('plotbox', data, layout);

}

function estplotsmoothvar(plotvar) {


  y0var = [];
  d1var = [];
  d2var = [];
  d3var = [];
  d4var = [];
  d5var = [];
  d6var = [];
  d7var = [];
  d8var = [];
  d9var = [];


  //creating deciles
  for (i = 0; i < estmatlabdata.smoothedvardeciles[plotvar][0].length; i++) {
    d1var.push(estmatlabdata.smoothedvardeciles[plotvar][0][i]);
    d2var.push(estmatlabdata.smoothedvardeciles[plotvar][1][i]);
    d3var.push(estmatlabdata.smoothedvardeciles[plotvar][2][i]);
    d4var.push(estmatlabdata.smoothedvardeciles[plotvar][3][i]);
    d5var.push(estmatlabdata.smoothedvardeciles[plotvar][4][i]);
    d6var.push(estmatlabdata.smoothedvardeciles[plotvar][5][i]);
    d7var.push(estmatlabdata.smoothedvardeciles[plotvar][6][i]);
    d8var.push(estmatlabdata.smoothedvardeciles[plotvar][7][i]);
    d9var.push(estmatlabdata.smoothedvardeciles[plotvar][8][i]);
  }

  //creating mean plot
  for (i = 0; i < estmatlabdata.smoothedvarmean[plotvar].length; i++) {
    y0var.push(estmatlabdata.smoothedvarmean[plotvar][i][0]);
    // x2var.push(i);
  }


  var trace0 = {
    y: y0var,
    name: 'Mean',
    line: {
      color: 'rgb(23, 32, 42)',
      dash: 'solid',
      width: 3
    }
  };

  var trace1 = {
    y: d1var,
    name: 'Decile 1',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace2 = {
    y: d2var,
    name: 'Decile 2',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace3 = {
    y: d3var,
    name: 'Decile 3',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace4 = {
    y: d4var,
    name: 'Decile 4',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace5 = {
    y: d5var,
    name: 'Decile 5',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace6 = {
    y: d6var,
    name: 'Decile 6',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace7 = {
    y: d7var,
    name: 'Decile 7',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace8 = {
    y: d8var,
    name: 'Decile 8',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace9 = {
    y: d9var,
    name: 'Decile 9',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var data = [trace1, trace2, trace3, trace4, trace5, trace6, trace7, trace8, trace9, trace0];
  var layout = {
    title: plotvar,
    xaxis: {
      title: ''
    },
    yaxis: {
      title: ''
    }
  };

  Plotly.newPlot('plotbox', data, layout);

}

function estplotsmoothshocks(plotvar) {


  y0var = [];
  d1var = [];
  d2var = [];
  d3var = [];
  d4var = [];
  d5var = [];
  d6var = [];
  d7var = [];
  d8var = [];
  d9var = [];


  //creating deciles
  for (i = 0; i < estmatlabdata.smoothedshocksdeciles[plotvar][0].length; i++) {
    d1var.push(estmatlabdata.smoothedshocksdeciles[plotvar][0][i]);
    d2var.push(estmatlabdata.smoothedshocksdeciles[plotvar][1][i]);
    d3var.push(estmatlabdata.smoothedshocksdeciles[plotvar][2][i]);
    d4var.push(estmatlabdata.smoothedshocksdeciles[plotvar][3][i]);
    d5var.push(estmatlabdata.smoothedshocksdeciles[plotvar][4][i]);
    d6var.push(estmatlabdata.smoothedshocksdeciles[plotvar][5][i]);
    d7var.push(estmatlabdata.smoothedshocksdeciles[plotvar][6][i]);
    d8var.push(estmatlabdata.smoothedshocksdeciles[plotvar][7][i]);
    d9var.push(estmatlabdata.smoothedshocksdeciles[plotvar][8][i]);
  }

  //creating mean plot
  for (i = 0; i < estmatlabdata.smoothedshocksmean[plotvar].length; i++) {
    y0var.push(estmatlabdata.smoothedshocksmean[plotvar][i][0]);
  }


  var trace0 = {
    y: y0var,
    name: 'Mean',
    line: {
      color: 'rgb(23, 32, 42)',
      dash: 'solid',
      width: 3
    }
  };

  var trace1 = {
    y: d1var,
    name: 'Decile 1',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace2 = {
    y: d2var,
    name: 'Decile 2',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace3 = {
    y: d3var,
    name: 'Decile 3',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace4 = {
    y: d4var,
    name: 'Decile 4',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace5 = {
    y: d5var,
    name: 'Decile 5',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace6 = {
    y: d6var,
    name: 'Decile 6',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace7 = {
    y: d7var,
    name: 'Decile 7',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace8 = {
    y: d8var,
    name: 'Decile 8',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace9 = {
    y: d9var,
    name: 'Decile 9',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var data = [trace1, trace2, trace3, trace4, trace5, trace6, trace7, trace8, trace9, trace0];
  var layout = {
    title: plotvar,
    xaxis: {
      title: ''
    },
    yaxis: {
      title: ''
    }
  };

  Plotly.newPlot('plotbox', data, layout);

}

function estplotsmoothconst(plotvar) {

  y0var = [];
  d1var = [];
  d2var = [];
  d3var = [];
  d4var = [];
  d5var = [];
  d6var = [];
  d7var = [];
  d8var = [];
  d9var = [];

  //creating deciles
  for (i = 0; i < estmatlabdata.constantdeciles[plotvar][0].length; i++) {
    d1var.push(estmatlabdata.constantdeciles[plotvar][0][i]);
    d2var.push(estmatlabdata.constantdeciles[plotvar][1][i]);
    d3var.push(estmatlabdata.constantdeciles[plotvar][2][i]);
    d4var.push(estmatlabdata.constantdeciles[plotvar][3][i]);
    d5var.push(estmatlabdata.constantdeciles[plotvar][4][i]);
    d6var.push(estmatlabdata.constantdeciles[plotvar][5][i]);
    d7var.push(estmatlabdata.constantdeciles[plotvar][6][i]);
    d8var.push(estmatlabdata.constantdeciles[plotvar][7][i]);
    d9var.push(estmatlabdata.constantdeciles[plotvar][8][i]);
  }

  //creating mean plot
  for (i = 0; i < estmatlabdata.constantmean[plotvar].length; i++) {
    y0var.push(estmatlabdata.constantmean[plotvar][i][0]);
    // x2var.push(i);
  }

  var trace0 = {
    y: y0var,
    name: 'Mean',
    line: {
      color: 'rgb(23, 32, 42)',
      dash: 'solid',
      width: 3
    }
  };

  var trace1 = {
    y: d1var,
    name: 'Decile 1',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace2 = {
    y: d2var,
    name: 'Decile 2',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace3 = {
    y: d3var,
    name: 'Decile 3',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace4 = {
    y: d4var,
    name: 'Decile 4',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace5 = {
    y: d5var,
    name: 'Decile 5',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace6 = {
    y: d6var,
    name: 'Decile 6',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace7 = {
    y: d7var,
    name: 'Decile 7',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace8 = {
    y: d8var,
    name: 'Decile 8',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace9 = {
    y: d9var,
    name: 'Decile 9',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var data = [trace1, trace2, trace3, trace4, trace5, trace6, trace7, trace8, trace9, trace0];
  var layout = {
    title: plotvar,
    xaxis: {
      title: ''
    },
    yaxis: {
      title: ''
    }
  };

  Plotly.newPlot('plotbox', data, layout);

}

function estplotupdatedvars(plotvar) {

  y0var = [];
  d1var = [];
  d2var = [];
  d3var = [];
  d4var = [];
  d5var = [];
  d6var = [];
  d7var = [];
  d8var = [];
  d9var = [];

  //creating deciles
  for (i = 0; i < estmatlabdata.updatedvardeciles[plotvar][0].length; i++) {
    d1var.push(estmatlabdata.updatedvardeciles[plotvar][0][i]);
    d2var.push(estmatlabdata.updatedvardeciles[plotvar][1][i]);
    d3var.push(estmatlabdata.updatedvardeciles[plotvar][2][i]);
    d4var.push(estmatlabdata.updatedvardeciles[plotvar][3][i]);
    d5var.push(estmatlabdata.updatedvardeciles[plotvar][4][i]);
    d6var.push(estmatlabdata.updatedvardeciles[plotvar][5][i]);
    d7var.push(estmatlabdata.updatedvardeciles[plotvar][6][i]);
    d8var.push(estmatlabdata.updatedvardeciles[plotvar][7][i]);
    d9var.push(estmatlabdata.updatedvardeciles[plotvar][8][i]);
  }

  //creating mean plot
  for (i = 0; i < estmatlabdata.updatedvarmean[plotvar].length; i++) {
    y0var.push(estmatlabdata.updatedvarmean[plotvar][i][0]);
  }

  var trace0 = {
    y: y0var,
    name: 'Mean',
    line: {
      color: 'rgb(23, 32, 42)',
      dash: 'solid',
      width: 3
    }
  };

  var trace1 = {
    y: d1var,
    name: 'Decile 1',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace2 = {
    y: d2var,
    name: 'Decile 2',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace3 = {
    y: d3var,
    name: 'Decile 3',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace4 = {
    y: d4var,
    name: 'Decile 4',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace5 = {
    y: d5var,
    name: 'Decile 5',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace6 = {
    y: d6var,
    name: 'Decile 6',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace7 = {
    y: d7var,
    name: 'Decile 7',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace8 = {
    y: d8var,
    name: 'Decile 8',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace9 = {
    y: d9var,
    name: 'Decile 9',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var data = [trace1, trace2, trace3, trace4, trace5, trace6, trace7, trace8, trace9, trace0];
  var layout = {
    title: plotvar,
    xaxis: {
      title: ''
    },
    yaxis: {
      title: ''
    }
  };

  Plotly.newPlot('plotbox', data, layout);

}

function estplotmeanforecastvar(plotvar) {


  y0var = [];
  d1var = [];
  d2var = [];
  d3var = [];
  d4var = [];
  d5var = [];
  d6var = [];
  d7var = [];
  d8var = [];
  d9var = [];
  xvar = [];


  //creating deciles
  for (i = 0; i < estmatlabdata.meanforecastvardeciles[plotvar][0].length; i++) {
    d1var.push(estmatlabdata.meanforecastvardeciles[plotvar][0][i]);
    d2var.push(estmatlabdata.meanforecastvardeciles[plotvar][1][i]);
    d3var.push(estmatlabdata.meanforecastvardeciles[plotvar][2][i]);
    d4var.push(estmatlabdata.meanforecastvardeciles[plotvar][3][i]);
    d5var.push(estmatlabdata.meanforecastvardeciles[plotvar][4][i]);
    d6var.push(estmatlabdata.meanforecastvardeciles[plotvar][5][i]);
    d7var.push(estmatlabdata.meanforecastvardeciles[plotvar][6][i]);
    d8var.push(estmatlabdata.meanforecastvardeciles[plotvar][7][i]);
    d9var.push(estmatlabdata.meanforecastvardeciles[plotvar][8][i]);
  }

  //creating mean plot
  for (i = 0; i < estmatlabdata.meanforecastvarmean[plotvar].length; i++) {
    y0var.push(estmatlabdata.meanforecastvarmean[plotvar][i][0]);
    xvar.push(i + 1);
  }


  var trace0 = {
    x: xvar,
    y: y0var,
    name: 'Mean',
    mode: 'lines',
    mode: 'lines',
    line: {
      color: 'rgb(23, 32, 42)',
      dash: 'solid',
      width: 3
    }
  };

  var trace1 = {
    x: xvar,
    y: d1var,
    name: 'Decile 1',
    mode: 'lines',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace2 = {
    x: xvar,
    y: d2var,
    name: 'Decile 2',
    mode: 'lines',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace3 = {
    x: xvar,
    y: d3var,
    name: 'Decile 3',
    mode: 'lines',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace4 = {
    x: xvar,
    y: d4var,
    name: 'Decile 4',
    mode: 'lines',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace5 = {
    x: xvar,
    y: d5var,
    name: 'Decile 5',
    mode: 'lines',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace6 = {
    x: xvar,
    y: d6var,
    name: 'Decile 6',
    mode: 'lines',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace7 = {
    x: xvar,
    y: d7var,
    name: 'Decile 7',
    mode: 'lines',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace8 = {
    x: xvar,
    y: d8var,
    name: 'Decile 8',
    mode: 'lines',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace9 = {
    x: xvar,
    y: d9var,
    name: 'Decile 9',
    mode: 'lines',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var data = [trace1, trace2, trace3, trace4, trace5, trace6, trace7, trace8, trace9, trace0];
  var layout = {
    title: plotvar,
    xaxis: {
      title: ''
    },
    yaxis: {
      title: ''
    }
  };

  Plotly.newPlot('plotbox', data, layout);

}

function estplotpointforecastvar(plotvar) {


  y0var = [];
  d1var = [];
  d2var = [];
  d3var = [];
  d4var = [];
  d5var = [];
  d6var = [];
  d7var = [];
  d8var = [];
  d9var = [];
  xvar = [];


  //creating deciles
  for (i = 0; i < estmatlabdata.pointforecastvardeciles[plotvar][0].length; i++) {
    d1var.push(estmatlabdata.pointforecastvardeciles[plotvar][0][i]);
    d2var.push(estmatlabdata.pointforecastvardeciles[plotvar][1][i]);
    d3var.push(estmatlabdata.pointforecastvardeciles[plotvar][2][i]);
    d4var.push(estmatlabdata.pointforecastvardeciles[plotvar][3][i]);
    d5var.push(estmatlabdata.pointforecastvardeciles[plotvar][4][i]);
    d6var.push(estmatlabdata.pointforecastvardeciles[plotvar][5][i]);
    d7var.push(estmatlabdata.pointforecastvardeciles[plotvar][6][i]);
    d8var.push(estmatlabdata.pointforecastvardeciles[plotvar][7][i]);
    d9var.push(estmatlabdata.pointforecastvardeciles[plotvar][8][i]);
  }

  //creating mean plot
  for (i = 0; i < estmatlabdata.pointforecastvarmean[plotvar].length; i++) {
    y0var.push(estmatlabdata.pointforecastvarmean[plotvar][i][0]);
    xvar.push(i + 1);
  }


  var trace0 = {
    x: xvar,
    y: y0var,
    name: 'Mean',
    mode: 'lines',
    line: {
      color: 'rgb(23, 32, 42)',
      dash: 'solid',
      width: 3
    }
  };

  var trace1 = {
    x: xvar,
    y: d1var,
    name: 'Decile 1',
    mode: 'lines',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace2 = {
    x: xvar,
    y: d2var,
    name: 'Decile 2',
    mode: 'lines',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace3 = {
    x: xvar,
    y: d3var,
    name: 'Decile 3',
    mode: 'lines',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace4 = {
    x: xvar,
    y: d4var,
    name: 'Decile 4',
    mode: 'lines',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace5 = {
    x: xvar,
    y: d5var,
    name: 'Decile 5',
    mode: 'lines',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace6 = {
    x: xvar,
    y: d6var,
    name: 'Decile 6',
    mode: 'lines',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace7 = {
    x: xvar,
    y: d7var,
    name: 'Decile 7',
    mode: 'lines',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace8 = {
    x: xvar,
    y: d8var,
    name: 'Decile 8',
    mode: 'lines',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var trace9 = {
    x: xvar,
    y: d9var,
    name: 'Decile 9',
    mode: 'lines',
    line: {
      color: 'rgb(120, 250, 67)',
      dash: 'solid',
      width: 1
    }
  };

  var data = [trace1, trace2, trace3, trace4, trace5, trace6, trace7, trace8, trace9, trace0];
  var layout = {
    title: plotvar,
    xaxis: {
      title: ''
    },
    yaxis: {
      title: ''
    }
  };

  Plotly.newPlot('plotbox', data, layout);

}

function estplotforecastvar(plotvar) {

  supxvar = [];
  supyvar = [];
  infxvar = [];
  infyvar = [];

  x2var = []
  y2var = [];

  //creating confidence interval
  for (i = 0; i < estmatlabdata.forecastvarhpdsup[plotvar].length; i++) {
    supyvar.push(estmatlabdata.forecastvarhpdsup[plotvar][i][0]);
    supxvar.push(i + 1);
  }

  for (i = 0; i < estmatlabdata.forecastvarhpdinf[plotvar].length; i++) {
    infyvar.push(estmatlabdata.forecastvarhpdinf[plotvar][i][0]);
    infxvar.push(i + 1);
  }

  //creating forecast mean
  for (i = 0; i < estmatlabdata.forecastvarmean[plotvar].length; i++) {
    y2var.push(estmatlabdata.forecastvarmean[plotvar][i][0]);
    x2var.push(i + 1);
  }

  var trace0 = {
    x: x2var,
    y: y2var,
    name: 'Mean',
    mode: 'lines',
    line: {
      color: 'rgb(90, 90, 245)',
      dash: 'solid',
      width: 2
    }
  };

  var trace1 = {
    x: supxvar,
    y: supyvar,
    name: 'HPDsup',
    mode: 'lines',
    line: {
      color: 'rgb(90, 90, 245)',
      dash: 'solid',
      width: 1
    }
  };

  var trace2 = {
    x: infxvar,
    y: infyvar,
    name: 'HPDinf',
    mode: 'lines',
    line: {
      color: 'rgb(90, 90, 245)',
      dash: 'solid',
      width: 1
    }
  };

  var data = [trace0, trace1, trace2];
  var layout = {
    title: plotvar,
    xaxis: {
      title: ''
    },
    yaxis: {
      title: ''
    }
  };

  Plotly.newPlot('plotbox', data, layout);

}




function createestimoptions() {

  var estopttableoptions = '';
  estimoptionslist.forEach(function (element) {

    if (element.estoptinputtype == 0) { //options that do not require the entry of a value
      estopttableoptions += `<tr>
                            <th scope="row"><input type="checkbox" id="estimopt_id_` + element.estoptid + `" onchange="onoffestimoption(` + element.estoptid + `)" data-toggle="toggle" data-onstyle="success" data-size="mini"></th>
                            <td>` + element.estoptdynarename + `</td>
                            <td></td>
                            <td><div class="tooltipcss"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span><span class="tooltiptextcss">` + element.estoptinfo + `</span></div></td>
                          </tr>`;
    }

    if ((element.estoptinputtype != 0) && (element.estoptinputtype != 5)) { //options that do require the entry of a value

      if (element.estoptid == 0) {
        estopttableoptions += `<tr>
                          <th scope="row"><input type="checkbox" id="estimopt_id_` + element.estoptid + `" onchange="onoffestimoption(` + element.estoptid + `)" data-toggle="toggle" data-onstyle="success" data-size="mini"></th>
                          <td style="color:#229954;">` + element.estoptdynarename + `</td>
                          <td><input class="form-control input-sm" name="estimopt_input_` + element.estoptid + `" id="estimopt_input_` + element.estoptid + `" placeholder="` + element.estoptplaceholder + `" type="text" value="" disabled="disabled"></td>
                          <td><div class="tooltipcss"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span><span class="tooltiptextcss">` + element.estoptinfo + `</span></div></td>
                        </tr>`;
      } else {

        estopttableoptions += `<tr>
                          <th scope="row"><input type="checkbox" id="estimopt_id_` + element.estoptid + `" onchange="onoffestimoption(` + element.estoptid + `)" data-toggle="toggle" data-onstyle="success" data-size="mini"></th>
                          <td>` + element.estoptdynarename + `</td>
                          <td><input class="form-control input-sm" name="estimopt_input_` + element.estoptid + `" id="estimopt_input_` + element.estoptid + `" placeholder="` + element.estoptplaceholder + `" type="text" value="" disabled="disabled"></td>
                          <td><div class="tooltipcss"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span><span class="tooltiptextcss">` + element.estoptinfo + `</span></div></td>
                        </tr>`;
      }
    }

    if (element.estoptinputtype == 5) { //options that do require specific interface

      switch (element.estoptid) {

        case 23: //mcmc_jumping_covariance
          estopttableoptions += `<tr>
                          <th scope="row"><input type="checkbox" id="estimopt_id_` + element.estoptid + `" onchange="onoffestimoptionselect(` + element.estoptid + `)" data-toggle="toggle" data-onstyle="success" data-size="mini"></th>
                          <td>` + element.estoptdynarename + `</td>
                          <td><select class="form-control input-sm" name="estimopt_input_` + element.estoptid + `" id="estimopt_input_` + element.estoptid + `" disabled>
                          <option value="-1" selected="true" disabled="disabled">Select</option>
                          <option value="1">hessian</option>
                          <option value="2">prior_variance</option>
                          <option value="3">identity_matrix</option>
                          </select></td>
                          <td><div class="tooltipcss"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span><span class="tooltiptextcss">` + element.estoptinfo + `</span></div></td>
                        </tr>`;
          break;

        case 36: //posterior_sampling_method
          estopttableoptions += `<tr>
                        <th scope="row"><input type="checkbox" id="estimopt_id_` + element.estoptid + `" onchange="onoffestimoptionselect(` + element.estoptid + `)" data-toggle="toggle" data-onstyle="success" data-size="mini"></th>
                        <td>` + element.estoptdynarename + `</td>
                        <td><select class="form-control input-sm" name="estimopt_input_` + element.estoptid + `" id="estimopt_input_` + element.estoptid + `" disabled>
                        <option value="-1" selected="true" disabled="disabled">Select</option>
                        <option value="1">'random_walk_metropolis_hastings'</option>
                        <option value="2">'tailored_random_block_metropolis_hastings'</option>
                        <option value="3">'independent_metropolis_hastings'</option>
                        <option value="4">'slice'</option>
                        </select></td>
                        <td><div class="tooltipcss"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span><span class="tooltiptextcss">` + element.estoptinfo + `</span></div></td>
                      </tr>`;
          break;

        case 62: //sylvester
          estopttableoptions += `<tr>
                          <th scope="row"><input type="checkbox" id="estimopt_id_` + element.estoptid + `" onchange="onoffestimoptionselect(` + element.estoptid + `)" data-toggle="toggle" data-onstyle="success" data-size="mini"></th>
                          <td>` + element.estoptdynarename + `</td>
                          <td><select class="form-control input-sm" name="estimopt_input_` + element.estoptid + `" id="estimopt_input_` + element.estoptid + `" disabled>
                          <option value="-1" selected="true" disabled="disabled">Select</option>
                          <option value="1">default</option>
                          <option value="2">fixed_point</option>
                          </select></td>
                          <td><div class="tooltipcss"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span><span class="tooltiptextcss">` + element.estoptinfo + `</span></div></td>
                        </tr>`;
          break;

        case 64: //lyapunov
          estopttableoptions += `<tr>
                          <th scope="row"><input type="checkbox" id="estimopt_id_` + element.estoptid + `" onchange="onoffestimoptionselect(` + element.estoptid + `)" data-toggle="toggle" data-onstyle="success" data-size="mini"></th>
                          <td>` + element.estoptdynarename + `</td>
                          <td><select class="form-control input-sm" name="estimopt_input_` + element.estoptid + `" id="estimopt_input_` + element.estoptid + `" disabled>
                          <option value="-1" selected="true" disabled="disabled">Select</option>
                          <option value="1">default</option>
                          <option value="2">fixed_point</option>
                          <option value="3">doubling</option>
                          <option value="4">square_root_solver</option>
                          </select></td>
                          <td><div class="tooltipcss"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span><span class="tooltiptextcss">` + element.estoptinfo + `</span></div></td>
                        </tr>`;
          break;

        case 81: //resampling
          estopttableoptions += `<tr>
                              <th scope="row"><input type="checkbox" id="estimopt_id_` + element.estoptid + `" onchange="onoffestimoptionselect(` + element.estoptid + `)" data-toggle="toggle" data-onstyle="success" data-size="mini"></th>
                              <td>` + element.estoptdynarename + `</td>
                              <td><select class="form-control input-sm" name="estimopt_input_` + element.estoptid + `" id="estimopt_input_` + element.estoptid + `" disabled>
                              <option value="-1" selected="true" disabled="disabled">Select</option>
                              <option value="1">none</option>
                              <option value="2">systematic</option>
                              <option value="3">generic</option>
                              </select></td>
                              <td><div class="tooltipcss"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span><span class="tooltiptextcss">` + element.estoptinfo + `</span></div></td>
                            </tr>`;
          break;

        case 83: //resampling_method
          estopttableoptions += `<tr>
                                  <th scope="row"><input type="checkbox" id="estimopt_id_` + element.estoptid + `" onchange="onoffestimoptionselect(` + element.estoptid + `)" data-toggle="toggle" data-onstyle="success" data-size="mini"></th>
                                  <td>` + element.estoptdynarename + `</td>
                                  <td><select class="form-control input-sm" name="estimopt_input_` + element.estoptid + `" id="estimopt_input_` + element.estoptid + `" disabled>
                                  <option value="-1" selected="true" disabled="disabled">Select</option>
                                  <option value="1">kitagawa</option>
                                  <option value="2">stratified</option>
                                  <option value="3">smooth</option>
                                  </select></td>
                                  <td><div class="tooltipcss"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span><span class="tooltiptextcss">` + element.estoptinfo + `</span></div></td>
                                </tr>`;
          break;

        case 84: //resampling_method
          estopttableoptions += `<tr>
                                  <th scope="row"><input type="checkbox" id="estimopt_id_` + element.estoptid + `" onchange="onoffestimoptionselect(` + element.estoptid + `)" data-toggle="toggle" data-onstyle="success" data-size="mini"></th>
                                  <td>` + element.estoptdynarename + `</td>
                                  <td><select class="form-control input-sm" name="estimopt_input_` + element.estoptid + `" id="estimopt_input_` + element.estoptid + `" disabled>
                                  <option value="-1" selected="true" disabled="disabled">Select</option>
                                  <option value="1">sis</option>
                                  <option value="2">apf</option>
                                  <option value="3">gf</option>
                                  <option value="4">gmf</option>
                                  <option value="5">cpf</option>
                                  <option value="6">nlkf</option>
                                  </select></td>
                                  <td><div class="tooltipcss"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span><span class="tooltiptextcss">` + element.estoptinfo + `</span></div></td>
                                </tr>`;
          break;

        case 85: //proposal_approximation
          estopttableoptions += `<tr>
                                  <th scope="row"><input type="checkbox" id="estimopt_id_` + element.estoptid + `" onchange="onoffestimoptionselect(` + element.estoptid + `)" data-toggle="toggle" data-onstyle="success" data-size="mini"></th>
                                  <td>` + element.estoptdynarename + `</td>
                                  <td><select class="form-control input-sm" name="estimopt_input_` + element.estoptid + `" id="estimopt_input_` + element.estoptid + `" disabled>
                                  <option value="-1" selected="true" disabled="disabled">Select</option>
                                  <option value="1">cubature</option>
                                  <option value="2">montecarlo</option>
                                  <option value="3">unscented</option>
                                  </select></td>
                                  <td><div class="tooltipcss"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span><span class="tooltiptextcss">` + element.estoptinfo + `</span></div></td>
                                </tr>`;
          break;

        case 86: //distribution_approximation
          estopttableoptions += `<tr>
                                  <th scope="row"><input type="checkbox" id="estimopt_id_` + element.estoptid + `" onchange="onoffestimoptionselect(` + element.estoptid + `)" data-toggle="toggle" data-onstyle="success" data-size="mini"></th>
                                  <td>` + element.estoptdynarename + `</td>
                                  <td><select class="form-control input-sm" name="estimopt_input_` + element.estoptid + `" id="estimopt_input_` + element.estoptid + `" disabled>
                                  <option value="-1" selected="true" disabled="disabled">Select</option>
                                  <option value="1">cubature</option>
                                  <option value="2">montecarlo</option>
                                  <option value="3">unscented</option>
                                  </select></td>
                                  <td><div class="tooltipcss"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span><span class="tooltiptextcss">` + element.estoptinfo + `</span></div></td>
                                </tr>`;
          break;


        case 87: //cpf_weights
          estopttableoptions += `<tr>
                                  <th scope="row"><input type="checkbox" id="estimopt_id_` + element.estoptid + `" onchange="onoffestimoptionselect(` + element.estoptid + `)" data-toggle="toggle" data-onstyle="success" data-size="mini"></th>
                                  <td>` + element.estoptdynarename + `</td>
                                  <td><select class="form-control input-sm" name="estimopt_input_` + element.estoptid + `" id="estimopt_input_` + element.estoptid + `" disabled>
                                  <option value="-1" selected="true" disabled="disabled">Select</option>
                                  <option value="1">amisanotristani</option>
                                  <option value="2">murrayjonesparslow</option>
                                  </select></td>
                                  <td><div class="tooltipcss"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span><span class="tooltiptextcss">` + element.estoptinfo + `</span></div></td>
                                </tr>`;
          break;

      }


    }


  });

  $('#estimoptiontable tbody').append(estopttableoptions);
}

var estimoptionslist = [{
    estoptid: 0,
    estoptdynarename: 'endogenous_variables',
    estoptname: 'Number of observations',
    estoptplaceholder: 'All',
    estoptinputtype: 60,
    estoptinfo: 'Use this option to list the endogenous variables appearing after the estimation command. Available options are:  <br> varobs: only the observed endogenous variables will be considered, <br> A list of user selected endogenous variables separated by a space. Example: var1 var2 var3 <br> Defaut: By default all endogenous will be listed. Please leave this option unchecked to activate the default behavior.'
  },
  {
    estoptid: 1,
    estoptdynarename: 'nobs',
    estoptname: 'Number of observations',
    estoptplaceholder: 'All',
    estoptinputtype: 10,
    estoptinfo: 'The number of observations following first_obs to be used.<br> A range of numbers can be entered by using : as in for instance "50:100". <br> An array of number can be entered by using a space as in for instance "8 9 10 11 15".<br> Defaut: all observations in the file after first_obs.'
  },
  {
    estoptid: 2,
    estoptdynarename: 'first_obs',
    estoptname: 'Number of the 1st observation',
    estoptplaceholder: '1',
    estoptinputtype: 10,
    estoptinfo: 'The number of the first observation to be used. In case of estimating a DSGE-VAR, first_obs needs to be larger than the number of lags. <br> A range of numbers can be entered by using : as in for instance "50:100". <br> An array of number can be entered by using a space as in for instance "8 9 10 11 15".<br> Defaut: 1.'
  },
  {
    estoptid: 3,
    estoptdynarename: 'prefilter',
    estoptname: 'Prefilter',
    estoptplaceholder: '0',
    estoptinputtype: 1,
    estoptinfo: 'A value of 1 means that the estimation procedure will demean each data series by its empirical mean. If the loglinear option without the logdata option is requested, the data will first be logged and then demeaned.<br> Defaut: 0, i.e. no prefiltering.'
  },
  {
    estoptid: 4,
    estoptdynarename: 'presample',
    estoptname: 'Presample',
    estoptplaceholder: '0',
    estoptinputtype: 1,
    estoptinfo: 'The number of observations after first_obs to be skipped before evaluating the likelihood. These presample observations do not enter the likelihood, but are used as a training sample for starting the Kalman filter iterations. This option is incompatible with estimating a DSGE-VAR.<br> Defaut: 0.'
  },
  {
    estoptid: 5,
    estoptdynarename: 'loglinear',
    estoptname: 'Log-linear approximation',
    estoptplaceholder: '',
    estoptinputtype: 0,
    estoptinfo: 'Computes a log-linear approximation of the model instead of a linear approximation. As always in the context of estimation, the data must correspond to the definition of the variables used in the model (see Pfeifer (2013) for more details on how to correctly specify observation equations linking model variables and the data). If you specify the loglinear option, Dynare will take the logarithm of both your model variables and of your data as it assumes the data to correspond to the original non-logged model variables. The displayed posterior results like impulse responses, smoothed variables, and moments will be for the logged variables, not the original un-logged ones.<br> Defaut: computes a linear approximation.'
  },
  {
    estoptid: 6,
    estoptdynarename: 'logdata',
    estoptname: 'Log transformation',
    estoptplaceholder: '',
    estoptinputtype: 0,
    estoptinfo: 'Dynare applies the log transformation to the provided data if a log-linearization of the model is re- quested (loglinear) unless logdata option is used. This option is necessary if the user provides data already in logs, otherwise the log transformation will be applied twice (this may result in complex data).'
  },
  {
    estoptid: 7,
    estoptdynarename: 'nograph',
    estoptname: 'Do not create graphs',
    estoptplaceholder: '',
    estoptinputtype: 0,
    estoptinfo: 'Do not create graphs (which implies that they are not saved to the disk nor displayed). If this option is not used, graphs will be saved to disk (to the format specified by graph_format option, except if graph_format=none) and displayed to screen (unless nodisplay option is used).'
  },
  {
    estoptid: 8,
    estoptdynarename: 'posterior_nograph',
    estoptname: 'No Bayesian IRFs',
    estoptplaceholder: '',
    estoptinputtype: 0,
    estoptinfo: 'Suppresses the generation of graphs associated with Bayesian IRFs (bayesian_irf), posterior smoothed objects (smoother), and posterior forecasts (forecast).'
  },
  {
    estoptid: 9,
    estoptdynarename: 'lik_init',
    estoptname: 'Kalman filter initialization',
    estoptplaceholder: '1',
    estoptinputtype: 1,
    estoptinfo: '1: For stationary models, the initial matrix of variance of the error of forecast is set equal to the unconditional variance of the state variables.<br> 2: For nonstationary models: a wide prior is used with an initial matrix of variance of the error of forecast diagonal with 10 on the diagonal (follows the suggestion of Harvey and Phillips(1979)).<br> 3: For nonstationary models: use a diffuse filter (use rather the diffuse_filter option).<br> 4: The filter is initialized with the fixed point of the Riccati equation.<br> Use i) option 2 for the non-stationary elements by setting their initial variance in the forecast error matrix to 10 on the diagonal and all covariances to 0 and ii) option 1 for the stationary elements.<br> Defaut: 1. For advanced use only.'
  },
  {
    estoptid: 10,
    estoptdynarename: 'conf_sig',
    estoptname: 'Confidence interval',
    estoptplaceholder: '0.90',
    estoptinputtype: 2,
    estoptinfo: 'Confidence interval used for classical forecasting after estimation.<br> Defaut: 0.90.'
  },
  {
    estoptid: 11,
    estoptdynarename: 'mh_conf_sig',
    estoptname: 'Confidence/HPD interval',
    estoptplaceholder: '0.90',
    estoptinputtype: 2,
    estoptinfo: 'Confidence/HPD interval used for the computation of prior and posterior statistics like: parameter distributions, prior/posterior moments, conditional variance decomposition, impulse response functions, Bayesian forecasting.<br> Defaut: 0.90.'
  },
  {
    estoptid: 12,
    estoptdynarename: 'mh_replic',
    estoptname: 'MH algo number of replications',
    estoptplaceholder: '20000',
    estoptinputtype: 1,
    estoptinfo: 'Number of replications for Metropolis-Hastings algorithm. For the time being, mh_replic should be larger than 1200.<br> Defaut: 20000'
  },
  {
    estoptid: 13,
    estoptdynarename: 'sub_draws',
    estoptname: 'Number of draws',
    estoptplaceholder: '',
    estoptinputtype: 1,
    estoptinfo: 'Number of draws from the MCMC that are used to compute posterior distribution of various objects (smoothed variable, smoothed shocks, forecast, moments, IRF). The draws used to compute these posterior moments are sampled uniformly in the estimated empirical posterior distribution (i.e. draws of the MCMC). sub_draws should be smaller than the total number of MCMC draws available.<br> Defaut: Default: min(posterior_max_subsample_draws, (Total number of draws)*(number of chains) ).'
  },
  {
    estoptid: 14,
    estoptdynarename: 'posterior_max_subsample_draws',
    estoptname: 'Maximum number of draws',
    estoptplaceholder: '1200',
    estoptinputtype: 1,
    estoptinfo: 'Maximum number of draws from the MCMC used to compute posterior distribution of various objects (smoothed variable, smoothed shocks, forecast, moments, IRF), if not overriden by option sub_draws.<br> Defaut: Default: 1200.'
  },
  {
    estoptid: 15,
    estoptdynarename: 'mh_nblocks',
    estoptname: 'MH algo number of parallel chains',
    estoptplaceholder: '2',
    estoptinputtype: 1,
    estoptinfo: 'Number of parallel chains for Metropolis-Hastings algorithm.<br> Defaut: 2.'
  },
  {
    estoptid: 16,
    estoptdynarename: 'mh_drop',
    estoptname: 'Fraction vectors to be dropped',
    estoptplaceholder: '0.5',
    estoptinputtype: 2,
    estoptinfo: 'The fraction of initially generated parameter vectors to be dropped as a burn-in before using posterior simulations. <br> Defaut: 0.5.'
  },
  {
    estoptid: 17,
    estoptdynarename: 'mh_jscale',
    estoptname: 'Scale parameter',
    estoptplaceholder: '0.2',
    estoptinputtype: 2,
    estoptinfo: 'The scale parameter of the jumping distribution’s covariance matrix (Metropolis-Hastings or TaRB-algorithm). The default value is rarely satisfactory. This option must be tuned to obtain, ideally, an acceptance ratio of 25%-33%. Basically, the idea is to increase the variance of the jumping distribution if the acceptance ratio is too high, and decrease the same variance if the acceptance ratio is too low. In some situations it may help to consider parameter-specific values for this scale parameter. This can be done in the estimated_params block.<br> Note that mode_compute=6 will tune the scale parameter to achieve an acceptance rate of AcceptanceRateTarget. The resulting scale parameter will be saved into a file named MODEL_FILENAME_mh_scale.mat. This file can be loaded in subsequent runs via the posterior_sampler_options option scale_file. Both mode_compute=6 and scale_file will overwrite any value specified in estimated_params with the tuned value.<br> Defaut: 0.2.'
  },
  {
    estoptid: 18,
    estoptdynarename: 'mh_init_scale',
    estoptname: 'Scale parameter initial draw',
    estoptplaceholder: '',
    estoptinputtype: 2,
    estoptinfo: 'The scale to be used for drawing the initial value of the Metropolis-Hastings chain. Generally, the starting points should be overdispersed for the Brooks and Gelman (1998) convergence diagnostics to be meaningful.<br> It is important to keep in mind that mh_init_scale is set at the beginning of Dynare execution, i.e. the default will not take into account potential changes in mh_jscale introduced by either mode_compute=6 or the posterior_sampler_options option scale_file. If mh_init_scale is too wide during initalization of the posterior sampler so that 100 tested draws are inadmissible (e.g. Blanchard-Kahn conditions are always violated), Dynare will request user input of a new mh_init_scale value with which the next 100 draws will be drawn and tested. If the nointeractive option has been invoked, the program will instead automatically decrease mh_init_scale by 10 percent after 100 futile draws and try another 100 draws. This iterative procedure will take place at most 10 times, at which point Dynare will abort with an error message.<br> Defaut: 2*mh_jscale.'
  },
  {
    estoptid: 19,
    estoptdynarename: 'mh_tune_jscale',
    estoptname: 'Automatic scaling',
    estoptplaceholder: '0.33',
    estoptinputtype: 2,
    estoptinfo: 'Automatically tunes the scale parameter of the jumping distribution’s covariance matrix (Metropolis-Hastings), so that the overall acceptance ratio is close to the desired level.<br> It is not possible to match exactly the desired acceptance ratio because of the stochastic nature of the algorithm (the proposals and the initial conditions of the markov chains if mh_nblocks>1). This option is only available for the Random Walk Metropolis Hastings algorithm.<br> Defaut: 0.33.'
  },
  {
    estoptid: 20,
    estoptdynarename: 'mh_recover',
    estoptname: 'Crashed MH recovery',
    estoptplaceholder: '',
    estoptinputtype: 0,
    estoptinfo: 'Attempts to recover a Metropolis-Hastings simulation that crashed prematurely, starting with the last available saved mh-file. Shouldn’t be used together with load_mh_file or a different mh_replic than in the crashed run. Since Dynare 4.5 the proposal density from the previous run will automatically be loaded. In older versions, to assure a neat continuation of the chain with the same proposal density, you should provide the mode_file used in the previous run or the same user-defined mcmc_jumping_covariance when using this option. Note that under Octave, a neat continuation of the crashed chain with the respective last random number generator state is currently not supported.'
  },
  {
    estoptid: 21,
    estoptdynarename: 'mode_compute',
    estoptname: 'Mode computation optimizer',
    estoptplaceholder: '4',
    estoptinputtype: 1,
    estoptinfo: 'Specifies the optimizer for the mode computation.<br>0: The mode isn’t computed. When the mode_file option is specified, the mode is simply read from that file. When mode_file option is not specified, Dynare reports the value of the log posterior (log likelihood) evaluated at the initial value of the parameters. When mode_file is not specified and there is no estimated_params block, but the smoother option is used, it is a roundabout way to compute the smoothed value of the variables of a model with calibrated parameters. <br> 1: Uses fmincon optimization routine (available under MATLAB if the Optimization Toolbox is installed; available under Octave if the optim package from Octave-Forge, version 1.6 or above, is installed). <br> 2: Uses the continuous simulated annealing global optimization algorithm described in Corana et al.(1987) and Goffe et al.(1994). <br> 3: Uses fminunc optimization routine (available under MATLAB if the Optimization Toolbox is installed; available under Octave if the optim package from Octave-Forge is installed). <br> 4: Uses Chris Sims’s csminwel. <br>5: Uses Marco Ratto’s newrat. This value is not compatible with non linear filters or DSGE-VAR models. This is a slice optimizer: most iterations are a sequence of univariate optimization step, one for each estimated parameter or shock. Uses csminwel for line search in each step. <br> 6: Uses a Monte-Carlo based optimization routine (see Dynare wiki for more details). <br> 7: Uses fminsearch, a simplex-based optimization routine (available under MATLAB if the Optimization Toolbox is installed; available under Octave if the optim package from Octave-Forge is installed). <br> 8: Uses Dynare implementation of the Nelder-Mead simplex-based optimization routine (generally more efficient than the MATLAB or Octave implementation available with mode_compute=7). <br> 9: Uses the CMA-ES (Covariance Matrix Adaptation Evolution Strategy) algorithm of Hansen and Kern (2004), an evolutionary algorithm for difficult non-linear non-convex optimization. <br> 10: Uses the simpsa algorithm, based on the combination of the non-linear simplex and simulated annealing algorithms as proposed by Cardoso, Salcedo and Feyo de Azevedo (1996). <br> 11: This is not strictly speaking an optimization algorithm. The (estimated) parameters are treated as state variables and estimated jointly with the original state variables of the model using a nonlinear filter. The algorithm implemented in Dynare is described in Liu and West (2001), and works with k order local approximations of the model. <br> 12: Uses the particleswarm optimization routine (available under MATLAB if the Global Optimization Toolbox is installed; not available under Octave). <br> 101: Uses the SolveOpt algorithm for local nonlinear optimization problems proposed by Kuntsevich and Kappel (1997). <br> 102: Uses simulannealbnd optimization routine (available under MATLAB if the Global Optimization Toolbox is installed; not available under Octave).<br> Defaut: 4: Chris Sims’s csminwel.'
  },
  {
    estoptid: 22,
    estoptdynarename: 'silent_optimizer',
    estoptname: 'Silent computing',
    estoptplaceholder: '',
    estoptinputtype: 0,
    estoptinfo: 'Instructs Dynare to run mode computing/optimization silently without displaying results or saving files in between. Useful when running loops.'
  },
  {
    estoptid: 23,
    estoptdynarename: 'mcmc_jumping_covariance',
    estoptname: 'Covariance specification',
    estoptplaceholder: 'hessian',
    estoptinputtype: 5,
    estoptinfo: 'Tells Dynare which covariance to use for the proposal density of the MCMC sampler. <br>hessian: Uses the Hessian matrix computed at the mode.<br> prior_variance: Uses the prior variances. No infinite prior variances are allowed in this case.<br> identity_matrix: Uses an identity matrix.<br> Defaut: hessian.'
  },
  {
    estoptid: 24,
    estoptdynarename: 'mode_check',
    estoptname: 'Posterior density plot',
    estoptplaceholder: '',
    estoptinputtype: 0,
    estoptinfo: 'Tells Dynare to plot the posterior density for values around the computed mode for each estimated parameter in turn. This is helpful to diagnose problems with the optimizer. Note that for order>1 the likelihood function resulting from the particle filter is not differentiable anymore due to the resampling step. For this reason, the mode_check plot may look wiggly.'
  },
  {
    estoptid: 25,
    estoptdynarename: 'mode_check_neighbourhood_size',
    estoptname: 'Check plots symmetry ',
    estoptplaceholder: '1',
    estoptinputtype: 1,
    estoptinfo: 'Used in conjunction with option mode_check, if set to 1, tells Dynare to ensure that the check plots are symmetric around the posterior mode. A value of 0 allows to have asymmetric plots, which can be useful if the posterior mode is close to a domain boundary, or in conjunction with mode_check_neighbourhood_size = Inf when the domain in not the entire real line. <br> Defaut: 1.'
  },
  {
    estoptid: 26,
    estoptdynarename: 'mode_check_symmetric_plots',
    estoptname: 'Check plots window',
    estoptplaceholder: '0.5',
    estoptinputtype: 2,
    estoptinfo: 'Used in conjunction with option mode_check, gives the width of the window around the posterior mode to be displayed on the diagnostic plots. This width is expressed in percentage deviation. The Inf value is allowed, and will trigger a plot over the entire domain (see also mode_check_symmetric_plots). <br> Defaut: 0.5.'
  },
  {
    estoptid: 27,
    estoptdynarename: 'mode_check_number_of_points',
    estoptname: 'Points around posterior mode',
    estoptplaceholder: '20',
    estoptinputtype: 1,
    estoptinfo: 'Number of points around the posterior mode where the posterior kernel is evaluated (for each parameter). <br> Defaut: 20.'
  },
  {
    estoptid: 28,
    estoptdynarename: 'prior_trunc',
    estoptname: 'Extreme values probability',
    estoptplaceholder: '1.0e-32',
    estoptinputtype: 4,
    estoptinfo: 'Probability of extreme values of the prior density that is ignored when computing bounds for the parameters.<br> Defaut: 1.0e-32'
  },
  {
    estoptid: 29,
    estoptdynarename: 'huge_number',
    estoptname: 'Infinite value replacement',
    estoptplaceholder: '1.0e7',
    estoptinputtype: 4,
    estoptinfo: 'Value for replacing infinite values in the definition of (prior) bounds when finite values are required for computational reasons.<br> Defaut: 1e7.'
  },
  {
    estoptid: 30,
    estoptdynarename: 'optim',
    estoptname: 'Optimizer options',
    estoptplaceholder: '',
    estoptinputtype: 60,
    estoptinfo: "A list of NAME and VALUE pairs. Can be used to set options for the optimization routines. The set of available options depends on the selected optimization routine (i.e. on the value of option mode_compute). See Dynare documentation for available options.<br> Example: 'NumgradAlgorithm',3,'TolFun',1e-5"
  },
  {
    estoptid: 31,
    estoptdynarename: 'nodiagnostic',
    estoptname: 'No convergence diagnostics',
    estoptplaceholder: '',
    estoptinputtype: 0,
    estoptinfo: 'Does not compute the convergence diagnostics for Metropolis-Hastings.'
  },
  {
    estoptid: 32,
    estoptdynarename: 'bayesian_irf',
    estoptname: 'Posterior distribution of IRFs',
    estoptplaceholder: '',
    estoptinputtype: 0,
    estoptinfo: 'Triggers the computation of the posterior distribution of IRFs. The length of the IRFs are controlled by the irf option. Results are stored in oo_.PosteriorIRF.dsge'
  },
  {
    estoptid: 33,
    estoptdynarename: 'relative_irf',
    estoptname: 'Normalized IRFs.',
    estoptplaceholder: '',
    estoptinputtype: 0,
    estoptinfo: 'Requests the computation of normalized IRFs. At first order, the normal shock vector of size one standard deviation is divided by the standard deviation of the current shock and multiplied by 100. The impulse responses are hence the responses to a unit shock of size 1 (as opposed to the regular shock size of one standard deviation), multiplied by 100. Thus, for a loglinearized model where the variables are measured in percent, the IRFs have the interpretation of the percent responses to a 100 percent shock. For example, a response of 400 of output to a TFP shock shows that output increases by 400 percent after a 100 percent TFP shock (you will see that TFP increases by 100 on impact). Given linearity at order=1, it is straightforward to rescale the IRFs stored in oo_.irfs to any desired size. At higher order, the interpretation is different. The relative_irf option then triggers the generation of IRFs as the response to a 0.01 unit shock (corresponding to 1 percent for shocks measured in percent) and no multiplication with 100 is performed. That is, the normal shock vector of size one standard deviation is divided by the standard deviation of the current shock and divided by 100. For example, a response of 0.04 of log output (thus measured in percent of the steady state output level) to a TFP shock also measured in percent then shows that output increases by 4 percent after a 1 percent TFP shock (you will see that TFP increases by 0.01 on impact).'
  },
  {
    estoptid: 34,
    estoptdynarename: 'dsge_var',
    estoptname: 'DSGE-VAR estimation',
    estoptplaceholder: '',
    estoptinputtype: 2,
    estoptinfo: 'Triggers the estimation of a DSGE-VAR model, where the weight of the DSGE prior of the VAR model is calibrated to the value passed (see Del Negro and Schorfheide (2004)). It represents the ratio of dummy over actual observations. To assure that the prior is proper, the value must be bigger than (k+n)/T, where k is the number of estimated parameters, n is the number of observables, and T is the number of observations.'
  },
  {
    estoptid: 35,
    estoptdynarename: 'dsge_varlag',
    estoptname: 'DSGE-VAR number of lags',
    estoptplaceholder: '4',
    estoptinputtype: 1,
    estoptinfo: 'The number of lags used to estimate a DSGE-VAR model.<br> Defaut: 4.'
  },
  {
    estoptid: 36,
    estoptdynarename: 'posterior_sampling_method',
    estoptname: 'Sampler',
    estoptplaceholder: 'random_walk_metropolis_hastings',
    estoptinputtype: 5,
    estoptinfo: "Selects the sampler used to sample from the posterior distribution during Bayesian estimation. <br>'random_walk_metropolis_hastings': Instructs Dynare to use the Random-Walk Metropolis-Hastings. In this algorithm, the proposal density is recentered to the previous draw in every step. <br>'tailored_random_block_metropolis_hastings': Instructs Dynare to use the Tailored randomized block (TaRB) Metropolis-Hastings algorithm proposed by Chib and Ramamurthy (2010) instead of the standard Random-Walk Metropolis-Hastings. In this algorithm, at each iteration the estimated parameters are randomly assigned to different blocks. For each of these blocks a mode-finding step is conducted. The inverse Hessian at this mode is then used as the covariance of the proposal density for a Random-Walk Metropolis-Hastings step. If the numerical Hessian is not positive definite, the generalized Cholesky decomposition of Schnabel and Eskow (1990) is used, but without pivoting. The TaRB-MH algorithm massively reduces the autocorrelation in the MH draws and thus reduces the number of draws required to representatively sample from the posterior. However, this comes at a computational cost as the algorithm takes more time to run. <br>'independent_metropolis_hastings': Use the Independent Metropolis-Hastings algorithm where the proposal distribution - in contrast to the Random Walk Metropolis-Hastings algorithm - does not depend on the state of the chain. <br>'slice': Instructs Dynare to use the Slice sampler of Planas, Ratto, and Rossi (2015). Note that 'slice' is incompatible with prior_trunc=0.<br> Defaut: random_walk_metropolis_hastings"
  },
  {
    estoptid: 37,
    estoptdynarename: 'posterior_sampler_options',
    estoptname: 'Sampler options',
    estoptplaceholder: '',
    estoptinputtype: 60,
    estoptinfo: 'A list of NAME and VALUE pairs. Can be used to set options for the posterior sampling methods. The set of available options depends on the selected posterior sampling routine (i.e. on the value of option posterior_sampling_method). See Dynare documentation.'
  },
  {
    estoptid: 38,
    estoptdynarename: 'moments_varendo',
    estoptname: 'Posterior distribution of the theoretical moments',
    estoptplaceholder: '',
    estoptinputtype: 0,
    estoptinfo: 'Triggers the computation of the posterior distribution of the theoretical moments of the endogenous variables. Results are stored in oo_.PosteriorTheoreticalMoments (see oo_.PosteriorTheoreticalMoments). The number of lags in the autocorrelation function is controlled by the ar option.'
  },
  {
    estoptid: 39,
    estoptdynarename: 'contemporaneous_correlation',
    estoptname: 'Contemporaneous correlation',
    estoptplaceholder: '',
    estoptinputtype: 0,
    estoptinfo: 'Saves the contemporaneous correlation between the endogenous variables in oo_.contemporaneous_correlation. Results are stored in oo_.PosteriorTheoreticalMoments. Note that the nocorr option has no effect.'
  },
  {
    estoptid: 40,
    estoptdynarename: 'no_posterior_kernel_density',
    estoptname: 'No kernel density estimator',
    estoptplaceholder: '',
    estoptinputtype: 0,
    estoptinfo: 'Shuts off the computation of the kernel density estimator for the posterior objects.'
  },
  {
    estoptid: 41,
    estoptdynarename: 'conditional_variance_decomposition',
    estoptname: 'Conditional variance decomposition distribution',
    estoptplaceholder: '',
    estoptinputtype: 10,
    estoptinfo: 'Computes the posterior distribution of the conditional variance decomposition for the specified period(s). The periods must be strictly positive. Conditional variances are given by var(yt+k|t). For period 1, the conditional variance decomposition provides the decomposition of the effects of shocks upon impact. The results are stored in oo_.PosteriorTheoreticalMoments.dsge.ConditionalVarianceDecomposition.. Note that this option requires the option moments_varendo to be specified. In the presence of measurement error, the field will contain the variance contribution after measurement error has been taken out, i.e. the decomposition will be conducted of the actual as opposed to the measured variables. The variance decomposition of the measured variables will be stored in oo_.PosteriorTheoreticalMoments.dsge.ConditionalVarianceDecompositionME. <br> A range of numbers can be entered by using : as in for instance "50:100". <br> An array of number can be entered by using a space as in for instance "8 9 10 11 15".'
  },
  {
    estoptid: 42,
    estoptdynarename: 'filtered_vars',
    estoptname: 'Posterior distribution of filtered endogenous variables',
    estoptplaceholder: '',
    estoptinputtype: 0,
    estoptinfo: 'Triggers the computation of the posterior distribution of filtered endogenous variables/one-step ahead forecasts, i.e. Etyt+1. Results are stored in oo_.FilteredVariables (see below for a description of this variable).'
  },
  {
    estoptid: 43,
    estoptdynarename: 'smoother',
    estoptname: 'Posterior distribution of smoothed endogenous variables',
    estoptplaceholder: '',
    estoptinputtype: 0,
    estoptinfo: 'Triggers the computation of the posterior distribution of smoothed endogenous variables and shocks, i.e. the expected value of variables and shocks given the information available in all observations up to the final date (ETyt). Results are stored in oo_.SmoothedVariables, oo_.SmoothedShocks and oo_.SmoothedMeasurementErrors. Also triggers the computation of oo_.UpdatedVariables, which contains the estimation of the expected value of variables given the information available at the current date (Etyt). See below for a description of all these variables.'
  },
  {
    estoptid: 44,
    estoptdynarename: 'forecast',
    estoptname: 'Forecast posterior distribution',
    estoptplaceholder: '',
    estoptinputtype: 1,
    estoptinfo: 'Computes the posterior distribution of a forecast on INTEGER periods after the end of the sample used in estimation. If no Metropolis-Hastings is computed, the result is stored in variable oo_.forecast and corresponds to the forecast at the posterior mode. If a Metropolis-Hastings is computed, the distribution of forecasts is stored in variables oo_.PointForecast and oo_.MeanForecast. See Forecasting, for a description of these variables.'
  },
  {
    estoptid: 45,
    estoptdynarename: 'tex',
    estoptname: 'TeX output',
    estoptplaceholder: '',
    estoptinputtype: 0,
    estoptinfo: 'Requests the printing of results and graphs in TeX tables and graphics that can be later directly included in LaTeX files.'
  },
  {
    estoptid: 46,
    estoptdynarename: 'kalman_algo',
    estoptname: 'Kalman filter algo',
    estoptplaceholder: '0',
    estoptinputtype: 1,
    estoptinfo: '0: Automatically use the Multivariate Kalman Filter for stationary models and the Multivariate Diffuse Kalman Filter for non-stationary models.<br> 1: Use the Multivariate Kalman Filter.<br> 2: Use the Univariate Kalman Filter. <br> 3: Use the Multivariate Diffuse Kalman Filter. <br> 4: Use the Univariate Diffuse Kalman Filter.<br> In case of missing observations of single or all series, Dynare treats those missing values as unobserved states and uses the Kalman filter to infer their value (see e.g. Durbin and Koopman (2012), Ch. 4.10) This procedure has the advantage of being capable of dealing with observations where the forecast error variance matrix becomes singular for some variable(s). If this happens, the respective observation enters with a weight of zero in the log-likelihood, i.e. this observation for the respective variable(s) is dropped from the likelihood computations (for details see Durbin and Koopman (2012), Ch. 6.4 and 7.2.5 and Koopman and Durbin (2000)). If the use of a multivariate Kalman filter is specified and a singularity is encountered, Dynare by default automatically switches to the univariate Kalman filter for this parameter draw. This behavior can be changed via the use_univariate_filters_if_singularity_is_detected option.<br> Defaut: 0.'
  },
  {
    estoptid: 47,
    estoptdynarename: 'fast_kalman_filter',
    estoptname: 'Fast Kalman filter',
    estoptplaceholder: '',
    estoptinputtype: 0,
    estoptinfo: 'Select the fast Kalman filter using Chandrasekhar recursions as described by Herbst (2015). This setting is only used with kalman_algo=1 or kalman_algo=3. In case of using the diffuse Kalman filter (kalman_algo=3/lik_init=3), the observables must be stationary. This option is not yet compatible with analytic_derivation.'
  },
  {
    estoptid: 48,
    estoptdynarename: 'kalman_tol',
    estoptname: 'Covariance matrix singularity tolerance',
    estoptplaceholder: '1.0e-10',
    estoptinputtype: 4,
    estoptinfo: 'Numerical tolerance for determining the singularity of the covariance matrix of the prediction errors during the Kalman filter (minimum allowed reciprocal of the matrix condition number).<br> Defaut: 1.0e-10.'
  },
  {
    estoptid: 49,
    estoptdynarename: 'diffuse_kalman_tol',
    estoptname: 'Covariance matric singularity tolerance (diffuse)',
    estoptplaceholder: '1.0e-6',
    estoptinputtype: 4,
    estoptinfo: 'Numerical tolerance for determining the singularity of the covariance matrix of the prediction errors (F∞) and the rank of the covariance matrix of the non-stationary state variables (P∞) during the Diffuse Kalman filter.<br> Defaut: 1.0e-6.'
  },
  {
    estoptid: 50,
    estoptdynarename: 'filter_covariance',
    estoptname: '1-step ahead error of forecast covariance matrices',
    estoptplaceholder: '',
    estoptinputtype: 0,
    estoptinfo: 'Saves the series of one step ahead error of forecast covariance matrices. With Metropolis, they are saved in oo_.FilterCovariance, otherwise in oo_.Smoother.Variance. Saves also k-step ahead error of forecast covariance matrices if filter_step_ahead is set.'
  },
  {
    estoptid: 51,
    estoptdynarename: 'filter_step_ahead',
    estoptname: 'k-step ahead filtered values',
    estoptplaceholder: '',
    estoptinputtype: 10,
    estoptinfo: 'Triggers the computation k-step ahead filtered values, i.e. Etyt+k. Stores results in oo_.FilteredVariablesKStepAhead. Also stores 1-step ahead values in oo_.FilteredVariables. oo_.FilteredVariablesKStepAheadVariances is stored if filter_covariance. <br> A range of numbers can be entered by using : as in for instance "50:100". <br> An array of number can be entered by using a space as in for instance "8 9 10 11 15".'
  },
  {
    estoptid: 52,
    estoptdynarename: 'filter_decomposition',
    estoptname: 'k-step ahead filtered values shock decomposition',
    estoptplaceholder: '',
    estoptinputtype: 0,
    estoptinfo: 'Triggers the computation of the shock decomposition of the above k-step ahead filtered values. Stores results in oo_.FilteredVariablesShockDecomposition.'
  },
  {
    estoptid: 53,
    estoptdynarename: 'smoothed_state_uncertainty',
    estoptname: 'Variance of smoothed estimates',
    estoptplaceholder: '',
    estoptinputtype: 0,
    estoptinfo: 'Triggers the computation of the variance of smoothed estimates, i.e. varT(yt). Stores results in oo_.Smoother.State_uncertainty.'
  },
  {
    estoptid: 54,
    estoptdynarename: 'diffuse_filter',
    estoptname: 'Diffuse Kalman filter',
    estoptplaceholder: '',
    estoptinputtype: 0,
    estoptinfo: 'Uses the diffuse Kalman filter (as described in Durbin and Koopman (2012) and Koopman and Durbin (2003) for the multivariate and Koopman and Durbin (2000) for the univariate filter) to estimate models with non-stationary observed variables.<br> When diffuse_filter is used the lik_init option of estimation has no effect.<br> When there are nonstationary exogenous variables in a model, there is no unique deterministic steady state. For instance, if productivity is a pure random walk: at=at-1+et any value of a¯ of a is a deterministic steady state for productivity. Consequently, the model admits an infinity of steady states. In this situation, the user must help Dynare in selecting one steady state, except if zero is a trivial model’s steady state, which happens when the linear option is used in the model declaration. The user can either provide the steady state to Dynare using a steady_state_model block (or writing a steady state file) if a closed form solution is available, see steady_state_model, or specify some constraints on the steady state, see equation_tag_for_conditional_steady_state, so that Dynare computes the steady state conditionally on some predefined levels for the non stationary variables. In both cases, the idea is to use dummy values for the steady state level of the exogenous non stationary variables.<br> Note that the nonstationary variables in the model must be integrated processes (their first difference or k-difference must be stationary).'
  },
  {
    estoptid: 55,
    estoptdynarename: 'selected_variables_only',
    estoptname: 'Selected variables smoothing',
    estoptplaceholder: '',
    estoptinputtype: 0,
    estoptinfo: 'Only run the classical smoother on the variables listed just after the estimation command. This option is incompatible with requesting classical frequentist forecasts and will be overridden in this case. When using Bayesian estimation, the smoother is by default only run on the declared endogenous variables. Default: run the smoother on all the declared endogenous variables.'
  },
  {
    estoptid: 56,
    estoptdynarename: 'cova_compute',
    estoptname: 'Covariance matric computation',
    estoptplaceholder: '1',
    estoptinputtype: 1,
    estoptinfo: 'When 0, the covariance matrix of estimated parameters is not computed after the computation of posterior mode (or maximum likelihood). This increases speed of computation in large models during development, when this information is not always necessary. Of course, it will break all successive computations that would require this covariance matrix. Otherwise, if this option is equal to 1, the covariance matrix is computed and stored in variable hh of MODEL_FILENAME_mode.mat.<br> Defaut: 1.'
  },
  {
    estoptid: 57,
    estoptdynarename: 'solve_algo',
    estoptname: 'Non-linear solver algo',
    estoptplaceholder: '4',
    estoptinputtype: 1,
    estoptinfo: 'Determines the non-linear solver to use.Possible values for OPTION are: <br> 0: Use fsolve (under MATLAB, only available if you have the Optimization Toolbox; always available under Octave). <br>1: Use Dynare’s own nonlinear equation solver (a Newton-like algorithm with line-search). <br>2: Splits the model into recursive blocks and solves each block in turn using the same solver as value 1. <br>3: Use Chris Sims’ solver. <br>4: Splits the model into recursive blocks and solves each block in turn using a trust-region solver with autoscaling. <br>5: Newton algorithm with a sparse Gaussian elimination (SPE) (requires bytecode option, see Model declaration). <br>6: Newton algorithm with a sparse LU solver at each iteration (requires bytecode and/or block option, see Model declaration). <br>7: Newton algorithm with a Generalized Minimal Residual (GMRES) solver at each iteration (requires bytecode and/or block option, see Model declaration). <br>8: Newton algorithm with a Stabilized Bi-Conjugate Gradient (BICGSTAB) solver at each iteration (requires bytecode and/or block option, see Model declaration). <br>9: Trust-region algorithm on the entire model. <br>10: Levenberg-Marquardt mixed complementarity problem (LMMCP) solver (Kanzow and Petra (2004)). <br>11: PATH mixed complementarity problem solver of Ferris and Munson (1999). The complementarity conditions are specified with an mcp equation tag, see lmmcp. Dynare only provides the interface for using the solver. Due to licence restrictions, you have to download the solver’s most current version yourself from http://pages.cs.wisc.edu/~ferris/path.html and place it in MATLAB’s search path.<br> Defaut: 4: Splits the model into recursive blocks and solves each block in turn using a trust-region solver with autoscaling.'
  },
  {
    estoptid: 58,
    estoptdynarename: 'order',
    estoptname: 'Order of approximation around the deterministic SS',
    estoptplaceholder: '1',
    estoptinputtype: 1,
    estoptinfo: 'Order of approximation around the deterministic steady state. When greater than 1, the likelihood is evaluated with a particle or nonlinear filter (see Fernandez-Villaverde and Rubio-Ramirez (2005)).<br> Defaut:  1, i.e. the likelihood of the linearized model is evaluated using a standard Kalman filter.'
  },
  {
    estoptid: 59,
    estoptdynarename: 'irf',
    estoptname: 'IRF number of periods',
    estoptplaceholder: '40',
    estoptinputtype: 1,
    estoptinfo: 'Number of periods on which to compute the IRFs. Setting irf=0 suppresses the plotting of IRFs. Only used if bayesian_irf is passed. <br> Defaut: 40.'
  },
  {
    estoptid: 60,
    estoptdynarename: 'irf_plot_threshold',
    estoptname: 'Threshold size for plotting IRFs',
    estoptplaceholder: '1.0e-10',
    estoptinputtype: 2,
    estoptinfo: 'Threshold size for plotting IRFs. All IRFs for a particular variable with a maximum absolute deviation from the steady state smaller than this value are not displayed.<br> Defaut: 1.0e-10.'
  },
  {
    estoptid: 61,
    estoptdynarename: 'aim_solver',
    estoptname: 'AIM solver',
    estoptplaceholder: '',
    estoptinputtype: 0,
    estoptinfo: 'Use the Anderson-Moore Algorithm (AIM) to compute the decision rules, instead of using Dynare’s default method based on a generalized Schur decomposition. This option is only valid for first order approximation. See AIM website for more details on the algorithm.'
  },
  {
    estoptid: 62,
    estoptdynarename: 'sylvester',
    estoptname: 'Sylvester equation solver',
    estoptplaceholder: 'default',
    estoptinputtype: 5,
    estoptinfo: 'Determines the algorithm used to solve the Sylvester equation for block decomposed model. Possible values for OPTION are: <br>default: Uses the default solver for Sylvester equations (gensylv) based on Ondra Kamenik’s algorithm (see here for more information). <br>fixed_point: Uses a fixed point algorithm to solve the Sylvester equation (gensylv_fp). This method is faster than the default one for large scale models.<br> Defaut: default'
  },
  {
    estoptid: 63,
    estoptdynarename: 'sylvester_fixed_point_tol',
    estoptname: 'Sylvester solver convergence',
    estoptplaceholder: '1.0e-12',
    estoptinputtype: 2,
    estoptinfo: 'The convergence criterion used in the fixed point Sylvester solver.<br> Defaut: 1.0e-12.'
  },
  {
    estoptid: 64,
    estoptdynarename: 'lyapunov',
    estoptname: 'Lyapunov equation solver',
    estoptplaceholder: 'default',
    estoptinputtype: 5,
    estoptinfo: 'Determines the algorithm used to solve the Lyapunov equation to initialized the variance-covariance matrix of the Kalman filter using the steady-state value of state variables. Possible values for OPTION are:  <br>default: Uses the default solver for Lyapunov equations based on Bartels-Stewart algorithm. <br>fixed_point: Uses a fixed point algorithm to solve the Lyapunov equation. This method is faster than the default one for large scale models, but it could require a large amount of iterations. <br>doubling: Uses a doubling algorithm to solve the Lyapunov equation (disclyap_fast). This method is faster than the two previous one for large scale models. <br>square_root_solver: Uses a square-root solver for Lyapunov equations (dlyapchol). This method is fast for large scale models (available under MATLAB if the Control System Toolbox is installed; available under Octave if the control package from Octave-Forge is installed).<br> Defaut: default.'
  },
  {
    estoptid: 65,
    estoptdynarename: 'lyapunov_fixed_point_tol',
    estoptname: '',
    estoptplaceholder: '1.0e-10',
    estoptinputtype: 2,
    estoptinfo: 'This is the convergence criterion used in the fixed point Lyapunov solver.<br> Defaut: 1.0e-10.'
  },
  {
    estoptid: 66,
    estoptdynarename: 'lyapunov_doubling_tol',
    estoptname: '',
    estoptplaceholder: '1.0e-16',
    estoptinputtype: 2,
    estoptinfo: 'This is the convergence criterion used in the doubling algorithm to solve the Lyapunov equation.<br> Defaut: 1.0e-16'
  },
  {
    estoptid: 67,
    estoptdynarename: 'use_penalized_objective_for_hessian',
    estoptname: '',
    estoptplaceholder: '',
    estoptinputtype: 0,
    estoptinfo: 'Use the penalized objective instead of the objective function to compute numerically the hessian matrix at the mode. The penalties decrease the value of the posterior density (or likelihood) when, for some perturbations, Dynare is not able to solve the model (issues with steady state existence, Blanchard and Kahn conditions, …). In pratice, the penalized and original objectives will only differ if the posterior mode is found to be near a region where the model is ill-behaved. By default the original objective function is used.'
  },
  {
    estoptid: 68,
    estoptdynarename: 'analytic_derivation',
    estoptname: '',
    estoptplaceholder: '',
    estoptinputtype: 0,
    estoptinfo: 'Triggers estimation with analytic gradient. The final hessian is also computed analytically. Only works for stationary models without missing observations, i.e. for kalman_algo<3.'
  },
  {
    estoptid: 69,
    estoptdynarename: 'ar',
    estoptname: '',
    estoptplaceholder: '5',
    estoptinputtype: 1,
    estoptinfo: 'Order of autocorrelation coefficients to compute and to print. Only useful in conjunction with option moments_varendo.<br> Defaut: 5.'
  },
  {
    estoptid: 70,
    estoptdynarename: 'endogenous_prior',
    estoptname: '',
    estoptplaceholder: '',
    estoptinputtype: 0,
    estoptinfo: 'Use endogenous priors as in Christiano, Trabandt and Walentin (2011). The procedure is motivated by sequential Bayesian learning. Starting from independent initial priors on the parameters, specified in the estimated_params block, the standard deviations observed in a “pre-sample”, taken to be the actual sample, are used to update the initial priors. Thus, the product of the initial priors and the pre-sample likelihood of the standard deviations of the observables is used as the new prior (for more information, see the technical appendix of Christiano, Trabandt and Walentin (2011)). This procedure helps in cases where the regular posterior estimates, which minimize in-sample forecast errors, result in a large overprediction of model variable variances (a statistic that is not explicitly targeted, but often of particular interest to researchers).'
  },
  {
    estoptid: 71,
    estoptdynarename: 'use_univariate_filters_if_singularity_is_detected',
    estoptname: '',
    estoptplaceholder: '1',
    estoptinputtype: 1,
    estoptinfo: 'Decide whether Dynare should automatically switch to univariate filter if a singularity is encountered in the likelihood computation (this is the behaviour if the option is equal to 1). Alternatively, if the option is equal to 0, Dynare will not automatically change the filter, but rather use a penalty value for the likelihood when such a singularity is encountered.<br> Defaut: 1.'
  },
  {
    estoptid: 72,
    estoptdynarename: 'keep_kalman_algo_if_singularity_is_detected',
    estoptname: '',
    estoptplaceholder: '',
    estoptinputtype: 0,
    estoptinfo: 'With the default use_univariate_filters_if_singularity_is_detected=1, Dynare will switch to the univariate Kalman filter when it encounters a singular forecast error variance matrix during Kalman filtering. Upon encountering such a singularity for the first time, all subsequent parameter draws and computations will automatically rely on univariate filter, i.e. Dynare will never try the multivariate filter again. Use the keep_kalman_algo_if_singularity_is_detected option to have the use_univariate_filters_if_singularity_is_detected only affect the behavior for the current draw/computation.'
  },
  {
    estoptid: 73,
    estoptdynarename: 'rescale_prediction_error_covariance',
    estoptname: '',
    estoptplaceholder: '',
    estoptinputtype: 0,
    estoptinfo: 'Rescales the prediction error covariance in the Kalman filter to avoid badly scaled matrix and reduce the probability of a switch to univariate Kalman filters (which are slower). By default no rescaling is done.'
  },
  {
    estoptid: 74,
    estoptdynarename: 'qz_zero_threshold',
    estoptname: '',
    estoptplaceholder: '1.0e-6',
    estoptinputtype: 2,
    estoptinfo: 'Value used to test if a generalized eigenvalue is 0/0 in the generalized Schur decomposition (in which case the model does not admit a unique solution).<br> Defaut: 1.0e-6.'
  },
  {
    estoptid: 75,
    estoptdynarename: 'taper_steps',
    estoptname: '',
    estoptplaceholder: '4 8 15',
    estoptinputtype: 11,
    estoptinfo: 'Percent tapering used for the spectral window in the Geweke (1992,1999) convergence diagnostics (requires mh_nblocks=1). The tapering is used to take the serial correlation of the posterior draws into account. <br> An array of number can be entered by using a space as in for instance "8 9 10 11 15".<br> Defaut: 4 8 15'
  },
  {
    estoptid: 76,
    estoptdynarename: 'geweke_interval',
    estoptname: '',
    estoptplaceholder: '0.2 0.5',
    estoptinputtype: 21,
    estoptinfo: 'Percentage of MCMC draws at the beginning and end of the MCMC chain taken to compute the Geweke (1992,1999) convergence diagnostics (requires mh_nblocks=1) after discarding the first mh_drop = DOUBLE percent of draws as a burnin. <br> An array of number can be entered by using a space as in for instance "8.8 9.01 15.3".<br> Defaut: 0.2 0.5'
  },
  {
    estoptid: 77,
    estoptdynarename: 'raftery_lewis_diagnostics',
    estoptname: '',
    estoptplaceholder: '',
    estoptinputtype: 0,
    estoptinfo: 'Triggers the computation of the Raftery and Lewis (1992) convergence diagnostics. The goal is deliver the number of draws required to estimate a particular quantile of the CDF q with precision r with a probability s. Typically, one wants to estimate the q=0.025 percentile (corresponding to a 95 percent HPDI) with a precision of 0.5 percent (r=0.005) with 95 percent certainty (s=0.95). The defaults can be changed via raftery_lewis_qrs. Based on the theory of first order Markov Chains, the diagnostics will provide a required burn-in (M), the number of draws after the burnin (N) as well as a thinning factor that would deliver a first order chain (k). The last line of the table will also deliver the maximum over all parameters for the respective values.'
  },
  {
    estoptid: 78,
    estoptdynarename: 'raftery_lewis_qrs',
    estoptname: '',
    estoptplaceholder: '0.025 0.005 0.95',
    estoptinputtype: 21,
    estoptinfo: 'Sets the quantile of the CDF q that is estimated with precision r with a probability s in the Raftery and Lewis (1992) convergence diagnostics. <br> An array of number can be entered by using a space as in for instance "0.025 0.005 0.95".<br> Defaut: 0.025 0.005 0.95'
  },
  {
    estoptid: 79,
    estoptdynarename: 'consider_all_endogenous',
    estoptname: '',
    estoptplaceholder: '',
    estoptinputtype: 0,
    estoptinfo: 'Compute the posterior moments, smoothed variables, k-step ahead filtered variables and forecasts (when requested) on all the endogenous variables. This is equivalent to manually listing all the endogenous variables after the estimation command.'
  },
  {
    estoptid: 80,
    estoptdynarename: 'number_of_particles',
    estoptname: '',
    estoptplaceholder: '1000',
    estoptinputtype: 1,
    estoptinfo: 'Number of particles used when evaluating the likelihood of a non linear state space model.<br> Defaut: 1000.'
  },
  {
    estoptid: 81,
    estoptdynarename: 'resampling',
    estoptname: '',
    estoptplaceholder: '',
    estoptinputtype: 5,
    estoptinfo: 'Determines if resampling of the particles is done. Possible values for OPTION are: <br> none: No resampling. <br> systematic: Resampling at each iteration, this is the default value.<br> generic: Resampling if and only if the effective sample size is below a certain level defined by resampling_threshold * number_of_particles.<br> Defaut: '
  },
  {
    estoptid: 82,
    estoptdynarename: 'resampling_threshold',
    estoptname: '',
    estoptplaceholder: '',
    estoptinputtype: 2,
    estoptinfo: 'A real number between zero and one. The resampling step is triggered as soon as the effective number of particles is less than this number times the total number of particles (as set by number_of_particles). This option is effective if and only if option resampling has value generic.'
  },
  {
    estoptid: 83,
    estoptdynarename: 'resampling_method',
    estoptname: '',
    estoptplaceholder: '',
    estoptinputtype: 5,
    estoptinfo: 'Sets the resampling method. Possible values for OPTION are: <br>kitagawa, <br>stratified, <br>smooth.'
  },
  {
    estoptid: 84,
    estoptdynarename: 'filter_algorithm',
    estoptname: '',
    estoptplaceholder: '',
    estoptinputtype: 5,
    estoptinfo: 'Sets the particle filter algorithm. Possible values for OPTION are: <br>sis: Sequential importance sampling algorithm, this is the default value. <br>apf: Auxiliary particle filter. <br>gf: Gaussian filter. <br>gmf: Gaussian mixture filter. <br>cpf: Conditional particle filter. <br>nlkf: Use a standard (linear) Kalman filter algorithm with the nonlinear measurement and state equations.'
  },
  {
    estoptid: 85,
    estoptdynarename: 'proposal_approximation',
    estoptname: '',
    estoptplaceholder: 'unscented',
    estoptinputtype: 5,
    estoptinfo: 'Sets the method for approximating the proposal distribution. Possible values for OPTION are: <br>cubature,<br> montecarlo, <br>unscented.<br> Default value is unscented.'
  },
  {
    estoptid: 86,
    estoptdynarename: 'distribution_approximation',
    estoptname: '',
    estoptplaceholder: 'unscented',
    estoptinputtype: 5,
    estoptinfo: 'Sets the method for approximating the particle distribution. Possible values for OPTION are: <br>cubature,<br> montecarlo, <br>unscented.<br> Defaut: unscented.'
  },
  {
    estoptid: 87,
    estoptdynarename: 'cpf_weights',
    estoptname: '',
    estoptplaceholder: 'amisanotristani',
    estoptinputtype: 5,
    estoptinfo: 'Controls the method used to update the weights in conditional particle filter, possible values are:<br> amisanotristani: (Amisano et al. (2010)), <br> murrayjonesparslow: (Murray et al. (2013)).<br> Defaut: amisanotristani.'
  },
  {
    estoptid: 88,
    estoptdynarename: 'nonlinear_filter_initialization',
    estoptname: '',
    estoptplaceholder: '',
    estoptinputtype: 1,
    estoptinfo: 'Sets the initial condition of the nonlinear filters. By default the nonlinear filters are initialized with the unconditional covariance matrix of the state variables, computed with the reduced form solution of the first order approximation of the model. If nonlinear_filter_initialization=2, the nonlinear filter is instead initialized with a covariance matrix estimated with a stochastic simulation of the reduced form solution of the second order approximation of the model. Both these initializations assume that the model is stationary, and cannot be used if the model has unit roots (which can be seen with the check command prior to estimation). If the model has stochastic trends, user must use nonlinear_filter_initialization=3, the filters are then initialized with an identity matrix for the covariance matrix of the state variables. Default value is nonlinear_filter_initialization=1 (initialization based on the first order approximation of the model).'
  }
];

createestimoptions();