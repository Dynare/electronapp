const ipc = require('electron').ipcRenderer;
const showconsolealert = require('./appModules').showconsolealert;
const IsNumeric = require('./appModules').IsNumeric;
const loadjsonfile = require('./appModules').loadjsonfile;
const popthis = require('./appModules').popthis;
// const setpreprocessmore = require('./runprocesses').setpreprocessmore;
const runstochasticnode = require('./runprocesses').runstochasticnode;

// const path = require('path');
// var fs = require('fs');
// const updatedbSS = require('./appModules').updatedbSS;
// 
// const remote = require('electron').remote;
// const app = remote.app;



var stomatlabdata;
var stonumshocks = [];
for (i = 0; i < 1000; i++) {
  stonumshocks[i] = 0;
}





//------------------------------------------------------------@@@button set perfect
$('#setstochastic').click(async function () {

  // let basedir = path.resolve(__dirname, './assets/modfiles/');

  let modelname = $('#codename').val();
  if (modelname == null || modelname == "") {
    $.alert({
      title: 'Project name is empty.',
      content: 'Please choose a name for this project...',
    });
    return false;
  }

  let modelcode = {
    'modelcode': window.editor_dynare.getValue(),
    'modelhash': $('#codehash').val(),
    'modelname': $('#codename').val()
  };

  // calling the 'node side'
  try {
    var resp = await setpreprocessmore(modelcode);
  } catch (error) {
    if (error.status == 200) {
      return popthis('Error', error.message);
    } else if (error.status == 100) {
      showconsolealert(error.message, 1);
      return false;
    }
  }

  showconsolealert(resp.message, 1);
  scrolltextarea();

  //the model has exo shocks: we make a table
  // if a steady state has not been provided if ask extra info on that
  dynaremodel = resp.modeljson;

  // steady-state compute table
  document.getElementById("stosimssinittable").innerHTML = "";
  if (resp.status == 201) {
    var extrassinfo = "";
    var extrasshelp = "";
    if (resp.extrassinfo !== null) {
      extrassinfo = resp.extrassinfo;
      extrasshelp = " (the last known steady-state info have been loaded below)"
    }
    var extrasstype = "";
    if (resp.extrasstype == 1) {
      extrasstype = 'checked';
    }

    sscomputeeendo = `
      <h4>Steady state</h4>
      <p style="color:#3399CC">You did not provide any way to compute the steady-state. Please specify a way to compute the steady-state` + extrasshelp + `.</p>
      <div class="form-group no-margin"> <textarea class="form-control" id="stoSSblock" name="stoSSblock" placeholder="Steady-state equations;" rows="7">` + extrassinfo + `</textarea> </div>
      <div class="form-group no-margin"><label><input type="checkbox" name="stoissteadystateblock" id="stoissteadystateblock"` + extrasstype + `/> Check if the above will compute the exact steady-state (will be interpreted as a <span style="font-style: italic;">steady_state_model</span> block). Leave unchecked if the above should be used as a starting point to compute the steady-state (will be interpreted as an <span style="font-style: italic;">initval</span> block).</label></div>
      <input id="stomodelsteadystatestatus" name="stomodelsteadystatestatus" value="99" type="hidden">
      `;
  } else if (resp.status == 2) {
    sscomputeeendo = `
      <input id="stomodelsteadystatestatus" name="stomodelsteadystatestatus" value="55" type="hidden">
      `;
  } else {
    sscomputeeendo = `
      <input id="stomodelsteadystatestatus" name="stomodelsteadystatestatus" value="0" type="hidden">
      `;
  }
  document.getElementById("stosimssinittable").innerHTML = sscomputeeendo;
  document.getElementById("stomodel_endonum").value = (dynaremodel.endogenous).length;
  document.getElementById("stomodel_exonum").value = (dynaremodel.exogenous).length;
  $('#stosimModal').modal('show');

  return false;

});

//--------------------------------------------@@@button add sequence of shocks code
$('#stoaddshock').click(function () {
  var storowindex;
  var stoxcond = 0;
  var stoiter = 0;
  while (stoxcond == 0) {
    if (stonumshocks[stoiter] == 0) {
      stoxcond = 1;
      storowindex = stoiter;
      stonumshocks[stoiter] = 1;
    }
    stoiter = stoiter + 1;
  }
  stoexpectedshockstable = '';
  stoexpectedshockstable += '<tr><td><select id="stoshockname' + storowindex + '" class="form-control input-sm" name="stoshockname">';
  for (x in dynaremodel.exogenous) {
    stoexpectedshockstable += '<option value="' + x + '">' + dynaremodel.exogenous[x]["longName"] + '</option>';
  }
  stoexpectedshockstable += "</select></td>";
  stoexpectedshockstable += '<td><select id="stoshockattribute' + storowindex + '" class="form-control input-sm" name="stoshockattribute' + storowindex + '" onchange="disableassociated(' + storowindex + ')"><option value="1" selected="selected">Standard error</option><option value="2">Variance</option><option value="3">Covariance</option><option value="4">Correlation</option></td>';
  stoexpectedshockstable += '<td><input type="text" class="form-control no-border input-sm" name="stoshockvalue' + storowindex + '" id="stoshockvalue' + storowindex + '" placeholder="Value"></td>';
  stoexpectedshockstable += '<td><select id="stoassoshockname' + storowindex + '" class="form-control input-sm" name="stoassoshockname" disabled>';
  for (x in dynaremodel.exogenous) {
    stoexpectedshockstable += '<option value="' + x + '">' + dynaremodel.exogenous[x]["longName"] + '</option>';
  }
  stoexpectedshockstable += "</select></td>";
  stoexpectedshockstable += '<td><input type="button" value="X" onclick="StoSomeDeleteRowFunction(this,' + storowindex + ')" class="form-control no-border input-sm" name="storemoveshock' + storowindex + '" id="storemoveshock' + storowindex + '"></td></tr>';
  $('#stosimseqexptable tbody').append(stoexpectedshockstable);
  return false;
});

//----------------------------------------------------@@@removes a shock line in Shocks table
function StoSomeDeleteRowFunction(o, storowindex) {
  var p = o.parentNode.parentNode;
  p.parentNode.removeChild(p);
  stonumshocks[storowindex] = 0;
}

function disableassociated(shockindex) {
  var selectedvalue = $("#stoshockattribute" + shockindex).val();
  if (selectedvalue < 3) {
    $("#stoassoshockname" + shockindex).attr('disabled', 'disabled');
  } else {
    $("#stoassoshockname" + shockindex).removeAttr('disabled');
  }
  //cl(selectedvalue);
}






$('#runstochastic').click(async function () {

  //getting all the necessary info from the forms
  var ssstatus = $('#stomodelsteadystatestatus').val();
  var endonum = $('#stomodel_endonum').val();
  var exonum = $('#stomodel_exonum').val();
  var ssdescription = [];
  var globalvaluescount = 0;

  // checking the shocks tab
  var stochasticshocksdescription = [];
  var shockindex;
  var attributeflag = [];
  var shockspairs = [];
  var sstype = 0;

  for (i = 0; i < exonum; i++) {
    attributeflag[i] = 0;
  }

  // steady-state info check
  if (ssstatus == 99) {
    sstext = $('#stoSSblock').val();
    if (sstext.length == 0) {
      $('.nav-tabs a[href="#stosimsetuptable"]').tab('show');
      popthis('Steady state error.', 'Please provide a way to compute the steady-state.');
      return (0);
    }
    if ($('#stoissteadystateblock').is(":checked") == true) {
      var sstype = 1;
    } else {
      var sstype = 2;
    }
    ssdescription.push({
      'sstext': sstext,
      'sstype': parseInt(sstype, 10)
    });
  }



  // Shocks tab checks

  var rowCount = $('#stosimseqexptable tr').length - 1; // careful this counts the th row as a row, thus the -1

  if (rowCount > 0) {
    // there are shocks in the table
    globalvaluescount = 1;
    var stoxcond = 0;
    var stoiter = 0;
    var countshocks = 0;

    while (stoxcond == 0) {
      // we iterate over global stonumshocks that kept track of shock indexes in any order they were entered
      if (stonumshocks[stoiter] == 1) {
        // if stonumshocks identifies a valid row in the table, we start further checks
        countshocks = countshocks + 1;

        //we find the true row number of the current checked line in the table
        var stotablerow = $('#stoshockname' + stoiter.toString()).closest('tr');
        var stotablerowindex = stotablerow.index() + 1;

        // We fetch shock infos
        shockindex = $('#stoshockname' + stoiter.toString()).val();
        shockattributevalue = $("#stoshockattribute" + stoiter.toString()).val();
        shockvalue = $('#stoshockvalue' + stoiter.toString()).val();
        shockvalue = shockvalue.trim();

        assoshockindex = -10;
        if (shockattributevalue > 2) { //covariance or correation was selected
          assoshockindex = $('#stoassoshockname' + stoiter.toString()).val();
          if (shockindex == assoshockindex) {
            $('.nav-tabs a[href="#stosimseqexp"]').tab('show');
            popthis('Shocks error.', 'On row ' + stotablerowindex + ', the covariance/correlation should be taken for separate shocks.');
            return (0);
          }
          // we verify if covariance/correlation has already been specified for any two same shocks
          if (Number(assoshockindex) > Number(shockindex)) {
            if (shockspairs.indexOf(shockindex + '-' + assoshockindex) == -1) {
              shockspairs.push(shockindex + '-' + assoshockindex);
            } else {
              $('.nav-tabs a[href="#stosimseqexp"]').tab('show');
              popthis('Shocks error.', 'On row ' + stotablerowindex + ', this covariance/correlation has already been specified between these shocks.');
              return (0);
            }
          } else {
            if (shockspairs.indexOf(assoshockindex + '-' + shockindex) == -1) {
              shockspairs.push(assoshockindex + '-' + shockindex);
            } else {
              $('.nav-tabs a[href="#stosimseqexp"]').tab('show');
              popthis('Shocks error.', 'On row ' + stotablerowindex + ', this covariance/correlation has already been specified between these shocks.');
              return (0);
            }
          }

          if (shockattributevalue == 3) {
            //for covariances we need to make sure that both variances have been declared
            var stoycond = 0;
            var stoyiter = 0;
            var stoyflag = 0;
            var stoymainflag = 0;
            var stoyassoflag = 0;
            var countyshocks = 0;
            while (stoycond == 0) {
              if (stonumshocks[stoyiter] == 1) {
                countyshocks = countyshocks + 1;
                if (($('#stoshockname' + stoyiter.toString()).val() == shockindex) && (($("#stoshockattribute" + stoyiter.toString()).val() == 1) || ($("#stoshockattribute" + stoyiter.toString()).val() == 2))) {
                  if (stoymainflag == 0) {
                    stoyflag = stoyflag + 1;
                    stoymainflag = 1
                  }
                }
                if (($('#stoshockname' + stoyiter.toString()).val() == assoshockindex) && (($("#stoshockattribute" + stoyiter.toString()).val() == 1) || ($("#stoshockattribute" + stoyiter.toString()).val() == 2))) {
                  if (stoyassoflag == 0) {
                    stoyflag = stoyflag + 1;
                    stoyassoflag = 1
                  }
                }
              }
              if (countyshocks == rowCount) {
                stoycond = 1;
              }
              stoyiter = stoyiter + 1;
            }
            if (stoyflag < 2) {
              $('.nav-tabs a[href="#stosimseqexp"]').tab('show');
              popthis('Shocks error.', 'On row ' + stotablerowindex + ', the covariance relation requires that the variances (or std deviation) of both related shocks are declared. Please declare a value for both of them.');
              return (0);
            }


          }

        } else {
          //we check if the same shock already has a std or variance flag
          if (attributeflag[shockindex] > 0) {
            $('.nav-tabs a[href="#stosimseqexp"]').tab('show');
            popthis('Shocks error.', 'On row ' + stotablerowindex + ', You have already specified a variance or standard deviation attribute for this shock. Please keep only one such attribute per shock.');
            return (0);
          } else {
            attributeflag[shockindex] = shockattributevalue;
          }
        }

        // we validate the shock value
        if (shockvalue == null || shockvalue == "") {
          $('.nav-tabs a[href="#stosimseqexp"]').tab('show');
          popthis('Shocks error.', 'You did not specify any value for shock on row ' + countshocks + '.');
          return (0);
        } else {
          if (!(IsNumeric(shockvalue))) {
            $('.nav-tabs a[href="#stosimseqexp"]').tab('show');
            popthis('Shocks error.', 'The shock value on row ' + countshocks + ' is not a number.');
            return (0);
          }
        }

        stochasticshocksdescription.push({
          'shockindex': parseInt(shockindex, 10),
          'shockattributevalue': parseInt(shockattributevalue, 10),
          'shockvalue': parseFloat(shockvalue),
          'assoshockindex': parseInt(assoshockindex, 10)
        });

        //we count the number of this specific shock
        // shocksflag[shockindex] = shocksflag[shockindex] + 1;
      }
      if (countshocks == rowCount) {
        stoxcond = 1;
      }
      stoiter = stoiter + 1;
    }
  }

  //checking options tab
  var stooptionsdescription = [];
  var stooptcounter = 0;
  var stooptval;
  var stooptdyn;
  var sto0checker = 0;


  for (let element of stooptionslist) {

    if ($("#stoopt_id_" + stooptcounter.toString()).is(':checked')) {

      if (element.stooptinputtype == 0) { //checking no input options
        stooptionsdescription.push({
          'stooptindex': parseInt(stooptcounter, 10),
          'stooptdynarename': element.stooptdynarename,
          'stooptinputtype': parseInt(element.stooptinputtype, 10),
          'stooptvalue': ''
        });
      }

      if (element.stooptinputtype == 1) { //checking integer input options

        stooptval = $('#stoopt_input_' + stooptcounter.toString()).val();
        stooptval = stooptval.trim();
        stooptdyn = (element.stooptdynarename).bold();

        if ((stooptval == null || stooptval == "")) {
          $('.nav-tabs a[href="#stooptions"]').tab('show');
          popthis('Options error.', 'You have activated the ' + stooptdyn + ' option without specifying a value. Please deactivate this option if you are not using it or want to use its default value.');
          return (0);
        } else {
          if (!(Number.isInteger(Number(stooptval)))) {
            $('.nav-tabs a[href="#stooptions"]').tab('show');
            popthis('Options error.', stooptdyn + ' value should be an integer number.');
            return (0);
          }
          // if (Number(stooptval) < 0) {
          //   $('.nav-tabs a[href="#stooptions"]').tab('show');
          //   popthis('Options error.', stooptdyn + ' value cannot be negative. Please deactivate this option to use the default value.');
          //   return (0);
          // }
        }

        stooptionsdescription.push({
          'stooptindex': parseInt(stooptcounter, 10),
          'stooptdynarename': element.stooptdynarename,
          'stooptinputtype': parseInt(element.stooptinputtype, 10),
          'stooptvalue': parseInt(stooptval, 10)
        });
      }


      if ((element.stooptinputtype == 2) || (element.stooptinputtype == 4)) { //checking double and double with 'e' input options

        stooptval = $('#stoopt_input_' + stooptcounter.toString()).val();
        stooptval = stooptval.trim();
        stooptdyn = (element.stooptdynarename).bold();

        if ((stooptval == null || stooptval == "")) {
          $('.nav-tabs a[href="#stooptions"]').tab('show');
          popthis('Options error.', 'You have activated the ' + stooptdyn + ' option without specifying a value. Please deactivate this option if you are not using it or want to use its default value.');
          return (0);
        } else {
          if (!(IsNumeric(stooptval))) {
            $('.nav-tabs a[href="#stooptions"]').tab('show');
            popthis('Options error.', stooptdyn + ' value should be a number.');
            return (0);
          }
        }

        stooptionsdescription.push({
          'stooptindex': parseInt(stooptcounter, 10),
          'stooptdynarename': element.stooptdynarename,
          'stooptinputtype': parseInt(element.stooptinputtype, 10),
          'stooptvalue': parseFloat(stooptval, 10)
        });
      }


      if ((element.stooptinputtype == 10) || (element.stooptinputtype == 11)) { //checking integers, range of integers or array of integers

        stooptval = $('#stoopt_input_' + stooptcounter.toString()).val();
        stooptval = stooptval.trim();
        stooptdyn = (element.stooptdynarename).bold();

        if ((stooptval == null || stooptval == "")) { //entry is empty
          $('.nav-tabs a[href="#stooptions"]').tab('show');
          popthis('Options error.', 'You have activated the ' + stooptdyn + ' option without specifying a value. Please deactivate this option if you are not using it or want to use its default value.');
          return (0);
        } else {

          if ((IsNumeric(stooptval))) { //we verify if entry is a number

            if (!(Number.isInteger(Number(stooptval)))) { //if it's a number it has to be a single integer
              $('.nav-tabs a[href="#stooptions"]').tab('show');
              popthis('Options error.', stooptdyn + ' value should be an integer number.');
              return (0);
            }

            var dynlist = [];
            dynlist.push({
              'varname': parseInt(stooptval, 10)
            });

            stooptionsdescription.push({
              'stooptindex': parseInt(stooptcounter, 10),
              'stooptdynarename': element.stooptdynarename,
              'stooptinputtype': 1,
              'stooptvalue': dynlist
            });

          } else { //if not a number we verify if it is a range or an array of numbers

            if ((stooptval.indexOf(':') > -1) || (stooptval.indexOf(' ') > -1)) {

              if (stooptval.indexOf(':') > -1) { //it is a range
                var stooptsplit = stooptval.split(':');
                var stooptsplitlength = stooptsplit.length;


                if (stooptsplitlength == 2) { //the range should divide into two numbers
                  if (!(Number.isInteger(Number(stooptsplit[0])))) { //if it's a number it has to be a single integer
                    $('.nav-tabs a[href="#stooptions"]').tab('show');
                    popthis('Options error.', stooptdyn + ' range values should be integer numbers.');
                    return (0);
                  }
                  if (!(Number.isInteger(Number(stooptsplit[1])))) { //if it's a number it has to be a single integer
                    $('.nav-tabs a[href="#stooptions"]').tab('show');
                    popthis('Options error.', stooptdyn + ' range values should be integer numbers.');
                    return (0);
                  }
                  if (parseInt(stooptsplit[0], 10) > parseInt(stooptsplit[1], 10)) { //numbers should be ordered
                    $('.nav-tabs a[href="#stooptions"]').tab('show');
                    popthis('Options error.', stooptdyn + ' left range value can not be larger than right range value.');
                    return (0);
                  }

                  var dynlist = [];
                  for (i = parseInt(stooptsplit[0], 10); i <= parseInt(stooptsplit[1], 10); i++) {
                    dynlist.push({
                      'varname': parseInt(i, 10),
                    });
                  }

                  stooptionsdescription.push({
                    'stooptindex': parseInt(stooptcounter, 10),
                    'stooptdynarename': element.stooptdynarename,
                    'stooptinputtype': 10,
                    'stooptvalue': dynlist
                  });
                } else {
                  $('.nav-tabs a[href="#stooptions"]').tab('show');
                  popthis('Options error.', stooptdyn + ' value is not properly entered.');
                  return (0);
                }
              }

              if (stooptval.indexOf(' ') > -1) { //it is an array

                var stooptsplit = stooptval.split(' ');
                var stooptsplitlength = stooptsplit.length;
                var dynlist = [];
                for (i = 0; i < stooptsplitlength; i++) {
                  if (!(Number.isInteger(Number(stooptsplit[i])))) { //if it's a number it has to be a single integer
                    $('.nav-tabs a[href="#stooptions"]').tab('show');
                    popthis('Options error.', stooptdyn + ' array values should be integer numbers.');
                    return (0);
                  } else {
                    dynlist.push({
                      'varname': parseInt(stooptsplit[i], 10)
                    });
                  }
                }
                stooptionsdescription.push({
                  'stooptindex': parseInt(stooptcounter, 10),
                  'stooptdynarename': element.stooptdynarename,
                  'stooptinputtype': 11,
                  'stooptvalue': dynlist
                });
              }
            } else {
              $('.nav-tabs a[href="#stooptions"]').tab('show');
              popthis('Options error.', stooptdyn + ' value is not in the accepted value types. Please check the entry and the documentation.');
              return (0);
            }
          }
        }
      }



      if (element.stooptinputtype == 21) { //checking array of double

        stooptval = $('#stoopt_input_' + stooptcounter.toString()).val();
        stooptval = stooptval.trim();
        stooptdyn = (element.stooptdynarename).bold();

        if ((stooptval == null || stooptval == "")) { //entry is empty

          if (element.stooptid == 5) { //the bandpass filter option is a special case: when there is no input it becomes a switch
            stooptionsdescription.push({
              'stooptindex': parseInt(stooptcounter, 10),
              'stooptdynarename': element.stooptdynarename,
              'stooptinputtype': 0,
              'stooptvalue': ''
            });
          } else {
            $('.nav-tabs a[href="#stooptions"]').tab('show');
            popthis('Options error.', 'You have activated the ' + stooptdyn + ' option without specifying a value. Please deactivate this option if you are not using it or want to use its default value.');
            return (0);
          }



        } else {

          if ((IsNumeric(stooptval))) { //we verify if entry is a number

            var dynlist = [];
            dynlist.push({
              'varname': parseFloat(stooptval, 10)
            });

            stooptionsdescription.push({
              'stooptindex': parseInt(stooptcounter, 10),
              'stooptdynarename': element.stooptdynarename,
              'stooptinputtype': 2,
              'stooptvalue': dynlist
            });

          } else { //if not a number we verify if it is a range or an array of numbers

            if (stooptval.indexOf(' ') > -1) {
              var stooptsplit = stooptval.split(' ');
              var stooptsplitlength = stooptsplit.length;
              var dynlist = [];
              for (i = 0; i < stooptsplitlength; i++) {
                if (!(IsNumeric(stooptsplit[i]))) { //if it's a number it has to be a single double
                  $('.nav-tabs a[href="#stooptions"]').tab('show');
                  popthis('Options error.', stooptdyn + ' array values should be double numbers.');
                  return (0);
                } else {
                  dynlist.push({
                    'varname': parseFloat(stooptsplit[i], 10)
                  });
                }
              }
              stooptionsdescription.push({
                'stooptindex': parseInt(stooptcounter, 10),
                'stooptdynarename': element.stooptdynarename,
                'stooptinputtype': 21,
                'stooptvalue': dynlist
              });
            } else {
              $('.nav-tabs a[href="#stooptions"]').tab('show');
              popthis('Options error.', stooptdyn + ' value is not in the accepted value types (array of doubles). Please check the entry and the documentation.');
              return (0);
            }
          }
        }
      }


      if (element.stooptinputtype == 5) { //checking list type specific options

        var stooptindex = $('#stoopt_input_' + stooptcounter.toString()).val();
        stooptval = $("#stoopt_input_" + stooptcounter.toString() + " option:selected").text();
        stooptdyn = (element.stooptdynarename).bold();

        if (stooptindex == null) {
          $('.nav-tabs a[href="#stooptions"]').tab('show');
          popthis('Options error.', 'You have activated the ' + stooptdyn + ' option without selection a value. Please deactivate this option if you are not using it or want to use its default value.');
          return (0);
        } else {

          stooptionsdescription.push({
            'stooptindex': parseInt(stooptcounter, 10),
            'stooptdynarename': element.stooptdynarename,
            'stooptinputtype': 5,
            'stooptvalue': stooptval
          });
        }
      }


      if (element.stooptinputtype == 60) { //saving non checked types

        stooptval = $('#stoopt_input_' + stooptcounter.toString()).val();
        stooptval = stooptval.trim();
        stooptdyn = (element.stooptdynarename).bold();


        if ((stooptval == null || stooptval == "")) { //entry is empty
          $('.nav-tabs a[href="#stooptions"]').tab('show');
          popthis('Options error.', 'You have activated the ' + stooptdyn + ' option without specifying a value. Please deactivate this option if you are not using it or want to use its default value.');
          return (0);
        } else {


          if (stooptcounter == 0) { //checking specifically the "endogenous variables behind the stoch_simul command"
            sto0checker = 1;

            if (stooptval.indexOf(' ') > -1) { //an array of values has been entered, we check if they are endogenous vars
              var stooptsplit = stooptval.split(' ');
              var stooptsplitlength = stooptsplit.length;
              var dynlist = [];
              for (i = 0; i < stooptsplitlength; i++) {
                var endocheckflag = 0;
                for (x in dynaremodel.endogenous) {
                  if (dynaremodel.endogenous[x]["longName"] == stooptsplit[i]) {
                    endocheckflag = 1;
                  }
                }
                if (endocheckflag == 0) {
                  $('.nav-tabs a[href="#stooptions"]').tab('show');
                  popthis('Options error.', stooptdyn + ' values are not among the endogenous variables of the model.');
                  return (0);
                } else {
                  dynlist.push({
                    'varname': stooptsplit[i]
                  });
                }
              }

              stooptionsdescription.push({
                'stooptindex': parseInt(stooptcounter, 10),
                'stooptdynarename': element.stooptdynarename,
                'stooptinputtype': 11,
                'stooptvalue': dynlist
              });

            } else {
              //we check for a single endogenous entry
              var endocheckflag = 0;
              for (x in dynaremodel.endogenous) {
                if (dynaremodel.endogenous[x]["longName"] == stooptval) {
                  endocheckflag = 1;
                }
              }

              if (endocheckflag == 0) {
                $('.nav-tabs a[href="#stooptions"]').tab('show');
                popthis('Options error.', stooptdyn + ' value(s) are not among the endogenous variables of the model.');
                return (0);
              } else {
                var dynlist = [];
                dynlist.push({
                  'varname': stooptval
                });
                stooptionsdescription.push({
                  'stooptindex': parseInt(stooptcounter, 10),
                  'stooptdynarename': element.stooptdynarename,
                  'stooptinputtype': 60,
                  'stooptvalue': dynlist
                });
              }

              // }
            }

          }

          if (stooptcounter == 8) { //irf_shocks option
            if (stooptval.indexOf(' ') > -1) { //it is an array
              var stooptsplit = stooptval.split(' ');
              var stooptsplitlength = stooptsplit.length;
              var dynlist = [];
              for (i = 0; i < stooptsplitlength; i++) {
                var exocheckflag = 0;
                for (x in dynaremodel.exogenous) {
                  if (dynaremodel.exogenous[x]["longName"] == stooptsplit[i]) {
                    exocheckflag = 1;
                  }
                }
                if (exocheckflag == 0) {
                  $('.nav-tabs a[href="#estimoptions"]').tab('show');
                  popthis('Options error.', stooptdyn + ' values are not among the exogenous variables of the model.');
                  return (0);
                } else {
                  dynlist.push({
                    'varname': stooptsplit[i]
                  });
                }
              }
              stooptionsdescription.push({
                'stooptindex': parseInt(stooptcounter, 10),
                'stooptdynarename': element.stooptdynarename,
                'stooptinputtype': 60,
                'stooptvalue': dynlist
              });
            } else { //is not an array--> could be a single var

              var exocheckflag = 0;
              for (x in dynaremodel.exogenous) {
                if (dynaremodel.exogenous[x]["longName"] == stooptval) {
                  exocheckflag = 1;
                }
              }
              if (exocheckflag == 0) {
                $('.nav-tabs a[href="#estimoptions"]').tab('show');
                popthis('Options error.', stooptdyn + ' value is not among the exogenous variables of the model.');
                return (0);
              } else {
                var dynlist = [];
                dynlist.push({
                  'varname': stooptval
                });
                stooptionsdescription.push({
                  'stooptindex': parseInt(stooptcounter, 10),
                  'stooptdynarename': element.stooptdynarename,
                  'stooptinputtype': 60,
                  'stooptvalue': dynlist
                });
              }


            }
          }


        }
      }



    }
    stooptcounter++;
  }

  if (sto0checker == 0) { //we add the list of all endo vars to "endogenous variables" option if the user did not activate it

    var dynlist = [];
    for (x in dynaremodel.endogenous) {
      dynlist.push({
        'varname': dynaremodel.endogenous[x]["longName"]
      });
    }
    stooptionsdescription.push({
      'stooptindex': 0,
      'stooptdynarename': 'endogenous_variables',
      'stooptinputtype': 60,
      'stooptvalue': dynlist
    });
  }

  if (globalvaluescount == 0) {
    popthis('Simulation input error.', 'You have submitted no input to perform the simulation. Please input value(s) and retry.');
    return (0);
  } else {
    { //we post to the server

      var stosimval = {
        'modelhash': $('#codehash').val(),
        'ssstatus': parseInt(ssstatus, 10),
        'endonum': parseInt(endonum, 10),
        'exonum': parseInt(exonum, 10),
        'ssdescription': ssdescription,
        'stochasticshocksdescription': stochasticshocksdescription,
        'optionsdescription': stooptionsdescription,
        //'accesstype': 'runstochastic'
      };

      // var stosimvaldata = JSON.stringify(stosimval);

      $("#outputtable").empty();
      $("#plotbtn").empty();
      $("#plotbox").empty();
      $('#stosimModal').modal('hide');
      var div = document.createElement('div');
      div.className = 'loader';
      div.innerHTML = '';
      document.getElementById('inner').appendChild(div);
      // $('.nav-tabs a[href="#output"]').tab('show');


      // calling the 'node side'
      try {
        var resp = await runstochasticnode(JSON.stringify(stosimval));
      } catch (error) {
        console.log(error)
        if (error.status == 200) {
          return popthis('Error', error.message);
        } else if (error.status == 100) {
          showconsolealert(error.message, 1);
          return false;
        }
      }

      return false;
    }
  }

});

ipc.on('stochasticsimfinish', (event, arg) => {

  async function finishstochastic() {

    scrolltextarea();

    $('.nav-tabs a[href="#output"]').tab('show');

    // loading JSON output from Matlab
    try {
      stomatlabdata = await loadjsonfile('stochsimout.JSON');
    } catch (error) {
      return popthis('Error', 'Unable to open stochsimout.JSON to process:' + error);
    }

    var plotdropdown = `
      <section>
      <h2>Plots</h2>
      <div class="btn-group" role="group" aria-label="...">
      <div class="btn-group" role="group">
        <button id="plotbtn" class="btn btn-primary btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Select graph <span class="caret"></span><span class="sr-only">Toggle Dropdown</span>
        </button>
        <ul class="dropdown-menu" aria-labelledby="plotbtn">`;


    if ("irfs" in stomatlabdata) {
      plotdropdown += '<li class="dropdown-header">Endogenous variables:</li>';
      y = 0;
      for (x in stomatlabdata.irfs) {
        irfname = x.trim();
        if (y == 0) {
          var firstname = irfname;
        }
        plotdropdown += '<li onclick="stoplotirfs(&quot;' + irfname + '&quot;)"><a href="#stophere">' + irfname + '</a></li>';
        y = y + 1;
      }
    }

    if ("SpectralDensity" in stomatlabdata) {
      plotdropdown += '<li class="dropdown-header">Spectral densities:</li>';
      y = 0;
      for (x in stomatlabdata.var_list) {
        specvarname = stomatlabdata.var_list[y].trim();
        plotdropdown += '<li onclick="stoplotspectral(' + y + ')"><a href="#stophere">' + specvarname + '</a></li>';
        y = y + 1;
      }
    }
    plotdropdown += '</ul></div>';
    plotdropdown += `
      <div class="btn-group" role="group">
      <button id="exportbtn" class="btn btn-primary btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Export data <span class="caret"></span><span class="sr-only">Toggle Dropdown</span>
      </button>
      <ul class="dropdown-menu" aria-labelledby="exportbtn">
      <li class="dropdown-header">Select file type:</li>
      <li onclick="exportfile(&quot;json&quot;)"><a href="#stophere">Export to JSON</a></li>
      <li onclick="exportfile(&quot;csv&quot;)"><a href="#stophere">Export to CSV</a></li>
      </ul>
      </div>`;

    plotdropdown += `
        </div>
        </section>
        `;
    //sstbl1 += "</tbody></table></section>";
    $("#inner").empty();
    //document.getElementById("outputtable").innerHTML = sstbl1;
    document.getElementById("plotbtn").innerHTML = plotdropdown;

    if ("irfs" in stomatlabdata) {
      stoplotirfs(firstname);
    }

  }

  finishstochastic();


});






// graph plotter
function stoplotirfs(plotvar) {

  var trace1 = {
    //x: stomatlabdata.irfs.plotx,
    y: stomatlabdata.irfs[plotvar]
  };
  var data = [trace1];

  var layout = {
    title: plotvar,
    xaxis: {
      title: 'Periods'
    },
    yaxis: {
      title: plotvar
    }
  };

  Plotly.newPlot('plotbox', data, layout);
}

function stoplotspectral(plotvar) {
  freqvar = [];
  yvar = [];
  for (i = 0; i < stomatlabdata.SpectralDensity.density[plotvar].length; i++) {
    freqvar.push(stomatlabdata.SpectralDensity.freqs[i][0]);
    yvar.push(stomatlabdata.SpectralDensity.density[plotvar][i]);
  }
  var trace0 = {
    x: freqvar,
    y: yvar,
    name: 'Spectral Density',
    mode: 'lines',
    line: {
      color: 'rgb(0, 0, 0)',
      dash: 'solid',
      width: 2
    }
  };
  var data = [trace0];
  var layout = {
    title: stomatlabdata.var_list[plotvar].trim(),
    xaxis: {
      title: '0 ≤ ω ≤ π'
    },
    yaxis: {
      title: 'f(ω)'
    }
  };
  Plotly.newPlot('plotbox', data, layout);
}

// files exporter
function stoexportfile(exporttype) {

  var filename = $('#codename').val();

  if (exporttype == 'json') {
    var exportjson = {
      'simulation': [],
      'steadystate1': matlabdata.steady_state1,
      'steadystate2': matlabdata.steady_state2
    };
    for (var key in matlabdata.endo_simul) {
      if (key != "plotx") {
        exportjson.simulation.push({
          [key]: matlabdata.endo_simul[key]
        });
      }
    }
    var exportjsontext = JSON.stringify(exportjson);
    download(filename + ".JSON", exportjsontext);
  }

  if (exporttype == 'csv') {
    var exportcsv = matlabdata.endo_names + "\r\n";
    var csvarray = [];
    for (var mykey in matlabdata.endo_names) {
      csvarray.push(matlabdata.endo_simul[matlabdata.endo_names[mykey]]);
      //cl(mykey);
    }
    var transpose = csvarray[0].map((col, i) => csvarray.map(row => row[i]));
    for (itr = 0; itr < transpose.length; itr++) {
      exportcsv += transpose[itr] + "\r\n";
    }
    download(filename + ".csv", exportcsv);
  }
}

function onoffstooption(thisnum) {
  if ($('#stoopt_input_' + thisnum.toString()).is(':disabled')) {
    //textbox is disabled
    $('#stoopt_input_' + thisnum.toString()).removeAttr('disabled');
    $('#stoopt_input_' + thisnum.toString()).val('');
  } else {
    $('#stoopt_input_' + thisnum.toString()).attr({
      'disabled': 'disabled'
    });
    $('#stoopt_input_' + thisnum.toString()).val('');
  }
}

function onoffstooptionselect(thisnum) {
  if ($('#stoopt_input_' + thisnum.toString()).is(':disabled')) {
    //textbox is disabled
    $("#stoopt_input_" + thisnum.toString()).removeAttr('disabled');
    // $("#stoopt_input_" + thisnum.toString()).attr('disabled',false);
    // document.getElementById("stoopt_input_24").disabled = false;
  } else {
    $("#stoopt_input_" + thisnum.toString()).attr('disabled', 'disabled');
  }
}

function createstooptions() {
  var stoopttableoptions = '';
  stooptionslist.forEach(function (element) {
    if (element.stooptinputtype == 0) { //options that do not require the entry of a value
      stoopttableoptions += `<tr>
                            <th scope="row"><input type="checkbox" id="stoopt_id_` + element.stooptid + `" onchange="onoffstooption(` + element.stooptid + `)" data-toggle="toggle" data-onstyle="success" data-size="mini"></th>
                            <td>` + element.stooptdynarename + `</td>
                            <td></td>
                            <td><div class="tooltipcss"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span><span class="tooltiptextcss">` + element.stooptinfo + `</span></div></td>
                          </tr>`;
    }
    if ((element.stooptinputtype != 0) && (element.stooptinputtype != 5)) { //options that do require the entry of a value
      if (element.stooptid == 0) {
        stoopttableoptions += `<tr>
                          <th scope="row"><input type="checkbox" id="stoopt_id_` + element.stooptid + `" onchange="onoffstooption(` + element.stooptid + `)" data-toggle="toggle" data-onstyle="success" data-size="mini"></th>
                          <td style="color:#229954;">` + element.stooptdynarename + `</td>
                          <td><input class="form-control input-sm" name="stoopt_input_` + element.stooptid + `" id="stoopt_input_` + element.stooptid + `" placeholder="` + element.stooptplaceholder + `" type="text" value="" disabled="disabled"></td>
                          <td><div class="tooltipcss"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span><span class="tooltiptextcss">` + element.stooptinfo + `</span></div></td>
                        </tr>`;
      } else {
        stoopttableoptions += `<tr>
                          <th scope="row"><input type="checkbox" id="stoopt_id_` + element.stooptid + `" onchange="onoffstooption(` + element.stooptid + `)" data-toggle="toggle" data-onstyle="success" data-size="mini"></th>
                          <td>` + element.stooptdynarename + `</td>
                          <td><input class="form-control input-sm" name="stoopt_input_` + element.stooptid + `" id="stoopt_input_` + element.stooptid + `" placeholder="` + element.stooptplaceholder + `" type="text" value="" disabled="disabled"></td>
                          <td><div class="tooltipcss"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span><span class="tooltiptextcss">` + element.stooptinfo + `</span></div></td>
                        </tr>`;
      }
    }
    if (element.stooptinputtype == 5) { //options that do require specific interface
      switch (element.stooptid) {
        case 28: //sylvester
          stoopttableoptions += `<tr>
                          <th scope="row"><input type="checkbox" id="stoopt_id_` + element.stooptid + `" onchange="onoffstooptionselect(` + element.stooptid + `)" data-toggle="toggle" data-onstyle="success" data-size="mini"></th>
                          <td>` + element.stooptdynarename + `</td>
                          <td><select class="form-control input-sm" name="stoopt_input_` + element.stooptid + `" id="stoopt_input_` + element.stooptid + `" disabled>
                          <option value="-1" selected="true" disabled="disabled">Select</option>
                          <option value="1">default</option>
                          <option value="2">fixed_point</option>
                          </select></td>
                          <td><div class="tooltipcss"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span><span class="tooltiptextcss">` + element.stooptinfo + `</span></div></td>
                        </tr>`;
          break;
        case 30: //dr
          stoopttableoptions += `<tr>
                        <th scope="row"><input type="checkbox" id="stoopt_id_` + element.stooptid + `" onchange="onoffstooptionselect(` + element.stooptid + `)" data-toggle="toggle" data-onstyle="success" data-size="mini"></th>
                        <td>` + element.stooptdynarename + `</td>
                        <td><select class="form-control input-sm" name="stoopt_input_` + element.stooptid + `" id="stoopt_input_` + element.stooptid + `" disabled>
                        <option value="-1" selected="true" disabled="disabled">Select</option>
                        <option value="1">default</option>
                        <option value="2">cycle_reduction</option>
                        <option value="3">logarithmic_reduction</option>
                        </select></td>
                        <td><div class="tooltipcss"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span><span class="tooltiptextcss">` + element.stooptinfo + `</span></div></td>
                      </tr>`;
          break;
      }
    }
  });
  $('#stooptiontable tbody').append(stoopttableoptions);
}
var stooptionslist = [{
    stooptid: 0,
    stooptdynarename: 'endogenous_variables',
    stooptname: '',
    stooptplaceholder: 'All',
    stooptinputtype: 60,
    stooptinfo: 'Use this option to list the endogenous variables appearing after the estimation command. Available options are:  <br> varobs: only the observed endogenous variables will be considered, <br> A list of user selected endogenous variables separated by a space. Example: var1 var2 var3 <br> Defaut: By default all endogenous will be added. Please leave this option unchecked to activate the default behavior.'
  },
  {
    stooptid: 1,
    stooptdynarename: 'ar',
    stooptname: '',
    stooptplaceholder: '5',
    stooptinputtype: 1,
    stooptinfo: 'Order of autocorrelation coefficients to compute and to print.<br> Defaut: 5.'
  },
  {
    stooptid: 2,
    stooptdynarename: 'drop',
    stooptname: '',
    stooptplaceholder: '100',
    stooptinputtype: 1,
    stooptinfo: 'Number of points (burnin) dropped at the beginning of simulation before computing the summary statistics. Note that this option does not affect the simulated series stored in oo_.endo_simul and the workspace. Here, no periods are dropped.<br> Defaut: 100.'
  },
  {
    stooptid: 3,
    stooptdynarename: 'hp_filter',
    stooptname: '',
    stooptplaceholder: '',
    stooptinputtype: 2,
    stooptinfo: 'Uses HP filter with λ= DOUBLE before computing moments. If theoretical moments are requested, the spectrum of the model solution is filtered following the approach outlined in Uhlig (2001). <br> Defaut: no filter.'
  },
  {
    stooptid: 4,
    stooptdynarename: 'one_sided_hp_filter',
    stooptname: '',
    stooptplaceholder: '',
    stooptinputtype: 2,
    stooptinfo: 'Uses the one-sided HP filter with λ= DOUBLE described in Stock and Watson (1999) before computing moments. This option is only available with simulated moments.<br> Defaut: no filter.'
  },
  {
    stooptid: 5,
    stooptdynarename: 'bandpass_filter',
    stooptname: '',
    stooptplaceholder: '',
    stooptinputtype: 21,
    stooptinfo: 'Uses a bandpass filter with the default passband before computing moments. If theoretical moments are requested, the spectrum of the model solution is filtered using an ideal bandpass filter. If empirical moments are requested, the Baxter and King (1999) filter is used.<br> Defaut: no filter.'
  },
  {
    stooptid: 6,
    stooptdynarename: 'filtered_theoretical_moments_grid',
    stooptname: '',
    stooptplaceholder: '512',
    stooptinputtype: 1,
    stooptinfo: 'When computing filtered theoretical moments (with either option hp_filter or option bandpass_filter), this option governs the number of points in the grid for the discrete Inverse Fast Fourier Transform. It may be necessary to increase it for highly autocorrelated processes.<br> Defaut: 512.'
  },
  {
    stooptid: 7,
    stooptdynarename: 'irf',
    stooptname: '',
    stooptplaceholder: '40',
    stooptinputtype: 1,
    stooptinfo: 'Number of periods on which to compute the IRFs. Setting irf=0 suppresses the plotting of IRFs.<br> Defaut: 40'
  },
  {
    stooptid: 8,
    stooptdynarename: 'irf_shocks',
    stooptname: '',
    stooptplaceholder: 'All',
    stooptinputtype: 60,
    stooptinfo: 'The exogenous variables for which to compute IRFs. <br> A list of exogenous variables separated by a space is expected as in, for instance: exovar1 exovar2 exovar3<br> Defaut: All.'
  },
  {
    stooptid: 9,
    stooptdynarename: 'relative_irf',
    stooptname: '',
    stooptplaceholder: '',
    stooptinputtype: 0,
    stooptinfo: 'Requests the computation of normalized IRFs. At first order, the normal shock vector of size one standard deviation is divided by the standard deviation of the current shock and multiplied by 100. The impulse responses are hence the responses to a unit shock of size 1 (as opposed to the regular shock size of one standard deviation), multiplied by 100. Thus, for a loglinearized model where the variables are measured in percent, the IRFs have the interpretation of the percent responses to a 100 percent shock. For example, a response of 400 of output to a TFP shock shows that output increases by 400 percent after a 100 percent TFP shock (you will see that TFP increases by 100 on impact). Given linearity at order=1, it is straightforward to rescale the IRFs stored in oo_.irfs to any desired size. At higher order, the interpretation is different. The relative_irf option then triggers the generation of IRFs as the response to a 0.01 unit shock (corresponding to 1 percent for shocks measured in percent) and no multiplication with 100 is performed. That is, the normal shock vector of size one standard deviation is divided by the standard deviation of the current shock and divided by 100. For example, a response of 0.04 of log output (thus measured in percent of the steady state output level) to a TFP shock also measured in percent then shows that output increases by 4 percent after a 1 percent TFP shock (you will see that TFP increases by 0.01 on impact).'
  },
  {
    stooptid: 10,
    stooptdynarename: 'irf_plot_threshold',
    stooptname: '',
    stooptplaceholder: '1.0e-10',
    stooptinputtype: 4,
    stooptinfo: 'Threshold size for plotting IRFs. All IRFs for a particular variable with a maximum absolute deviation from the steady state smaller than this value are not displayed.<br> Defaut: 1.0e-10'
  },
  {
    stooptid: 11,
    stooptdynarename: 'nocorr',
    stooptname: '',
    stooptplaceholder: '',
    stooptinputtype: 0,
    stooptinfo: 'Don’t print the correlation matrix<br> Defaut: Printed.'
  },
  {
    stooptid: 12,
    stooptdynarename: 'nodecomposition',
    stooptname: '',
    stooptplaceholder: '',
    stooptinputtype: 0,
    stooptinfo: 'Don’t compute (and don’t print) unconditional variance decomposition.'
  },
  {
    stooptid: 13,
    stooptdynarename: 'nofunctions',
    stooptname: '',
    stooptplaceholder: '',
    stooptinputtype: 0,
    stooptinfo: 'Don’t print the coefficients of the approximated solution (printing them is the default).<br> Defaut: Printed.'
  },
  {
    stooptid: 14,
    stooptdynarename: 'nomoments',
    stooptname: '',
    stooptplaceholder: '',
    stooptinputtype: 0,
    stooptinfo: 'Don’t print moments of the endogenous variablesDon’t print moments of the endogenous variablesDon’t print moments of the endogenous variables<br> Defaut: Printed.'
  },
  {
    stooptid: 15,
    stooptdynarename: 'nograph',
    stooptname: '',
    stooptplaceholder: '',
    stooptinputtype: 0,
    stooptinfo: 'Do not create graphs (which implies that they are not saved to the disk nor displayed). If this option is not used, graphs will be saved to disk and displayed to screen.'
  },
  {
    stooptid: 16,
    stooptdynarename: 'order',
    stooptname: '',
    stooptplaceholder: '2',
    stooptinputtype: 1,
    stooptinfo: 'Order of Taylor approximation. Note that for third order and above, the k_order_solver option is implied and only empirical moments are available (you must provide a value for periods option).<br> Defaut: 2.'
  },
  {
    stooptid: 17,
    stooptdynarename: 'k_order_solver',
    stooptname: '',
    stooptplaceholder: '',
    stooptinputtype: 0,
    stooptinfo: 'Use a k-order solver (implemented in C++) instead of the default Dynare solver. This option is not yet compatible with the bytecode option (see Model declaration).<br> Defaut: disabled for order 1 and 2, enabled for order 3 and above.'
  },
  {
    stooptid: 18,
    stooptdynarename: 'periods',
    stooptname: '',
    stooptplaceholder: '0',
    stooptinputtype: 1,
    stooptinfo: 'If different from zero, empirical moments will be computed instead of theoretical moments. The value of the option specifies the number of periods to use in the simulations. Values of the initval block, possibly recomputed by steady, will be used as starting point for the simulation. The simulated endogenous variables are made available to the user in a vector for each variable and in the global matrix oo_.endo_simul (see oo_.endo_simul). The simulated exogenous variables are made available in oo_.exo_simul (see oo_.exo_simul).<br> Defaut: 0.'
  },
  {
    stooptid: 19,
    stooptdynarename: 'qz_criterium',
    stooptname: '',
    stooptplaceholder: '1.000001',
    stooptinputtype: 2,
    stooptinfo: 'Value used to split stable from unstable eigenvalues in reordering the Generalized Schur decomposition used for solving first order problems.<br> Defaut: 1.000001.'
  },
  {
    stooptid: 20,
    stooptdynarename: 'qz_zero_threshold',
    stooptname: '',
    stooptplaceholder: '1.0e-6',
    stooptinputtype: 2,
    stooptinfo: 'Value used to test if a generalized eigenvalue is 0/0 in the generalized Schur decomposition (in which case the model does not admit a unique solution).<br> Defaut: 1.0e-6'
  },
  {
    stooptid: 21,
    stooptdynarename: 'replic',
    stooptname: '',
    stooptplaceholder: '',
    stooptinputtype: 1,
    stooptinfo: 'Number of simulated series used to compute the IRFs.<br> Defaut: 1 if order=1, and 50 otherwise.'
  },
  {
    stooptid: 22,
    stooptdynarename: 'simul_replic',
    stooptname: '',
    stooptplaceholder: '1',
    stooptinputtype: 1,
    stooptinfo: 'Number of series to simulate when empirical moments are requested (i.e. periods > 0). Note that if this option is greater than 1, the additional series will not be used for computing the empirical moments but will simply be saved in binary form to the file FILENAME_simul.<br> Defaut: 1.'
  },
  {
    stooptid: 23,
    stooptdynarename: 'solve_algo',
    stooptname: '',
    stooptplaceholder: '4',
    stooptinputtype: 1,
    stooptinfo: 'Determines the non-linear solver to use.Possible values for OPTION are: <br> 0: Use fsolve (under MATLAB, only available if you have the Optimization Toolbox; always available under Octave). <br>1: Use Dynare’s own nonlinear equation solver (a Newton-like algorithm with line-search). <br>2: Splits the model into recursive blocks and solves each block in turn using the same solver as value 1. <br>3: Use Chris Sims’ solver. <br>4: Splits the model into recursive blocks and solves each block in turn using a trust-region solver with autoscaling. <br>5: Newton algorithm with a sparse Gaussian elimination (SPE) (requires bytecode option, see Model declaration). <br>6: Newton algorithm with a sparse LU solver at each iteration (requires bytecode and/or block option, see Model declaration). <br>7: Newton algorithm with a Generalized Minimal Residual (GMRES) solver at each iteration (requires bytecode and/or block option, see Model declaration). <br>8: Newton algorithm with a Stabilized Bi-Conjugate Gradient (BICGSTAB) solver at each iteration (requires bytecode and/or block option, see Model declaration). <br>9: Trust-region algorithm on the entire model. <br>10: Levenberg-Marquardt mixed complementarity problem (LMMCP) solver (Kanzow and Petra (2004)). <br>11: PATH mixed complementarity problem solver of Ferris and Munson (1999). The complementarity conditions are specified with an mcp equation tag, see lmmcp. Dynare only provides the interface for using the solver. Due to licence restrictions, you have to download the solver’s most current version yourself from http://pages.cs.wisc.edu/~ferris/path.html and place it in MATLAB’s search path.<br> Defaut: 4: Splits the model into recursive blocks and solves each block in turn using a trust-region solver with autoscaling.'
  },
  {
    stooptid: 24,
    stooptdynarename: 'aim_solver',
    stooptname: '',
    stooptplaceholder: '',
    stooptinputtype: 0,
    stooptinfo: 'Use the Anderson-Moore Algorithm (AIM) to compute the decision rules, instead of using Dynare’s default method based on a generalized Schur decomposition. This option is only valid for first order approximation. See AIM website for more details on the algorithm.<br>Note: Option aim_solver is incompatible with order >= 2.'
  },
  {
    stooptid: 25,
    stooptdynarename: 'conditional_variance_decomposition',
    stooptname: '',
    stooptplaceholder: '',
    stooptinputtype: 10,
    stooptinfo: 'Computes a conditional variance decomposition for the specified period(s). The periods must be strictly positive. Conditional variances are given by var(yt+k|t).<br>For period 1, the conditional variance decomposition provides the decomposition of the effects of shocks upon impact. <br>The results are stored in oo_.conditional_variance_decomposition (see oo_.conditional_variance_decomposition). In the presence of measurement error, the oo_.conditional_variance_decomposition field will contain the variance contribution after measurement error has been taken out, i.e. the decomposition will be conducted of the actual as opposed to the measured variables. The variance decomposition of the measured variables will be stored in oo_.conditional_variance_decomposition_ME (see oo_.conditional_variance_decomposition_ME). The variance decomposition is only conducted, if theoretical moments are requested, i.e. using the periods=0-option. In case of order=2, Dynare provides a second-order accurate approximation to the true second moments based on the linear terms of the second-order solution (see Kim, Kim, Schaumburg and Sims (2008)). Note that the unconditional variance decomposition i.e. at horizon infinity) is automatically conducted if theoretical moments are requested and if nodecomposition is not set (see oo_.variance_decomposition). <br> A range of numbers can be entered by using : as in for instance "50:100". <br> An array of number can be entered by using a space as in for instance "8 9 10 11 15".'
  },
  {
    stooptid: 26,
    stooptdynarename: 'pruning',
    stooptname: '',
    stooptplaceholder: '',
    stooptinputtype: 0,
    stooptinfo: 'Discard higher order terms when iteratively computing simulations of the solution. At second order, Dynare uses the algorithm of Kim, Kim, Schaumburg and Sims (2008), while at third order its generalization by Andreasen, Fernández-Villaverde and Rubio-Ramírez (2018) is used. Not available above third order.'
  },
  {
    stooptid: 27,
    stooptdynarename: 'partial_information',
    stooptname: '',
    stooptplaceholder: '',
    stooptinputtype: 0,
    stooptinfo: 'Computes the solution of the model under partial information, along the lines of Pearlman, Currie and Levine (1986). Agents are supposed to observe only some variables of the economy. The set of observed variables is declared using the varobs command. Note that if varobs is not present or contains all endogenous variables, then this is the full information case and this option has no effect. More references can be found: https://archives.dynare.org/DynareWiki/PartialInformation'
  },
  {
    stooptid: 28,
    stooptdynarename: 'sylvester',
    stooptname: '',
    stooptplaceholder: 'default',
    stooptinputtype: 5,
    stooptinfo: 'Determines the algorithm used to solve the Sylvester equation for block decomposed model. Possible values for OPTION are: <br>default: Uses the default solver for Sylvester equations (gensylv) based on Ondra Kamenik’s algorithm (see here for more information). <br>fixed_point: Uses a fixed point algorithm to solve the Sylvester equation (gensylv_fp). This method is faster than the default one for large scale models.<br> Defaut: default'
  },
  {
    stooptid: 29,
    stooptdynarename: 'sylvester_fixed_point_tol',
    stooptname: '',
    stooptplaceholder: '1.0e-12',
    stooptinputtype: 2,
    stooptinfo: 'The convergence criterion used in the fixed point Sylvester solver.<br> Defaut: 1.0e-12.'
  },
  {
    stooptid: 30,
    stooptdynarename: 'dr',
    stooptname: '',
    stooptplaceholder: 'default',
    stooptinputtype: 5,
    stooptinfo: 'Determines the method used to compute the decision rule. Possible values for OPTION are: <br>default: Uses the default method to compute the decision rule based on the generalized Schur decomposition (see Villemot (2011) for more information). <br>cycle_reduction: Uses the cycle reduction algorithm to solve the polynomial equation for retrieving the coefficients associated to the endogenous variables in the decision rule. This method is faster than the default one for large scale models. <br>logarithmic_reduction: Uses the logarithmic reduction algorithm to solve the polynomial equation for retrieving the coefficients associated to the endogenous variables in the decision rule. This method is in general slower than the cycle_reduction.<br> Defaut: default.'
  },
  {
    stooptid: 31,
    stooptdynarename: 'dr_cycle_reduction_tol',
    stooptname: '',
    stooptplaceholder: '1.0e-7',
    stooptinputtype: 4,
    stooptinfo: 'The convergence criterion used in the cycle reduction algorithm.<br> Defaut: 1.0e-7.'
  },
  {
    stooptid: 32,
    stooptdynarename: 'dr_logarithmic_reduction_tol',
    stooptname: '',
    stooptplaceholder: '1.0e-12',
    stooptinputtype: 4,
    stooptinfo: 'The convergence criterion used in the logarithmic reduction algorithm.<br> Defaut: 1.0e-12.'
  },
  {
    stooptid: 33,
    stooptdynarename: 'dr_logarithmic_reduction_maxiter',
    stooptname: '',
    stooptplaceholder: '100',
    stooptinputtype: 1,
    stooptinfo: 'The maximum number of iterations used in the logarithmic reduction algorithm.<br> Defaut: 100.'
  },
  {
    stooptid: 34,
    stooptdynarename: 'loglinear',
    stooptname: '',
    stooptplaceholder: '',
    stooptinputtype: 0,
    stooptinfo: 'Computes a log-linear approximation of the model instead of a linear approximation. As always in the context of estimation, the data must correspond to the definition of the variables used in the model (see Pfeifer (2013) for more details on how to correctly specify observation equations linking model variables and the data). If you specify the loglinear option, Dynare will take the logarithm of both your model variables and of your data as it assumes the data to correspond to the original non-logged model variables. The displayed posterior results like impulse responses, smoothed variables, and moments will be for the logged variables, not the original un-logged ones. Note that ALL variables are log-transformed by using the Jacobian transformation, not only selected ones. Thus, you have to make sure that your variables have strictly positive steady states. stoch_simul will display the moments, decision rules, and impulse responses for the log-linearized variables. The decision rules saved in oo_.dr and the simulated variables will also be the ones for the log-linear variables.<br> Defaut: computes a linear approximation.'
  },
  {
    stooptid: 35,
    stooptdynarename: 'dr_display_tol',
    stooptname: '',
    stooptplaceholder: '1.0e-6',
    stooptinputtype: 2,
    stooptinfo: 'Tolerance for the suppression of small terms in the display of decision rules. Rows where all terms are smaller than dr_display_tol are not displayed. <br> Defaut: 1.0e-6.'
  },
  {
    stooptid: 36,
    stooptdynarename: 'contemporaneous_correlation',
    stooptname: '',
    stooptplaceholder: '',
    stooptinputtype: 0,
    stooptinfo: 'Saves the contemporaneous correlation between the endogenous variables in oo_.contemporaneous_correlation. Requires the nocorr option not to be set.'
  },
  {
    stooptid: 37,
    stooptdynarename: 'spectral_density',
    stooptname: '',
    stooptplaceholder: '',
    stooptinputtype: 0,
    stooptinfo: 'Triggers the computation and display of the theoretical spectral density of the (filtered) model variables. Results are stored in ´´oo_.SpectralDensity´´, defined below. Default: do not request spectral density estimates.'
  }
];
createstooptions();