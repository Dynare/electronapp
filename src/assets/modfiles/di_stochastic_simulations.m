function di_stochastic_simulations()

    % global M_ options_ oo_ ys0_ ex0_
    global M_ options_ oo_

    %remove this instruction when proper path is found
    addpath jsonlab-1.5;

    disp('di_stochastic_simulations performing Matlab tasks');

    %loading JSON
    jm = loadjson('stochsimin.JSON', 'SimplifyCell', 0);
    runflag = 1;
    data2json = struct();

    M_.exo_det_length = 0;

    for nshocks = 1:length(jm.stochasticshocksdescription)
        covartype = jm.stochasticshocksdescription{nshocks}.shockattributevalue;
        thisshock = (jm.stochasticshocksdescription{nshocks}.shockindex) + 1;
        assoshock = (jm.stochasticshocksdescription{nshocks}.assoshockindex) + 1;

        switch covartype
            case 1
                M_.Sigma_e(thisshock, thisshock) = (jm.stochasticshocksdescription{nshocks}.shockvalue)^2;
            case 2
                M_.Sigma_e(thisshock, thisshock) = jm.stochasticshocksdescription{nshocks}.shockvalue;
            case 3
                M_.Sigma_e(thisshock, assoshock) = jm.stochasticshocksdescription{nshocks}.shockvalue;
                M_.Sigma_e(assoshock, thisshock) = M_.Sigma_e(thisshock, assoshock);
                M_.sigma_e_is_diagonal = 0;
            case 4
                M_.Sigma_e(thisshock, assoshock) = 2 * sqrt(M_.Sigma_e(thisshock, thisshock) * M_.Sigma_e(assoshock, assoshock));
                M_.Sigma_e(assoshock, thisshock) = M_.Sigma_e(thisshock, assoshock);
                M_.Correlation_matrix(thisshock, assoshock) = jm.stochasticshocksdescription{nshocks}.shockvalue;
                M_.Correlation_matrix(assoshock, thisshock) = M_.Correlation_matrix(thisshock, assoshock);
                M_.sigma_e_is_diagonal = 0;
        end

    end

    %---All options

    for nopts = 1:length(jm.optionsdescription)
        optindex = jm.optionsdescription{nopts}.stooptindex;

        switch optindex
            case 0
                var_list_ = {};
                optval = jm.optionsdescription{nopts}.stooptvalue;

                for nvars = 1:length(optval)
                    var_list_{end + 1} = char(optval{nvars}.varname);
                end

            case 1
                options_.ar = jm.optionsdescription{nopts}.stooptvalue;
            case 2
                options_.drop = jm.optionsdescription{nopts}.stooptvalue;
            case 3
                options_.hp_filter = jm.optionsdescription{nopts}.stooptvalue;
            case 4
                options_.one_sided_hp_filter = jm.optionsdescription{nopts}.stooptvalue;
            case 5
                options_.bandpass.indicator = true;

                if jm.optionsdescription{nopts}.stooptinputtype == 21
                    options_.bandpass.passband = [];
                    optval = jm.optionsdescription{nopts}.stooptvalue;

                    for nvars = 1:length(optval)
                        options_.bandpass.passband(end + 1) = str2num(optval{nvars}.varname);
                    end

                    options_.bandpass.passband = options_.bandpass.passband';
                end

            case 6
                options_.filtered_theoretical_moments_grid = jm.optionsdescription{nopts}.stooptvalue;
            case 7
                options_.irf = jm.optionsdescription{nopts}.stooptvalue;
            case 8
                options_.irf_shocks = {};
                optval = jm.optionsdescription{nopts}.stooptvalue;

                for nvars = 1:length(optval)
                    options_.irf_shocks{end + 1} = char(optval{nvars}.varname);
                end

                options_.irf_shocks = options_.irf_shocks';
            case 9
                options_.relative_irf = true;
            case 10
                options_.impulse_responses.plot_threshold = jm.optionsdescription{nopts}.stooptvalue;
            case 11
                options_.nocorr = true;
            case 12
                options_.nodecomposition = true;
            case 13
                options_.nofunctions = true;
            case 14
                options_.nomoments = true;
            case 15
                options_.nograph = true;
            case 16
                options_.order = jm.optionsdescription{nopts}.stooptvalue;
            case 17
                options_.k_order_solver = true;
            case 18
                options_.periods = jm.optionsdescription{nopts}.stooptvalue;
            case 19
                options_.qz_criterium = jm.optionsdescription{nopts}.stooptvalue;
            case 20
                options_.qz_criterium = jm.optionsdescription{nopts}.stooptvalue;
            case 21
                options_.replic = jm.optionsdescription{nopts}.stooptvalue;
            case 22
                options_.simul_replic = jm.optionsdescription{nopts}.stooptvalue;
            case 23
                options_.solve_algo = jm.optionsdescription{nopts}.stooptvalue;
            case 24
                options_.aim_solver = true;
            case 25
                options_.conditional_variance_decomposition = [];
                optval = jm.optionsdescription{nopts}.stooptvalue;

                for nvars = 1:length(optval)
                    options_.conditional_variance_decomposition(end + 1) = optval{nvars}.varname;
                end

                options_.conditional_variance_decomposition = options_.conditional_variance_decomposition';
            case 26
                options_.pruning = true;
            case 27
                options_.partial_information = true;
            case 28

                if strcmp(jm.optionsdescription{nopts}.stooptvalue, 'fixed_point') == true
                    options_.sylvester_fp = true;
                else
                    options_.sylvester_fp = false;
                end

            case 29
                options_.sylvester_fixed_point_tol = jm.optionsdescription{nopts}.stooptvalue;
            case 30

                if strcmp(jm.optionsdescription{nopts}.stooptvalue, 'cycle_reduction') == true
                    options_.dr_cycle_reduction = true;
                    options_.dr_logarithmic_reduction = false;
                elseif strcmp(jm.optionsdescription{nopts}.stooptvalue, 'logarithmic_reduction') == true
                    options_.dr_cycle_reduction = false;
                    options_.dr_logarithmic_reduction = true;
                else
                    options_.dr_cycle_reduction = false;
                    options_.dr_logarithmic_reduction = false;
                end

            case 31
                options_.dr_cycle_reduction_tol = jm.optionsdescription{nopts}.stooptvalue;
            case 32
                options_.dr_logarithmic_reduction_tol = jm.optionsdescription{nopts}.stooptvalue;
            case 33
                options_.dr_logarithmic_reduction_maxiter = jm.optionsdescription{nopts}.stooptvalue;
            case 34
                options_.loglinear = true;
            case 35
                options_.dr_display_tol = jm.optionsdescription{nopts}.stooptvalue;
            case 36
                options_.contemporaneous_correlation = true;
            case 37
                options_.SpectralDensity.trigger = true;

        end

    end

    %---All options end

    try
        [info, oo_, options_, M_] = stoch_simul(M_, options_, oo_, var_list_);
    catch ME
        disp(ME);
		runflag=0;
		return;
    end

    if isfield(oo_, 'irfs') == 1
        irfnames = fieldnames(oo_.irfs);

        for jj = 1:numel(fieldnames(oo_.irfs))
            data2json.irfs.(strtrim(char(irfnames(jj)))) = oo_.irfs.(irfnames{jj});
        end

    end

    if isfield(oo_, 'SpectralDensity') == 1
        data2json.SpectralDensity = oo_.SpectralDensity;
    end

    if isfield(oo_, 'var_list') == 1
        data2json.var_list = oo_.var_list;
    end

	if runflag==1
		%we only create output JSON if there are no errors
    	savejson('', data2json, 'stochsimout.JSON');
	end

    return;
