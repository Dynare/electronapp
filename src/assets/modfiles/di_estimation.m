function di_stochastic_simulations()

    global M_ options_ oo_ dataset_ estim_params_

    disp('di_estimation performing Matlab tasks');

    data2json = struct();

    data2json.exo_names = char(M_.exo_names);
    data2json.param_names = char(M_.param_names);
    data2json.exo_nbr = M_.exo_nbr;
    data2json.param_nbr = M_.param_nbr;

    %Data
    data2json.obs_nbr = length(dataset_.name);
    data2json.obs_names = char(dataset_.name);
    data2json.obs_data = dataset_.data;

    data2json.obs_data = {};

    for nobs = 1:length(dataset_.name)
        data2json.obs_data.(strtrim(char(dataset_.name(nobs, :)))) = dataset_.data(:, nobs);
    end

    %Smoothed observables

    if isfield(oo_, 'mle_mode') == 1
        %this is produced only with ML estim
        data2json.estimtypeflag = 0; %this will indicate ML estimation

        % if isfield(oo_.mle_mode,'parameters')==1
        % data2json.mle_mode_params=oo_.mle_mode.parameters;
        % data2json.mle_std_params=oo_.mle_std_at_mode.parameters;
        % end

        % if isfield(oo_.mle_mode,'shocks_std')==1
        % data2json.mle_mode_shocks_std=oo_.mle_mode.shocks_std;
        % data2json.mle_std_shocks_std=oo_.mle_std_at_mode.shocks_std;
        % end

        if isfield(oo_, 'mle_mode') == 1
            data2json.mle_mode = oo_.mle_mode;
        end

        if isfield(oo_, 'mle_std_at_mode') == 1
            data2json.mle_std_at_mode = oo_.mle_std_at_mode;
        end

    else
        data2json.estimtypeflag = 1; %this will indicate Bayesian estimation

        %prior info

        if isfield(estim_params_, 'param_vals') == 1

            if isempty(estim_params_.param_vals) == 0
                data2json.param_vals = estim_params_.param_vals;
            end

        end

        if isfield(estim_params_, 'var_exo') == 1

            if isempty(estim_params_.var_exo) == 0
                data2json.var_exo = estim_params_.var_exo;
            end

        end

        if isfield(estim_params_, 'corrx') == 1

            if isempty(estim_params_.corrx) == 0
                data2json.corrx = estim_params_.corrx;
            end

        end

        %prior density
        if isfield(oo_, 'prior_density') == 1
            data2json.prior_density = oo_.prior_density;
        end

        %posterior
        if isfield(oo_, 'posterior_density') == 1
            data2json.posterior_density = oo_.posterior_density;
        end

        if isfield(oo_, 'posterior_mode') == 1
            data2json.posterior_mode = oo_.posterior_mode;
            data2json.posterior_std = oo_.posterior_std_at_mode;
            data2json.posterior_mean = oo_.posterior_mean;
            data2json.posterior_hpdinf = oo_.posterior_hpdinf;
            data2json.posterior_hpdsup = oo_.posterior_hpdsup;
        end

        %bayesian_irfs
        if isfield(oo_, 'PosteriorIRF') == 1
            irfnames = fieldnames(oo_.PosteriorIRF.dsge.Mean);

            for jj = 1:numel(fieldnames(oo_.PosteriorIRF.dsge.Mean))
                data2json.bayesianirfmean.(strtrim(char(irfnames(jj)))) = oo_.PosteriorIRF.dsge.Mean.(irfnames{jj});
                data2json.bayesianirfhpdinf.(strtrim(char(irfnames(jj)))) = oo_.PosteriorIRF.dsge.HPDinf.(irfnames{jj});
                data2json.bayesianirfhpdsup.(strtrim(char(irfnames(jj)))) = oo_.PosteriorIRF.dsge.HPDsup.(irfnames{jj});
            end

        end

    end

    %smoother: some identical oo fields are used in a different way whether the option "smoother" is active or not. We need to take this into account
    if isfield(oo_.SmoothedVariables, 'deciles') == 1
        %when the "smoother" option is active
        smoothedvarnames = fieldnames(oo_.SmoothedVariables.Mean);

        for jj = 1:numel(fieldnames(oo_.SmoothedVariables.Mean))
            data2json.smoothedvarmean.(strtrim(char(smoothedvarnames(jj)))) = oo_.SmoothedVariables.Mean.(smoothedvarnames{jj});
            data2json.smoothedvardeciles.(strtrim(char(smoothedvarnames(jj)))) = oo_.SmoothedVariables.deciles.(smoothedvarnames{jj});
            data2json.constantmean.(strtrim(char(smoothedvarnames(jj)))) = oo_.Constant.Mean.(smoothedvarnames{jj});
            data2json.constantdeciles.(strtrim(char(smoothedvarnames(jj)))) = oo_.Constant.deciles.(smoothedvarnames{jj});
            data2json.updatedvarmean.(strtrim(char(smoothedvarnames(jj)))) = oo_.UpdatedVariables.Mean.(smoothedvarnames{jj});
            data2json.updatedvardeciles.(strtrim(char(smoothedvarnames(jj)))) = oo_.UpdatedVariables.deciles.(smoothedvarnames{jj});
        end

        if isfield(oo_, 'SmoothedShocks') == 1
            smoothedshocknames = fieldnames(oo_.SmoothedShocks.Mean);

            for jj = 1:numel(fieldnames(oo_.SmoothedShocks.Mean))
                data2json.smoothedshocksmean.(strtrim(char(smoothedshocknames(jj)))) = oo_.SmoothedShocks.Mean.(smoothedshocknames{jj});
                data2json.smoothedshocksdeciles.(strtrim(char(smoothedshocknames(jj)))) = oo_.SmoothedShocks.deciles.(smoothedshocknames{jj});
            end

        end

    else
        %when the "smoother" option is inactive
        %Smoothed shocks
        if isfield(oo_, 'SmoothedShocks') == 1
            data2json.SmoothedShocks = oo_.SmoothedShocks;
        end

        %Smoothed variables
        if isfield(oo_, 'SmoothedVariables') == 1
            data2json.SmoothedObs = {};

            for nobs = 1:length(dataset_.name)
                data2json.SmoothedObs.(strtrim(char(dataset_.name(nobs, :)))) = oo_.SmoothedVariables.(strtrim(char(dataset_.name(nobs, :))));
            end

        end

    end

    %forecasts
    if isfield(oo_, 'MeanForecast') == 1
        %there is a MH forecast field
        forecastvarnames = fieldnames(oo_.MeanForecast.Mean);

        for jj = 1:numel(fieldnames(oo_.MeanForecast.Mean))
            data2json.meanforecastvarmean.(strtrim(char(forecastvarnames(jj)))) = oo_.MeanForecast.Mean.(forecastvarnames{jj});
            data2json.meanforecastvardeciles.(strtrim(char(forecastvarnames(jj)))) = oo_.MeanForecast.deciles.(forecastvarnames{jj});
            data2json.pointforecastvarmean.(strtrim(char(forecastvarnames(jj)))) = oo_.PointForecast.Mean.(forecastvarnames{jj});
            data2json.pointforecastvardeciles.(strtrim(char(forecastvarnames(jj)))) = oo_.PointForecast.deciles.(forecastvarnames{jj});
        end

    end

    if isfield(oo_, 'forecast') == 1
        %there is a non-MH forecast field
        forecastvarnames = fieldnames(oo_.forecast.Mean);

        for jj = 1:numel(fieldnames(oo_.forecast.Mean))
            data2json.forecastvarmean.(strtrim(char(forecastvarnames(jj)))) = oo_.forecast.Mean.(forecastvarnames{jj});
            data2json.forecastvarhpdinf.(strtrim(char(forecastvarnames(jj)))) = oo_.forecast.HPDinf.(forecastvarnames{jj});
            data2json.forecastvarhpdsup.(strtrim(char(forecastvarnames(jj)))) = oo_.forecast.HPDsup.(forecastvarnames{jj});
        end

    end

    savejson('', data2json, 'estimout.JSON');

    disp('di_estimation finished');

    return;
