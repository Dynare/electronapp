// const {shell} = require('electron');
const ipc = require('electron').ipcRenderer;
const path = require('path');
const sqlite3 = require('sqlite3').verbose();
var short = require('short-uuid');
// const savetodb = require('./appModules').savetodb;
const showconsolealert= require('./appModules').showconsolealert;



const parser = new DOMParser();

function setfilename(val) {
  var fileName = val.substr(val.lastIndexOf("\\") + 1, val.length);
  document.getElementById("uploadFile").value = fileName;
}





const createNewModel = (dynaremodel) => {
  document.title = dynaremodel.modelname + ' Project';
  $('#codehash').val(dynaremodel.modelhash);
  $('#codename').val(dynaremodel.modelname);
  $('#code-dynare').val(dynaremodel.modelcode);

  var te_dynare = document.getElementById("code-dynare");
  // te_dynare.value = $('#code-dynare').val();//"%Enter model here\n"

  window.editor_dynare = CodeMirror.fromTextArea(te_dynare, {
    mode: "octave",
    lineNumbers: true,
    viewportMargin: Infinity,
    extraKeys: {
      "Ctrl-Q": function(cm) {
        cm.foldCode(cm.getCursor());
      }
    },
    foldGutter: true,
    gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"]
  });

  return;
}



const initapp = () => {

  var dbpath = path.resolve(__dirname, './assets/data/dynare.db');

  let db = new sqlite3.Database(dbpath, sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE, (err) => {
    if (err) {
      console.log('problem here')
      console.error(err.message);
      return console.log(err.message);
    }
  });

  console.log('Connected to the dynare database.');

  let sql = `SELECT COUNT(*) as mycount FROM sqlite_master WHERE type='table' AND name='dynaremodel';`;

  db.get(sql, [], (err, row) => {
    if (err) {
      return console.error(err.message);
    }
    if (row.mycount == 0) {
      //no tables in database, we create a new database
      modelhashuuid = short.generate();
      timestamp = new Date().toISOString().slice(0, 19).replace('T', ' ');
      db.serialize(() => {
        // Queries scheduled here will be serialized.
        db.run(`CREATE TABLE dynaremodel ( id INTEGER NOT NULL, modelhash VARCHAR(100) NOT NULL, modelname VARCHAR(100) NOT NULL, modelcode TEXT, modelextrassinfo TEXT, modelextrasstype INTEGER, modelcreatedon DATETIME NOT NULL, modelupdatedon DATETIME NOT NULL, PRIMARY KEY (id), UNIQUE (modelhash) );`,[], function(err) {
          if (err) {
            return console.log(err.message);
          }
          // get the last insert id
          console.log(`dynaremodel created`);
        });
        db.run(`INSERT INTO dynaremodel(modelhash,modelname,modelcode,modelcreatedon,modelupdatedon) VALUES(?,?,?,?,?)`, [modelhashuuid, modelhashuuid, '%Enter model here', timestamp, timestamp], function(err) {
          if (err) {
            return console.log(err.message);
          }
          // get the last insert id
          console.log(`A row has been inserted with rowid ${this.modelhash}`);
        });
        db.get('SELECT * FROM dynaremodel ORDER BY modelupdatedon DESC', [], (err, mymodel) => {
          if (err) {
            return console.error(err.message);
          } else {
            console.log(mymodel);
            createNewModel(mymodel);
          }
        });

        db.close((err) => {
          if (err) {
            return console.error(err.message);
          }
        });
      });

    }
    else {
      //there is a table in the database, we get the latest model
      db.serialize(() => {
        // We have to serialize in order to avoir race conditions
        db.get('SELECT * FROM dynaremodel ORDER BY modelupdatedon DESC', [], (err, mymodel) => {
          if (err) {
            return console.error(err.message);
          } else {
            console.log(mymodel);
            createNewModel(mymodel);
          }
        });
        db.close((err) => {
          if (err) {
            return console.error(err.message);
          }
        });
      });

    }


  });



  return;
}


initapp();



ipc.on('matlabmessagetoconsole', (event, msg) => {
  // showconsolealert(`${arg}`,0);
  showconsolealert(msg,0);
  scrolltextarea();
});

var numberPattern = /\d+/g;

ipc.on('matlabmessagetoconsole_progressbar_init', (event, arg) => {
  //initializes the MH progress bar
  // socket.on('matlabmessagetoconsole_progressbar_init', function(msg){
    
    $('#MHmodal').modal({backdrop: 'static', keyboard: false}) ;
    $('#MHmodal').modal('show'); 
    // modalupdatecount=0;
    // console.log('matlabmessagetoconsole_progressbar_init received');
    // document.getElementById("MHloop").innerHTML="1";
  // });
});

ipc.on('matlabmessagetoconsole_progressbar', (event, msg) => {
  //updates the MH progress bar
  // socket.on('matlabmessagetoconsole_progressbar', function(msg){
    // console.log('matlabmessagetoconsole_progressbar received');
   
    var mynumarray=msg.match(numberPattern);
    document.getElementById("MHloop").innerHTML=mynumarray[0];
    document.getElementById("MHloopover").innerHTML=mynumarray[1];
    document.getElementById("MHCAR").innerHTML=mynumarray[2].toString()+'.'+mynumarray[3].toString();
    var elem = document.getElementById("myBar");
    var width = mynumarray[4];

    if (width >= 100) {
      i = 0;
    } else {
      // width++;
      elem.style.width = width + "%";
      elem.innerHTML = width  + "%";
    }

  // });
});


ipc.on('matlabmessagetoconsole_progressbarbayes_init', (event, arg) => {
  //initializes the Bayesian IRF progress bar
    
  $('#MHmodal').modal('hide');
  $('#BImodal').modal({
    backdrop: 'static',
    keyboard: false
  });
  $('#BImodal').modal('show');
});


ipc.on('matlabmessagetoconsole_progressbarbayes', (event, msg) => {
  //updates the Bayesian IRF progress bar
  var mynumarray = msg.match(numberPattern);
  var elem = document.getElementById("myBIBar");
  var width = mynumarray[0];

  if (width >= 100) {
    i = 0;
  } else {
    // width++;
    elem.style.width = width + "%";
    elem.innerHTML = width + "%";
  }
});



ipc.on('matlabmessagetoconsole_progressbarsmooth_init', (event, arg) => {
  //initializes the Smoother progress bar
    
  $('#MHmodal').modal('hide');
  $('#BImodal').modal('hide');
  $('#SMmodal').modal({
    backdrop: 'static',
    keyboard: false
  });
  $('#SMmodal').modal('show');
});


ipc.on('matlabmessagetoconsole_progressbarbayes', (event, msg) => {
  //updates the Smoother progress bar
  var mynumarray = msg.match(numberPattern);
  var elem = document.getElementById("mySMBar");
  var width = mynumarray[0];
  if (width >= 100) {
    i = 0;
  } else {
    // width++;
    elem.style.width = width + "%";
    elem.innerHTML = width + "%";
  }
});












ipc.on('createnewproject', (event, arg) => {
  console.log('got it!')
  createnewproject();
})
